package com.example.dusan.musicapp.player;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.widget.RemoteViews;

import com.example.dusan.musicapp.Consts;
import com.example.dusan.musicapp.MainActivity;
import com.example.dusan.musicapp.R;
import com.example.dusan.musicapp.common.models.RecentlySong;
import com.example.dusan.musicapp.playlist.Playlist;
import com.example.dusan.musicapp.playlist.Song;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;


public class MusicService extends Service
        implements MediaPlayer.OnCompletionListener, MediaPlayer.OnPreparedListener,
        MediaPlayer.OnErrorListener, MusicFocusable{
    final static String TAG = "MusicService";

    static final String ACTION_PLAY_NEXT = "com.example.dusan.musicapp.PLAY_NEXT";
    static final String ACTION_PLAY_PREVIOUS = "com.example.dusan.musicapp.PLAY_PREVIOUS";
    static final String ACTION_PAUSE = "com.example.dusan.musicapp.PAUSE";
    static final String ACTION_CLOSE = "com.example.dusan.musicapp.CLOSE";

    private LocalBinder mLocalBinder = new LocalBinder();
    private Callback mCallback;

    ArrayList<Song> mSongs = null;

    // currently playing song pointer
    int mSongPointer = -1;

    // our media player
    MediaPlayer mPlayer = null;
    private ArrayList<Playlist> mArrayListRecentlys;
    private boolean mIsLoop = true;// now allays allow playlist play loop

    public enum State{
        PREPARING, // song is preparing
        PLAYING, // song is playing
        PAUSED, // song is paused
        STOPPED // media player is stopped
    }

    public enum PlayMode {
        NORMAL,
        PLAY_RECENTLY_SONG
    }

    State mState = State.STOPPED;

    // do we have audio focus?
    enum AudioFocus {
        NoFocusNoDuck,    // we don't have audio focus, and can't duck
        NoFocusCanDuck,   // we don't have focus, but can play at a low volume ("ducking")
        Focused           // we have full audio focus
    }
    AudioFocus mAudioFocus = AudioFocus.NoFocusNoDuck;

    // our AudioFocusHelper object, if it's available (it's available on SDK level >= 8)
    // If not available, this will be null. Always check for null before using!
    AudioFocusHelper mAudioFocusHelper = null;

    // The volume we set the media player to when we lose audio focus, but are allowed to reduce
    // the volume instead of stopping playback.
    public static final float DUCK_VOLUME = 0.1f;

    // Wifi lock that we hold when streaming files from the internet, in order to prevent the
    // device from shutting off the Wifi radio
    WifiManager.WifiLock mWifiLock;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.e(TAG, "Creating service");

        // create array list, which holds all songs to play
        mSongs = new ArrayList<>();

        mAudioFocusHelper = new AudioFocusHelper(getApplicationContext(), this);

        // Create the Wifi lock (this does not acquire the lock, this just creates it)
        mWifiLock = ((WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE))
                .createWifiLock(WifiManager.WIFI_MODE_FULL, "mylock");

        mIsAppRunning = true;

        playPrevIntent = PendingIntent.getService(this, 0, new Intent(MusicService.ACTION_PLAY_PREVIOUS), 0);
        playNextIntent = PendingIntent.getService(this, 0, new Intent(MusicService.ACTION_PLAY_NEXT), 0);
        pauseIntent = PendingIntent.getService(this, 0, new Intent(MusicService.ACTION_PAUSE), 0);
        closeIntent = PendingIntent.getService(this, 0, new Intent(MusicService.ACTION_CLOSE), 0);
    }

    @Override
    public void onDestroy() {
        Log.e(TAG, "onDestroy()");
        giveUpAudioFocus();

        // we can also release the Wifi lock, if we're holding it
        if (mWifiLock.isHeld()) mWifiLock.release();
    }



    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String action = intent.getAction();

        if (action != null && action.equals(MusicService.ACTION_PLAY_PREVIOUS)){

            processPlayPreviousRequest();

        } else if ( action != null && action.equals(MusicService.ACTION_PLAY_NEXT)){

            processPlayNextRequest();

        } else if (action != null && action.equals(MusicService.ACTION_PAUSE)){
            processTogglePlayRequest();
        } else if (action != null && action.equals(MusicService.ACTION_CLOSE)){
            closeNotification();
        }

        return Service.START_NOT_STICKY; // Means we started the service, but don't want it to
        // restart in case it's killed.
    }

    private void closeNotification(){
        stopForeground(true);
        stopSelf();
        mNotification = null;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mLocalBinder;
    }

    private static final int NOTIFICATION_ID = 99;
    private Notification mNotification = null;
    private NotificationManager mNotificationManager = null;
    PendingIntent playPrevIntent,
            playNextIntent,
            pauseIntent,
            closeIntent;
    private RemoteViews mRemoteViews;

    private void setNotification(Song song, Playlist playlist){

        // first time create whole notification
        if (mNotification == null){

            // Creates an explicit intent for an ResultActivity to receive.
            Intent resultIntent = new Intent(this, MainActivity.class);

            // This ensures that the back button follows the recommended
            // convention for the back key.
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);

            // Adds the back stack for the Intent (but not the Intent itself)
            stackBuilder.addParentStack(MainActivity.class);

            // Adds the Intent that starts the Activity to the top of the stack.
            stackBuilder.addNextIntent(resultIntent);
            PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
                    0, PendingIntent.FLAG_UPDATE_CURRENT);

            // Create remote view and set bigContentView.
            mRemoteViews = new RemoteViews(this.getPackageName(),
                    R.layout.service_notification_layout);
            // expandedView.setTextViewText(R.id.text_view, "Neat logo!");
            mRemoteViews.setOnClickPendingIntent(R.id.play_next, playNextIntent);
            mRemoteViews.setOnClickPendingIntent(R.id.pause, pauseIntent);
            mRemoteViews.setOnClickPendingIntent(R.id.play_prev, playPrevIntent);
            mRemoteViews.setOnClickPendingIntent(R.id.close_notification, closeIntent);

            if (playlist.mFrom == Playlist.From.RADIO) {
                mRemoteViews.setImageViewBitmap(R.id.image, playlist.mBitmap);
            } else if(playlist.mBitmap != null){
                // it's local album we already have bitmap
                mRemoteViews.setImageViewBitmap(R.id.image, playlist.mBitmap);
            } else {
                // it's album from server, we need to get bitmap
                new getBitmap().execute(Consts.getCoverArtUrl(playlist.mCoverArt));
            }

            mRemoteViews.setTextViewText(R.id.song_title, song.mTitle);
            mRemoteViews.setTextViewText(R.id.song_artist, song.mArtist);
            mRemoteViews.setTextViewText(R.id.album_title, playlist.mTitle);

            mNotification = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setAutoCancel(false)
                    .setContentIntent(resultPendingIntent)
                    // .setDeleteIntent(stopServiceIntent)
                    .setContentTitle(song.mTitle)
                    .setContentText(song.mArtist)
                    // .setContent(mRemoteViews)
                    .build();

            mNotification.bigContentView = mRemoteViews;

            mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            // mId allows you to update the notification later on.
            mNotificationManager.notify(NOTIFICATION_ID, mNotification);


            // start foreground service
            startForeground(NOTIFICATION_ID, mNotification);

        } else {

            if(playlist.mBitmap != null){
                // it's local album we already have bitmap
                mRemoteViews.setImageViewBitmap(R.id.image, playlist.mBitmap);
            } else {
                // it's album from server, we need to get bitmap
                new getBitmap().execute(Consts.getCoverArtUrl(playlist.mCoverArt));
            }

            mRemoteViews.setTextViewText(R.id.song_title, song.mTitle);
            mRemoteViews.setTextViewText(R.id.song_artist, song.mArtist);
            mRemoteViews.setTextViewText(R.id.album_title, playlist.mTitle);

            // refresh view
            mNotificationManager.notify(NOTIFICATION_ID, mNotification);
        }

    }

    // get bitmap from url async task
    private class getBitmap extends AsyncTask<String, Void, Bitmap>{
        @Override
        protected Bitmap doInBackground(String... urls) {
            try {
                URL url = new URL(urls[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(input);
                return myBitmap;
            } catch (IOException e) {
                // Log exception
                return null;
            }
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            mRemoteViews.setImageViewBitmap(R.id.image, bitmap);
            mNotificationManager.notify(NOTIFICATION_ID, mNotification);
        }
    }

    public void processTogglePlayRequest() {
        // ignore click if there is no music player
        if (mPlayer == null) {
            if (mCallback != null)
                mCallback.onPrepareErrorListener();
            return;
        }

        if(mPlayer.isPlaying()) {
            // if it's playing pause it and send on pause listener
            pausePlayer();
        } else {
            // if it's paused play it and send on play listener
            mPlayer.start();
            if (mCallback != null)
                mCallback.onPlayListener();

            if (mNotification != null) {
                // display pause image in notification
                mRemoteViews.setImageViewResource(R.id.pause, R.drawable.ic_action_pause_white);
                mNotificationManager.notify(NOTIFICATION_ID, mNotification);
            }
        }
    }

    public boolean isMedaPlayerNull() {
        return (mPlayer != null);
    }

    public boolean isMediaPlayerPlaying(){
        return mPlayer != null && mPlayer.isPlaying();
    }

    public void processPlayPreviousRequest() {
        playPreviousSong();
    }

    public void processPlayNextRequest(){
        playNextSong();
    }

    private Playlist mPlaylist;

    public Playlist getPlaylist() {
        return mPlaylist;
    }

    public void processAddToQueueRequest(ArrayList<Song> songs, Playlist playlist){
        Log.i(TAG, "processAddToQueue");

        mPlaylist = playlist;

        // create a copy of a songs since songs array list is used in playlist fragment!!!
        mSongs = new ArrayList<>(songs);

        // restart pointer
        restartSongPointer();

        // in this case always play next song
        mPlayNextSong = true;

        playNextSong();
    }

    public void procssAddToQueueRequest(ArrayList<RecentlySong> recentlySongs, int songPointer) {
        Log.e(TAG, "processAddToQueueRequest(ArrayList<Song> songs, Playlist playlist, int songPointer)");


        mSongPointer = songPointer;
        mPlayNextSong = true;
        Log.e(TAG, "done");

        playSong();
    }

    public void processAddToQueueRequest(ArrayList<Song> songs, Playlist playlist, int songPointer){
        Log.e(TAG, "processAddToQueueRequest(ArrayList<Song> songs, Playlist playlist, int songPointer)");
        mPlaylist = playlist;

        mSongs = new ArrayList<>(songs);

        mSongPointer = songPointer;

        Log.e(TAG, "nest line is mPlayNextSong = true;");
        mPlayNextSong = true;
        Log.e(TAG, "done");

        playSong();
    }

    // flat to check if next song should be played
    boolean mPlayNextSong = true;

    public void processAddToQueueRequest(ArrayList<Song> songs, boolean playNextSong, Playlist playlist){
        Log.i(TAG, "processAddToQueue(ArrayList<Song> songs, boolean playNextSong, Playlist playlist)");

        mPlaylist = playlist;

        // create a copy of a songs since songs array list is used in playlist fragment!!!
        mSongs = new ArrayList<>(songs);

        // restart pointer
        restartSongPointer();

        mPlayNextSong = playNextSong;

        playNextSong();
    }

    void pausePlayer(){
        if(mPlayer != null) {
            mPlayer.pause();
            mState = State.PAUSED;
        }
        if (mCallback != null)
            mCallback.onPauseListener();

        if (mNotification != null) {
            // display play image in notification
            mRemoteViews.setImageViewResource(R.id.pause, R.drawable.ic_action_play_white);
            mNotificationManager.notify(NOTIFICATION_ID, mNotification);
        }
    }

    public State getPlayerState(){
        return mState;
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        // check if it is end of a song (not changed by user)
        if(mp.getCurrentPosition() != 0){
            playNextSong();
        }
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        Log.e(TAG, "onError(MediaPlayer mp, int what="+what+", int extra)");
        if(what == MediaPlayer.MEDIA_ERROR_UNKNOWN){
            // if error is unknown retry playing song
            playSong();
            return false;
        }

        if (mCallback != null)
         mCallback.onPrepareErrorListener();
        mState = State.STOPPED;
        giveUpAudioFocus();
        return false;
    }

    Handler mSeekBarHandler;
    Runnable mSeekBarRunnable;

    @Override
    /** Called when media player is done preparing. */
    public void onPrepared(MediaPlayer mp) {
        Log.e(TAG, "onPrepared");

        // if prepare is called but app is minimized
        // ignore call
        if(!mIsAppRunning)
            return;

        if (mCallback != null)
            mCallback.onPreparedListener();
        // The media player is done preparing. That means we can start playing!
        configAndStartMediaPlayer();
        mState = State.PAUSED;

        // set currently playing song to MainActivity for UI update
        // send playlist to store in shared pref
        Playlist currentPlayList = mPlaylist;
        if (mCallback != null) {
            mCallback.onSongPlaying(mSongPointer, mSongs.get(mSongPointer), currentPlayList);
        }

        setNotification(mSongs.get(mSongPointer), currentPlayList);

        mSeekBarHandler = new Handler();
        mSeekBarRunnable = new Runnable() {
            @Override
            public void run() {
                int mediaPlayerPosition = getMediaPlayerCurrentPosition();
                // mediaPlayerPosition is -1 when media player is null
                if(mediaPlayerPosition != -1 && mCallback != null) {
                    mCallback.onProgressChanged(mediaPlayerPosition);
                }
                mSeekBarHandler.postDelayed(mSeekBarRunnable, 1000);
            }
        };
        mSeekBarHandler.post(mSeekBarRunnable);
    }

    public void trigerOnSongPlaying(){
        if (mCallback != null)
            mCallback.onSongPlaying(mSongPointer, mSongs.get(mSongPointer), mPlaylist);
    }

    public void setSongPointer(int songPointer){
        mSongPointer = songPointer;
        mPlayNextSong = true;
        playSong();
    }

    void restartSongPointer(){
        mSongPointer = -1;
    }

    public int getMediaPlayerCurrentPosition(){
        return mPlayer != null ? mPlayer.getCurrentPosition() : -1;
    }

    /**
     * Makes sure the media player exists and has been reset. This will create the media player
     * if needed, or reset the existing media player if one already exists.
     */
    void createMediaPlayerIfNeeded() {
        Log.e(TAG, "createMediaPlayerIfNeeded");
        if (mPlayer == null) {
            mPlayer = new MediaPlayer();

            // Make sure the media player will acquire a wake-lock while playing. If we don't do
            // that, the CPU might go to sleep while the song is playing, causing playback to stop.
            mPlayer.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);

            // we want the media player to notify us when it's ready preparing, and when it's done
            // playing:
            mPlayer.setOnPreparedListener(this);
            mPlayer.setOnCompletionListener(this);
            mPlayer.setOnErrorListener(this);
        }
        else
            mPlayer.reset();
    }

    /**
     * rewind player to value to which users drag to
     * */
    public void onSeekBarProgressChanged(int progress){
        if(mPlayer != null){
            // rewind
            mPlayer.seekTo(progress);
        }
    }

    void configAndStartMediaPlayer() {
        if (mAudioFocus == AudioFocus.NoFocusNoDuck) {
            // If we don't have audio focus and can't duck, we have to pause, even if mState
            // is State.Playing. But we stay in the Playing state so that we know we have to resume
            // playback once we get the focus back.
            if (mPlayer.isPlaying()) pausePlayer();
            return;
        }
        else if (mAudioFocus == AudioFocus.NoFocusCanDuck)
            mPlayer.setVolume(DUCK_VOLUME, DUCK_VOLUME);  // we'll be relatively quiet
        else
            mPlayer.setVolume(1.0f, 1.0f); // we can be loud

        Log.e(TAG, "mPlayNextSong=" + mPlayNextSong);
        if (/*!mPlayer.isPlaying() &&*/ mPlayNextSong) {
            mState = State.PLAYING;
            mPlayer.start();

            if (mCallback != null)
                mCallback.onPlayListener();
            if (mNotification != null) {
                // display pause image in notification
                mRemoteViews.setImageViewResource(R.id.pause, R.drawable.ic_action_pause_white);
                mNotificationManager.notify(NOTIFICATION_ID, mNotification);
            }
        } else {
            // when mPlayNextSong is setted to false block only first attempt
            // when app starts mPlayNextSong will be false, don't play song then
            // on next try make sure this flag is true
            mPlayNextSong = true;
        }
    }

    /** Set song pointer and play song for current pointer */
    void playNextSong(){
        if(!mIsLoop && songPointerOutOfBounds(mSongPointer+1)){
            if (mCallback != null)
                mCallback.onPrepareErrorListener();
            return;
        }
        if (mSongPointer < mSongs.size()-1) {
            mSongPointer++;
        } else {
            mSongPointer = 0;
        }
        playSong();
    }

    /** Set song pointer and play song for current pointer */
    void playPreviousSong(){
        if(!mIsLoop && songPointerOutOfBounds(mSongPointer-1)){
            if (mCallback != null)
                mCallback.onPrepareErrorListener();
            return;
        }

        if (mSongPointer == 0) {
            mSongPointer = mSongs.size()-1;
        } else {
            mSongPointer--;
        }
        playSong();
    }

    public Song getCurrentlyPlayingSong(){
        // if there are no currently playing song
        if (mSongPointer == -1)
            return null;

        return mSongs.get(mSongPointer);
    }

    void playSong(){
        Log.e(TAG, "playSong + " +  mSongs.get(mSongPointer).getURI().toString());
        tryToGetAudioFocus();
        try {
            createMediaPlayerIfNeeded();
            mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

            if (getPlaylist().mFrom == Playlist.From.RADIO) {
                mPlayer.setDataSource(mSongs.get(mSongPointer).mStreamUrl);
            } else {
                mPlayer.setDataSource(getApplicationContext(), mSongs.get(mSongPointer).getURI());
            }
//

            // starts preparing the media player in the background. When it's done, it will call
            // our OnPreparedListener (that is, the onPrepared() method on this class, since we set
            // the listener to 'this').
            //
            // Until the media player is prepared, we *cannot* call start() on it!
            Log.e(TAG, "before mPlayer.prepareAsync();");
            mPlayer.prepareAsync();
            mState = State.PREPARING;
            if(mCallback != null)
                mCallback.onPreparing();

            // If we are streaming from the internet, we want to hold a Wifi lock, which prevents
            // the Wifi radio from going to sleep while the song is playing. If, on the other hand,
            // we are *not* streaming, we want to release the lock if we were holding it before.
            if (!mSongs.get(mSongPointer).mLocal) {
                Log.e(TAG, "streaming song, get wifi lock");
                mWifiLock.acquire();
            }
            else if (mWifiLock.isHeld()) {
                mWifiLock.release();
            }

        } catch (IOException e) {
            Log.e(TAG, "IOException playing next song: " + e.getMessage());
            e.printStackTrace();
        }
    }

    // flag to know if app is running or not
    //   if main activity is killed it may happen that async preparation of media player isn't finished
    //   in that case just ignore onPrepare() callback
    private boolean mIsAppRunning;

    public void setIsAppRunning(boolean isAppRunning){
        mIsAppRunning = isAppRunning;
    }

    public void setCallback(Callback callback){
        mCallback = callback;
    }

    public class LocalBinder extends Binder{
        public MusicService getService(){
            return MusicService.this;
        }
    }

    private boolean songPointerOutOfBounds(int tSongPointer){
        return (tSongPointer < 0 || tSongPointer >= mSongs.size());
    }

    public interface Callback{
        void onSongPlaying(int index, Song song, Playlist playlist);
        void onPlayListener();
        void onPauseListener();
        void onPreparedListener();
        void onPrepareErrorListener();
        void onPreparing();
        void onProgressChanged(int progress);
    }

    // flag to check if player was playing before it lost focus
    // if player was not playing before focus loss it shouldn't be playing after
    boolean isPlayingWhileLostAudioFocus = false;

    /**
     * MusicFocusable implemented methods
     * */
    @Override
    public void onGainedAudioFocus() {
        Log.e(TAG, "onGainedAudioFocus");
        Log.e(TAG, "mState=" + mState);
//        Toast.makeText(getApplicationContext(), "gained audio focus.", Toast.LENGTH_SHORT).show();
        mAudioFocus = AudioFocus.Focused;


        // if player was not playing before focus loss it shouldn't be playing after
        if(isPlayingWhileLostAudioFocus)
            configAndStartMediaPlayer(); // restart media player with new focus settings

        // set again to default value
        isPlayingWhileLostAudioFocus = false;
    }

    @Override
    public void onLostAudioFocus(boolean canDuck) {
        Log.e(TAG, "onLostAudioFocus");
//        Toast.makeText(getApplicationContext(), "lost audio focus." + (canDuck ? "can duck" :
//                "no duck"), Toast.LENGTH_SHORT).show();
        mAudioFocus = canDuck ? AudioFocus.NoFocusCanDuck : AudioFocus.NoFocusNoDuck;

        // check if player is playing onLostAudioFocus
        isPlayingWhileLostAudioFocus = mPlayer != null && mPlayer.isPlaying();

        // start/restart/pause media player with new focus settings
        if (mPlayer != null && mPlayer.isPlaying())
            configAndStartMediaPlayer();
    }
    /** end of MusicFocusable implemented methods*/
    void giveUpAudioFocus() {
        if (mAudioFocus == AudioFocus.Focused && mAudioFocusHelper != null
                && mAudioFocusHelper.abandonFocus())
            mAudioFocus = AudioFocus.NoFocusNoDuck;
    }

    void tryToGetAudioFocus() {
        Log.e(TAG, "tryToGetAudioFocus()");
        if (mAudioFocus != AudioFocus.Focused && mAudioFocusHelper != null
                && mAudioFocusHelper.requestFocus())
            mAudioFocus = AudioFocus.Focused;
    }

    public ArrayList<Song> getCurrentlyPlayingSongs(){
        return mSongs;
    }

}
