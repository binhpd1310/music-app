package com.example.dusan.musicapp.adapters;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dusan.musicapp.R;
import com.example.dusan.musicapp.models.StationRadio;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by ${binhpd} on 6/30/2016.
 */
public class RadioAdapter extends BaseAdapter {

    private final LayoutInflater inflater;
    private final ArrayList<StationRadio> mStationsRadio;
    private Context mContext;
    private String radioPlaying;
    AnimationDrawable playingAnimation;

    public RadioAdapter(Context context, ArrayList<StationRadio> stationsRadio) {
        mStationsRadio = stationsRadio;
        this.mContext = context;
        this.inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mStationsRadio.size();
    }

    @Override
    public StationRadio getItem(int position) {
        return mStationsRadio.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_radio, null);
            holder = new Holder();
            holder.civIcon = (ImageView) convertView.findViewById(R.id.ivLogo);
            holder.tvName = (TextView) convertView.findViewById(R.id.tvName);
            holder.ivAnimaiton = (ImageView) convertView.findViewById(R.id.playing_animation);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        StationRadio stationRadio = mStationsRadio.get(position);
        InputStream ims = null;
        try {
            ims = mContext.getAssets().open(stationRadio.getUrlLogo());
            // load image as Drawable
            Drawable d = Drawable.createFromStream(ims, null);
            holder.civIcon.setImageDrawable(d);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // set image to ImageView

        holder.tvName.setText(stationRadio.getName());
        if (stationRadio.getUrlStream().equals(radioPlaying)) {
            holder.ivAnimaiton.setVisibility(View.VISIBLE);
            holder.ivAnimaiton.setBackgroundResource(R.drawable.play_anim);
            playingAnimation = (AnimationDrawable) holder.ivAnimaiton.getBackground();
            playingAnimation.start();
        } else {
            holder.ivAnimaiton.setVisibility(View.INVISIBLE);
        }
        return convertView;
    }

    public void setRadioPlaying(String radioPlaying) {
        this.radioPlaying = radioPlaying;
    }

    static class Holder {
        ImageView civIcon;
        TextView tvName;
        ImageView ivAnimaiton;
    }
}