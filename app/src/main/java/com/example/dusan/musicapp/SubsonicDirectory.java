package com.example.dusan.musicapp;

import android.os.Parcel;
import android.os.Parcelable;

public class SubsonicDirectory implements Parcelable {
    private int mId;
    private String mName;

    public SubsonicDirectory(int id, String name){
        mId = id;
        mName = name;
    }

    public int getId(){
        return mId;
    }

    public String getName(){
        return mName;
    }



    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(mId);
        out.writeString(mName);
    }

    public static final Parcelable.Creator<SubsonicDirectory> CREATOR
            = new Parcelable.Creator<SubsonicDirectory>() {
        public SubsonicDirectory createFromParcel(Parcel in) {
            return new SubsonicDirectory(in);
        }

        public SubsonicDirectory[] newArray(int size) {
            return new SubsonicDirectory[size];
        }
    };

    private SubsonicDirectory(Parcel in) {
        mId = in.readInt();
        mName = in.readString();
    }

    public int describeContents() {
        return 0;
    }
}
