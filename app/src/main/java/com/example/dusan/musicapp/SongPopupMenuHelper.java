package com.example.dusan.musicapp;


import android.content.Context;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.dusan.musicapp.playlist.Song;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SongPopupMenuHelper implements PopupMenu.OnMenuItemClickListener{
    private static final String TAG = "SongPopupMenuHelper";

    // when user clicks on settings in playlist view
    //   save song which user clicked, so if user further click "Add to playlist" use this
    //   song to add to playlist
    private Song mClickedSongSettings;

    private Context mParentContext;

    private MaterialDialog mPlaylistDialog;
    private MaterialDialog mCreatePlaylistDialog;

    // use this dialog for everything else
    private MaterialDialog mRecycleDialog;

    RequestQueue mParentRequestQueue;

    public SongPopupMenuHelper(Context context, RequestQueue queue){
        mParentContext = context;
        mParentRequestQueue = queue;

        mPlaylistDialog = null;
        mCreatePlaylistDialog = null;
        mRecycleDialog = null;
    }

    public void openPopupMenu(View v){
        PopupMenu popup = new PopupMenu(mParentContext, v);

        mClickedSongSettings = (Song) v.findViewById(R.id.song_settings).getTag();

        // This activity implements OnMenuItemClickListener
        popup.setOnMenuItemClickListener(this);
        popup.inflate(R.menu.song_popup_menu);
        popup.show();
    }


    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add_to_playlist:
                addToPlaylist();
                return true;
            default:
                return false;
        }
    }

    private void addToPlaylist(){
        // local songs can't be added to play lists since play lists are stored on server
        if(mClickedSongSettings.mLocal){
            mRecycleDialog = new MaterialDialog.Builder(mParentContext)
                    .title(R.string.app_name)
                    .content(R.string.local_songs_to_playlist_explanation)
                    .positiveText(R.string.close)
                    .show();
            return;
        }

        mRecycleDialog = new MaterialDialog.Builder(mParentContext)
                .title(R.string.app_name)
                .content(R.string.loading_dialog_msg)
                .progress(true, 0)
                .cancelable(false)
                .show();

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Consts.getGetPlaylistsUrl(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject r = new JSONObject(response);

                            if( r.getJSONObject("subsonic-response").getString("status").equals("ok") ){

                                JSONArray playlists = r.getJSONObject("subsonic-response")
                                        .getJSONObject("playlists").getJSONArray("playlist");
                                JSONObject row;
                                ArrayList<SubsonicPlaylist> albums = new ArrayList<>();
                                for (int i = 0; i < playlists.length(); i++) {
                                    row = playlists.getJSONObject(i);
                                    // display only playlists created by user
                                    if(!row.getBoolean("public")){
                                        albums.add(new SubsonicPlaylist(row));
                                    }
                                }
                                openAlbumSelectionDiagram(albums);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        // Add the request to the RequestQueue.
        mParentRequestQueue.add(stringRequest);
    }

    private void openAlbumSelectionDiagram(final ArrayList<SubsonicPlaylist> albums){
        // convert array list to array
        final String[] albumsArr = new String[albums.size()];
        for (int i = 0; i < albums.size(); i++) {
            albumsArr[i] = albums.get(i).getName();
        }

        mPlaylistDialog = new MaterialDialog.Builder(mParentContext)
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {

                        // create new playlist
                        mCreatePlaylistDialog = new MaterialDialog.Builder(mParentContext)
                                .callback(new MaterialDialog.ButtonCallback() {
                                    @Override
                                    public void onPositive(MaterialDialog dialog) {
                                        // getCreatePlaylistUrl
                                        createPlaylistOnSever();
                                    }
                                })
                                .title(R.string.app_name)
                                .customView(R.layout.create_new_playlist_dialog_layout, true)
                                .positiveText(R.string.create_playlist)
                                .negativeText(R.string.close_app_string)
                                .show();
                    }
                })
                .title(R.string.app_name)
                .items(albumsArr)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        addSongToPlaylist(albums.get(which));
                    }
                })
                .positiveText(R.string.create_new_playlist)
                .negativeText(R.string.cancel)
                .show();

        // hide loading dialog
        if(mRecycleDialog != null){
            mRecycleDialog.dismiss();
            mRecycleDialog = null;
        }
    }

    private void addSongToPlaylist(final SubsonicPlaylist playlist){
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Consts.getUpdatePlaylistUril(playlist.getId(), mClickedSongSettings.mId),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject r = new JSONObject(response);

                            if( r.getJSONObject("subsonic-response").getString("status").equals("ok") ){
                                Toast.makeText(mParentContext, mParentContext.getString(R.string.one_song_added_to) + " " + playlist.getName() + ".",
                                        Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        // Add the request to the RequestQueue.
        mParentRequestQueue.add(stringRequest);

    }

    private void createPlaylistOnSever(){
        View v = mCreatePlaylistDialog.getCustomView();
        final String playlistTitle = ((EditText) v.findViewById(R.id.playlist_title)).getText().toString();
        String url = Consts.getCreatePlaylistUrl(
                playlistTitle,
                Integer.toString(mClickedSongSettings.mId));

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e(TAG, response);

                        try {
                            JSONObject r = new JSONObject(response);

                            if( r.getJSONObject("subsonic-response").getString("status").equals("ok") ){
                                Toast.makeText(mParentContext, mParentContext.getString(R.string.one_song_added_to) + " " + playlistTitle + ".",
                                        Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        // Add the request to the RequestQueue.
        mParentRequestQueue.add(stringRequest);
    }

    public void clearDialogs(){
        if(mPlaylistDialog != null) {
            mPlaylistDialog.dismiss();
            mPlaylistDialog = null;
        }
        if(mCreatePlaylistDialog != null) {
            mCreatePlaylistDialog.dismiss();
            mCreatePlaylistDialog = null;
        }
        if(mRecycleDialog != null){
            mRecycleDialog.dismiss();
            mRecycleDialog = null;
        }
    }
}
