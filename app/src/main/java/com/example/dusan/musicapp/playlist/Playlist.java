package com.example.dusan.musicapp.playlist;


import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import com.example.dusan.musicapp.MainActivity;

public class Playlist implements Parcelable{

    public enum From {
        SUBSONIC_ALBUM,
        SUBSONIC_PLAYLIST,
        LOCAL_ALBUM,
        RADIO
    }

    // one of these two is always null
    public Bitmap mBitmap;
    public String mCoverArt;

    public String mTitle;
    public String mArtist;
    public String mNumOfSongs;

    public From mFrom;
    public String mId;

    public Playlist(Bitmap bitmap, String coverArt, String title, String artist, String numOfSongs){
        mBitmap = bitmap;
        mCoverArt = coverArt;
        mTitle = title;
        mArtist = artist;
        mNumOfSongs = numOfSongs;
    }

    public Playlist(){
        mBitmap = null;
        mCoverArt = null;
        mTitle = null;
        mArtist = null;
        mNumOfSongs = null;
    }

    public Playlist(String id, From from){
        mBitmap = null;
        mCoverArt = null;
        mTitle = null;
        mArtist = null;
        mNumOfSongs = null;
        mId = id;
        mFrom = from;
    }

    public void setFrom(String from){
        if(from.equals(From.SUBSONIC_ALBUM.toString())){
            mFrom = From.SUBSONIC_ALBUM;
        } else if (from.equals(From.SUBSONIC_PLAYLIST.toString())){
            mFrom = From.SUBSONIC_PLAYLIST;
        } else if (from.equals(From.LOCAL_ALBUM.toString())){
            mFrom = From.LOCAL_ALBUM;
        } else if (from.equals(From.RADIO.toString())){
            mFrom = From.RADIO;
        }
    }

    @Override
    public String toString() {
        return (mBitmap == null ? "on server ":"local ") + mTitle + " " + mArtist + " # of songs=" + mNumOfSongs;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * Storing the Song data to Parcel object
     **/
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(MainActivity.encodeTobase64(mBitmap));
        dest.writeString(mCoverArt);
        dest.writeString(mTitle);
        dest.writeString(mArtist);
        dest.writeString(mNumOfSongs);
        dest.writeString(mFrom.toString());
        dest.writeString(mId);
    }

    /**
     * Retrieving Song data from Parcel object
     * This constructor is invoked by the method createFromParcel(Parcel source) of
     * the object CREATOR
     **/
    private Playlist(Parcel in){
        this.mBitmap = MainActivity.decodeBase64(in.readString());
        this.mCoverArt = in.readString();
        this.mTitle = in.readString();
        this.mArtist = in.readString();
        this.mNumOfSongs = in.readString();
        setFrom(in.readString());
        mId = in.readString();
    }

    public static final Parcelable.Creator<Playlist> CREATOR = new Parcelable.Creator<Playlist>() {

        @Override
        public Playlist createFromParcel(Parcel source) {
            return new Playlist(source);
        }

        @Override
        public Playlist[] newArray(int size) {
            return new Playlist[size];
        }
    };
}
