package com.example.dusan.musicapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.model.AppInviteContent;
import com.facebook.share.widget.AppInviteDialog;
import com.google.android.gms.appinvite.AppInviteInvitation;

import butterknife.ButterKnife;

/**
 * Created by binhpd on 10/29/17.
 */
public class InviteHelper {
    public static final int REQUEST_INVITE = 1234;
    private final CallbackManager mCallbackManager;
    private Activity activity;

    public InviteHelper (Activity activity, CallbackManager callbackManager) {
        this.activity = activity;
        this.mCallbackManager = callbackManager;
    }
    void showInviteDialog() {
        View view = LayoutInflater.from(activity).inflate(R.layout.invite_dialog, null);
        Button tvFacebook = ButterKnife.findById(view, R.id.tvFacebookInvite);
        Button tvGoogle = ButterKnife.findById(view, R.id.tvGoogleInvite);
        final AlertDialog dialog = new AlertDialog.Builder(activity).setView(view).create();
        dialog.setTitle("Invite via");
        tvFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                inviteViaFacebook();
            }
        });

        tvGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inviteViaGoogle(activity);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void inviteViaGoogle(Context context ) {
        try {
            Intent intent = new AppInviteInvitation.IntentBuilder(context.getString(R.string.invitation_title))
                    .setMessage(context.getString(R.string.invitation_message))
                    .setDeepLink(Uri.parse(activity.getString(R.string.invitation_deep_link)))
                    .setCustomImage(Uri.parse(activity.getString(R.string.invitation_custom_image)))
                    .setCallToActionText(activity.getString(R.string.invitation_cta))
                    .build();
            activity.startActivityForResult(intent, REQUEST_INVITE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//
//    private void inviteViaFacebook() {
//        String appLinkUrl, previewImageUrl;
//
//        appLinkUrl = activity.getString(R.string.facebook_url_app);
//        previewImageUrl = activity.getString(R.string.invitation_custom_image);
//
//        if (AppInviteDialog.canShow()) {
//            AppInviteContent content = new AppInviteContent.Builder()
//                    .setApplinkUrl(appLinkUrl)
//                    .setPreviewImageUrl(previewImageUrl)
//                    .build();
//
//            AppInviteDialog appInviteDialog = new AppInviteDialog(activity);
//            appInviteDialog.registerCallback(mCallbackManager, new FacebookCallback<AppInviteDialog.Result>() {
//                @Override
//                public void onSuccess(AppInviteDialog.Result result) {
//                    Toast.makeText(activity, R.string.invite_success, Toast.LENGTH_LONG).show();
////                    DialogUtilities.getOkAlertDialog(MainActivity.this, R.string.send_invite_friend_success).show();
//                }
//
//                @Override
//                public void onCancel() {
//                }
//
//                @Override
//                public void onError(FacebookException e) {
//                }
//            });
//
//
//            appInviteDialog.show(activity, content);
//        }
//    }

}
