package com.example.dusan.musicapp.listennow;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.dusan.musicapp.Consts;
import com.example.dusan.musicapp.R;


public class ListenNowFragment extends Fragment {
    private static final String TAG = "ListenNowFragment";

    CoordinatorLayout rootLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Defines the xml file for the fragment
        View view = inflater.inflate(R.layout.my_library_fragment_layout, container, false);

        rootLayout = (CoordinatorLayout) view.findViewById(R.id.rootLayout);


        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.listen_now));

        view.findViewById(R.id.loader).setVisibility(View.VISIBLE);

        // get all playlists from server
        String url = Consts.SERVER_URL + Consts.SERVER_ACTION_GET_PLAYLIST +
                "u=" + Consts.USER_NAME + "&p=" + Consts.USER_PASSWORD + "&" + Consts.SERVER_REQUIRED_PARAMETERS;

        Log.e(TAG, url);


        // Setup handles to view objects here
        // etFoo = (EditText) view.findViewById(R.id.etFoo);
        return view;
    }
}
