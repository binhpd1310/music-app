package com.example.dusan.musicapp.common.utilities;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.example.dusan.musicapp.common.models.RecentlySong;
import com.example.dusan.musicapp.playlist.Playlist;
import com.example.dusan.musicapp.playlist.Song;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

/**
 * Created by ${binhpd} on 6/8/2016.
 */
public class RecentlySongHelper {
    // Write
    //--------------------------------
    private static final int MAX_SONGS = 9;
    private static final String FILE_EXTENSION = ".json";
    private static final String fileName = "recentlySong.json";
    private static final String JSON_ID = "id";
    private static final String JSON_TITLE = "title";
    private static final String JSON_AIRTIST = "airticle";
    private static final String JSON_DURATION = "duration";
    private static final String JSON_LOCAL = "local";
    private static final String JSON_ALBUM_ID = "album_id";
    private static final String JSON_PLAYING = "playing";
    private static final String JSON_SONGS = "song";
    private static final String JSON_SONG_INDEX = "index";
    private static final String JSON_PLAY_LIST = "playlist";
    private static final String PLAYLIST_BITMAP = "bitmap";
    private static final String PLAYLIST_COVER = "cover";
    private static final String PLAYLIST_TITLE= "title";
    private static final String PLAYLIST_AIRTIST = "airtist";
    private static final String PLAYLIST_DURATION = "duration";
    private static final String PLAYLIST_NUMBER_OF_SONG = "numbers";
    private static final String PLAYLIST_ALBUM_ID = "id";
    private static final String PLAYLIST_FROM = "from";

    private static String pathFile;

    private static ArrayList<RecentlySong> mRecentlySongs;

    public static void getInstance(Context context) {
        if (TextUtils.isEmpty(pathFile)) {
            try {
                pathFile = context.getExternalFilesDir(null) + fileName;
                mRecentlySongs = new ArrayList<>();
                File fileOutput = new File(pathFile);
                if (!fileOutput.exists()) {
                    fileOutput.createNewFile();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void addSong(RecentlySong recentlySong) {
        for (RecentlySong cusor : mRecentlySongs) {
            if (cusor.getPlaylist().mId.equals(recentlySong.getPlaylist().mId) &&
                    cusor.getPlaylist().mFrom.equals(recentlySong.getPlaylist().mFrom)) {
                mRecentlySongs.remove(cusor);
                break;
            }
        }
        mRecentlySongs.add(0, recentlySong);
    }

    public static void write() {
        try {
            FileOutputStream outputStream = new FileOutputStream(new File(pathFile));
            JSONArray listSong = new JSONArray();
            int size = mRecentlySongs.size();

            for (int i = 0; i < size; i++) {
                if (i < MAX_SONGS) {

                    try {
                        JSONObject node = new JSONObject();
                        node.put(JSON_SONG_INDEX, mRecentlySongs.get(i).getIndex());

                        Song song = mRecentlySongs.get(i).getSongs();
                        JSONObject jsonSong = new JSONObject();

                        jsonSong.put(JSON_ID, song.mId);
                        jsonSong.put(JSON_TITLE, song.mTitle);
                        jsonSong.put(JSON_AIRTIST, song.mArtist);
                        jsonSong.put(JSON_DURATION, song.mDuration);
                        jsonSong.put(JSON_LOCAL, song.mLocal);
                        jsonSong.put(JSON_ALBUM_ID, song.mAlbumId);
                        jsonSong.put(JSON_PLAYING, song.mPlaying);

                        node.put(JSON_SONGS, jsonSong);

                        JSONObject jsonPlayList = new JSONObject();

                        Playlist playlist = mRecentlySongs.get(i).getPlaylist();
                        jsonPlayList.put(PLAYLIST_BITMAP, "");
                        jsonPlayList.put(PLAYLIST_COVER, !TextUtils.isEmpty(playlist.mCoverArt) ? playlist.mCoverArt : "");
                        jsonPlayList.put(PLAYLIST_TITLE, playlist.mTitle);
                        jsonPlayList.put(PLAYLIST_AIRTIST, playlist.mArtist);
                        jsonPlayList.put(PLAYLIST_NUMBER_OF_SONG, playlist.mNumOfSongs);
                        jsonPlayList.put(PLAYLIST_ALBUM_ID, playlist.mId);
                        jsonPlayList.put(PLAYLIST_FROM, playlist.mFrom);

                        node.put(JSON_PLAY_LIST, jsonPlayList);

                        listSong.put(node);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }

            OutputStreamWriter myOutWriter =  new OutputStreamWriter(outputStream);
            myOutWriter.append(listSong.toString());
            myOutWriter.close();

            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static void readObjectFromFile() {
        try {
            JSONArray listSongs = loadListSongFromJsonFile();
            if (listSongs == null) {
                return;
            }
            for (int i = 0; i < listSongs.length(); i++) {
                int index = Integer.valueOf(listSongs.getJSONObject(i).getInt(JSON_SONG_INDEX));

                JSONObject jsonSong = listSongs.getJSONObject(i).getJSONObject(JSON_SONGS);
                Song song = new Song(
                        Integer.valueOf(jsonSong.getString(JSON_ID)),
                        jsonSong.getString(JSON_TITLE),
                        jsonSong.getString(JSON_AIRTIST),
                        jsonSong.getString(JSON_DURATION),
                        jsonSong.getString(JSON_ALBUM_ID),
                        Boolean.valueOf(jsonSong.getString(JSON_LOCAL)));

                JSONObject jsonPlayList = listSongs.getJSONObject(i).getJSONObject(JSON_PLAY_LIST);

                Playlist playlist = new Playlist(
                        null,
                        jsonPlayList.getString(PLAYLIST_COVER),
                        jsonPlayList.getString(PLAYLIST_TITLE),
                        jsonPlayList.getString(PLAYLIST_AIRTIST),
                        jsonPlayList.getString(PLAYLIST_NUMBER_OF_SONG));
                playlist.setFrom(jsonPlayList.getString(PLAYLIST_FROM));
                playlist.mId = (jsonPlayList.getString(PLAYLIST_ALBUM_ID));
                mRecentlySongs.add(new RecentlySong(index, song, playlist));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private static JSONArray loadListSongFromJsonFile() {
        String json;
        try {
            File file = new File(pathFile);
            if (!file.exists()) {
                Log.d(RecentlySongHelper.class.getName(), "File don't exist");
                return null;
            }

            InputStream inputStream = new FileInputStream(file);

            int sizeAvailable = inputStream.available();
            byte[] buffer = new byte[sizeAvailable];
            inputStream.read(buffer);
            inputStream.close();
            json = new String(buffer, "UTF-8");
            return new JSONArray(json);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getFileName() {
        return fileName;
    }

    public static ArrayList<RecentlySong> getRecentlySongs() {
        mRecentlySongs.clear();
        readObjectFromFile();
        return mRecentlySongs;
    }
}
