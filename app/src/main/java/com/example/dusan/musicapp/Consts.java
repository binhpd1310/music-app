package com.example.dusan.musicapp;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * class for app consts
 */
public final class Consts {
    public static final String EXTRAS_INDEX_SONG = "extras_index_song";
    public static  String USER_NAME = "";
    public static  String USER_EMAIL = "";
    public static  String USER_PASSWORD = "";

    public static String SERVER_URL = "http://209.148.82.6:4040/rest/";

    /*
    * server actions string
    * */
    public static final String SERVER_ACTION_CREATE_USER = "createUser.view?";
    public static final String SERVER_ACTION_GET_PLAYLISTS = "getPlaylists.view?";
    public static final String SERVER_ACTION_GET_COVER_ART = "getCoverArt.view?";
    public static final String SERVER_ACTION_GET_PLAYLIST = "getPlaylist.view?";
    public static final String SERVER_ACTION_STREAM = "stream.view?";
    public static final String SERVER_ACTION_CREATE_PLAYLIST = "createPlaylist.view?";
    public static final String SERVER_ACTION_UPDATE_PLAYLIST = "updatePlaylist.view?";
    public static final String SERVER_ACTION_DELETE_PLAYLIST = "deletePlaylist.view?";
    public static final String SERVER_ACTION_GET_INDEXES = "getIndexes.view?";
    public static final String SERVER_ACTION_GET_MUSIC_DIRECTORY = "getMusicDirectory.view?";
    public static final String SERVER_ACTION_GET_ALBUM_LIST = "getAlbumList.view?";




    public static final String SERVER_REQUIRED_PARAMETERS = "v=1.11.0&c=MusicApp&f=json";

    /*
    * admin details
    * */
    public static final String ADMIN_USERNAME = "admin";
    public static final String ADMIN_PASSWORD = "admin";

    // PRIVATE //
    /**
     The caller references the constants using <tt>Consts.EMPTY_STRING</tt>,
     and so on. Thus, the caller should be prevented from constructing objects of
     this class, by declaring this private constructor.
     */
    private Consts(){
        //this prevents even the native class from
        //calling this ctor as well :
        throw new AssertionError();
    }

    public static final String FEATURE_ALBUM_PREFIX = "-feature-";

    public static final String FIRST_LEFT_PLAYLIST_PREFIX = "-first-left-";
    public static final String SECOND_LEFT_PLAYLIST_PREFIX = "-second-left-";
    public static final String FIRST_RIGHT_PLAYLIST_PREFIX = "-first-right-";
    public static final String THIRD_LEFT_PLAYLIST_PREFIX = "-third-left-";
    public static final String FOURTH_LEFT_PLAYLIST_PREFIX = "-fourth-left-";
    public static final String SECOND_RIGHT_PLAYLIST_PREFIX = "-second-right-";

    public static String getCoverArtUrl(String id){
        return Consts.SERVER_URL + Consts.SERVER_ACTION_GET_COVER_ART +
                "u=" + Consts.USER_NAME + "&p=" + Consts.USER_PASSWORD + "&" + Consts.SERVER_REQUIRED_PARAMETERS +
                "&id=" + id;
    }

    public static String getStream(int id){
        return Consts.SERVER_URL + Consts.SERVER_ACTION_STREAM +
                "u=" + Consts.USER_NAME + "&p=" + Consts.USER_PASSWORD + "&" + Consts.SERVER_REQUIRED_PARAMETERS +
                "&id=" + id;
    }

    public static String getCreatePlaylistUrl(String name, String songId){
        String url = "";
        try {
            url = Consts.SERVER_URL + Consts.SERVER_ACTION_CREATE_PLAYLIST +
                    "u=" + Consts.USER_NAME + "&p=" + Consts.USER_PASSWORD + "&" + Consts.SERVER_REQUIRED_PARAMETERS +
                    "&name=" + URLEncoder.encode(name, "utf-8") +
                    "&songId=" + URLEncoder.encode(songId, "utf-8");
        } catch (UnsupportedEncodingException e){
        }
        return url;
    }

    public static String getGetPlaylistsUrl(){
        return Consts.SERVER_URL + Consts.SERVER_ACTION_GET_PLAYLISTS +
                "u=" + Consts.USER_NAME + "&p=" + Consts.USER_PASSWORD + "&" + Consts.SERVER_REQUIRED_PARAMETERS;
    }

    public static String getUpdatePlaylistUril(int playlistId, int songId){
        return Consts.SERVER_URL + Consts.SERVER_ACTION_UPDATE_PLAYLIST +
                "u=" + Consts.USER_NAME + "&p=" + Consts.USER_PASSWORD + "&" + Consts.SERVER_REQUIRED_PARAMETERS +
                "&playlistId="+playlistId + "&songIdToAdd=" + songId;
    }

    public static String getGetPlaylistUrl(String playlistId){
        return Consts.SERVER_URL + Consts.SERVER_ACTION_GET_PLAYLIST +
                "u=" + Consts.USER_NAME + "&p=" + Consts.USER_PASSWORD + "&" + Consts.SERVER_REQUIRED_PARAMETERS +
                "&id=" + playlistId;
    }

    public static String getDeletePlaylistUrl(String playlistId){
        return Consts.SERVER_URL + Consts.SERVER_ACTION_DELETE_PLAYLIST +
                "u=" + Consts.USER_NAME + "&p=" + Consts.USER_PASSWORD + "&" + Consts.SERVER_REQUIRED_PARAMETERS +
                "&id=" + playlistId;
    }

    public static String getGetIndexesUrl(){
        return Consts.SERVER_URL + Consts.SERVER_ACTION_GET_INDEXES +
                "u=" + Consts.USER_NAME + "&p=" + Consts.USER_PASSWORD + "&" + Consts.SERVER_REQUIRED_PARAMETERS;
    }

    public static String getGetMusicDirectoryUrl(String id){
        return Consts.SERVER_URL + Consts.SERVER_ACTION_GET_MUSIC_DIRECTORY +
                "u=" + Consts.USER_NAME + "&p=" + Consts.USER_PASSWORD + "&" + Consts.SERVER_REQUIRED_PARAMETERS +
                "&id=" + id;
    }

    public static String getGetAlbumList(String type, int size){
        return Consts.SERVER_URL + Consts.SERVER_ACTION_GET_ALBUM_LIST +
                "u=" + Consts.USER_NAME + "&p=" + Consts.USER_PASSWORD + "&" + Consts.SERVER_REQUIRED_PARAMETERS +
                "&type=" + type + "&size=" + size;
    }

}
