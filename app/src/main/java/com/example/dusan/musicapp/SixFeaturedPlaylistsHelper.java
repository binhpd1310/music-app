package com.example.dusan.musicapp;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.RequestQueue;
import com.example.dusan.musicapp.common.AppConstainst;
import com.example.dusan.musicapp.player.MusicService;
import com.example.dusan.musicapp.playlist.Playlist;
import com.example.dusan.musicapp.playlist.PlaylistFragment;
import com.example.dusan.musicapp.playlist.Song;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SixFeaturedPlaylistsHelper implements View.OnClickListener{
    public static final String FIRST_LEFT = "FIRST_LEFT";
    public static final String SECOND_LEFT = "SECOND_LEFT";
    public static final String THIRD_LEFT = "THIRD_LEFT";
    public static final String FOURTH_LEFT = "FOURTH_LEFT";
    public static final String FIRST_RIGHT = "FIRST_RIGHT";
    public static final String SECOND_RIGHT = "SECOND_RIGHT";

    public static final String FIRST_LEFT_NAME = "FIRST_LEFT_NAME";
    public static final String SECOND_LEFT_NAME = "SECOND_LEFT_NAME";
    public static final String THIRD_LEFT_NAME = "THIRD_LEFT_NAME";
    public static final String FOURTH_LEFT_NAME = "FOURTH_LEFT_NAME";
    public static final String FIRST_RIGHT_NAME = "FIRST_RIGHT_NAME";
    public static final String SECOND_RIGHT_NAME = "SECOND_RIGHT_NAME";

    private static final String TAG = "SixFeaturedPLHLPR";

    private RequestQueue mRequestQueue;
    private MainActivity mParentContext;

    private FragmentManager mFragmentManager;

    // first left img
    private ImageView mFirstLeftImageView;
    private TextView mTvFirstLeft;
    private int mFirstLeftId;

    // second left
    private ImageView mSecondLeftImageView;
    private TextView mTvSecondLeft;
    private int mSecondLeftId;

    // first right
    private ImageView mFirstRightImageView;
    private TextView mTvFirstRight;
    private int mFirstRightId;

    // third left img
    private ImageView mThirdLeftImageView;
    private TextView mTvThirdLeft;
    private int mThirdLeftId;

    // fourth left
    private ImageView mFourthLeftImageView;
    private TextView mTvFourthLeft;
    private int mFourthLeftId;

    // second right
    private ImageView mSecondRightImageView;
    private TextView mTvSecondRight;

    private int mSecondRightId;
    private SharedPreferences mSharedPref;


    public SixFeaturedPlaylistsHelper(RequestQueue requestQueue, MainActivity parentContext, FragmentManager fragmentManager, SharedPreferences sharedPref){
        mFirstLeftImageView = null;
        mFirstLeftId = -1;

        mSecondLeftImageView = null;
        mSecondLeftId = -1;

        mFirstRightImageView = null;
        mFirstRightId = -1;

        mThirdLeftImageView = null;
        mThirdLeftId = -1;

        mFourthLeftImageView = null;
        mFourthLeftId = -1;

        mSecondRightImageView = null;
        mSecondRightId = -1;

        mRequestQueue = requestQueue;
        mParentContext = parentContext;
        mFragmentManager = fragmentManager;

        this.mSharedPref = sharedPref;
    }

    public void setFirstLeftImageView(ImageView firstLeftImageView, TextView tvFirstLeft){
        mFirstLeftImageView = firstLeftImageView;
        mTvFirstLeft = tvFirstLeft;
        mFirstLeftImageView.setOnClickListener(this);
    }

    public void setSecondLeftImageView(ImageView secondLeftImageView, TextView tvSecondLeft){
        mSecondLeftImageView = secondLeftImageView;
        mTvSecondLeft = tvSecondLeft;
        mSecondLeftImageView.setOnClickListener(this);
    }

    public void setFirstRightImageView(ImageView firstRightImageView, TextView tvFirstRight){
        mFirstRightImageView = firstRightImageView;
        mTvFirstRight = tvFirstRight;
        mFirstRightImageView.setOnClickListener(this);
    }

    public void setThirdLeftImageView(ImageView thirdLeftImageView, TextView tvThirdLeft){
        mThirdLeftImageView = thirdLeftImageView;
        mTvThirdLeft = tvThirdLeft;
        mThirdLeftImageView.setOnClickListener(this);
    }

    public void setFourthLeftImageView(ImageView fourthLeftImageView, TextView tvFourthLeft){
        mFourthLeftImageView = fourthLeftImageView;
        mTvFourthLeft = tvFourthLeft;
        mFourthLeftImageView.setOnClickListener(this);
    }

    public void setSecondRightImageView(ImageView secondRightImageView, TextView tvSecondRight){
        mSecondRightImageView = secondRightImageView;
        mTvSecondRight = tvSecondRight;
        mSecondRightImageView.setOnClickListener(this);
    }

    public void load(){
        new Ajax(mRequestQueue, Consts.getGetIndexesUrl(), new Ajax.AjaxCallbacks() {
            @Override
            public void onSuccess(JSONObject response) {
                try {
                    JSONArray indexes = response.getJSONObject("subsonic-response")
                            .getJSONObject("indexes").getJSONArray("index");
                    JSONObject row;
                    JSONArray artists;
                    for (int i = 0; i < indexes.length(); i++) {
                        row = indexes.getJSONObject(i);

                        artists = row.getJSONArray(AppConstainst.FIELD_ARTIST);

                        for(int j = 0; j < artists.length(); j++){
                            row = artists.getJSONObject(j);
                            String namePlaylist = row.getString(AppConstainst.FIELD_NAME);
                            int id = row.getInt(AppConstainst.FIELD_ID);
                            if(namePlaylist.startsWith(Consts.FIRST_LEFT_PLAYLIST_PREFIX)){
                                loadImg(Position.FIRST_LEFT, "Latest", String.valueOf(id));
                                mFirstLeftId = id;
                            } else if(namePlaylist.startsWith(Consts.SECOND_LEFT_PLAYLIST_PREFIX)){
                                loadImg(Position.SECOND_LEFT, "Vintage", String.valueOf(id));
                                mSecondLeftId = id;
                            } else if(namePlaylist.startsWith(Consts.FIRST_RIGHT_PLAYLIST_PREFIX)){
                                loadImg(Position.FIRST_RIGHT, "Featured", String.valueOf(id));
                                mFirstRightId = id;
                            } else if(namePlaylist.startsWith(Consts.THIRD_LEFT_PLAYLIST_PREFIX)){
                                loadImg(Position.THIRD_LEFT, "Popular", String.valueOf(id));
                                mThirdLeftId = id;
                            } else if(namePlaylist.startsWith(Consts.FOURTH_LEFT_PLAYLIST_PREFIX)){
                                loadImg(Position.FOURTH_LEFT, "Club", String.valueOf(id));
                                mFourthLeftId = id;
                            } else if(namePlaylist.startsWith(Consts.SECOND_RIGHT_PLAYLIST_PREFIX)){
                                loadImg(Position.SECOND_RIGHT, "Top 10", String.valueOf(id));
                                mSecondRightId = id;
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onError(JSONObject response) {
            }
        }).start();
    }

    private void loadImg(Position position, String name, String coverArtId){
        SharedPreferences.Editor editor = mSharedPref.edit();
        if(position == Position.FIRST_LEFT){
//            Picasso.with(mParentContext).load(Consts.getCoverArtUrl(coverArtId))
//                    .into(mFirstLeftImageView);
//            mTvFirstLeft.setText(name);
            mFirstLeftImageView.setImageResource(R.drawable.ic_radio);
            mTvFirstLeft.setText("Radio");
            editor.putString(FIRST_LEFT, coverArtId);
            editor.putString(FIRST_LEFT_NAME, name);
        } else if(position == Position.SECOND_LEFT){
            Picasso.with(mParentContext).load(Consts.getCoverArtUrl(coverArtId))
                    .into(mSecondLeftImageView);
            mTvSecondLeft.setText(name);
            editor.putString(SECOND_LEFT, coverArtId);
            editor.putString(SECOND_LEFT_NAME, name);
        } else if(position == Position.FIRST_RIGHT){
            Picasso.with(mParentContext).load(Consts.getCoverArtUrl(coverArtId))
                    .into(mFirstRightImageView);
            mTvFirstRight.setText(name);
            editor.putString(FIRST_RIGHT, coverArtId);
            editor.putString(FIRST_RIGHT_NAME, name);
        } else if(position == Position.THIRD_LEFT){
            Picasso.with(mParentContext).load(Consts.getCoverArtUrl(coverArtId))
                    .into(mThirdLeftImageView);
            mTvThirdLeft.setText(name);
            editor.putString(THIRD_LEFT, coverArtId);
            editor.putString(THIRD_LEFT_NAME, name);
        } else if(position == Position.FOURTH_LEFT){
            Picasso.with(mParentContext).load(Consts.getCoverArtUrl(coverArtId))
                    .into(mFourthLeftImageView);
            mTvFourthLeft.setText(name);
            editor.putString(FOURTH_LEFT, coverArtId);
            editor.putString(FOURTH_LEFT_NAME, name);
        } else if(position == Position.SECOND_RIGHT){
            Picasso.with(mParentContext).load(Consts.getCoverArtUrl(coverArtId))
                    .into(mSecondRightImageView);
            mTvSecondRight.setText(name);
            editor.putString(SECOND_RIGHT, coverArtId);
            editor.putString(SECOND_RIGHT_NAME, name);
        }
        editor.commit();
    }

    private void openPlaylistFragment(int albumId, final String titlePrefix){
        final MaterialDialog materialDialog = new MaterialDialog.Builder(mParentContext)
                .title(R.string.app_name)
                .content(R.string.loading_dialog_msg)
                .progress(true, 0)
                .cancelable(false)
                .show();

        new Ajax(mRequestQueue,
                Consts.getGetMusicDirectoryUrl(Integer.toString(albumId)),
                new Ajax.AjaxCallbacks() {
                    @Override
                    public void onSuccess(JSONObject response) {
                        try {
                            Bundle bundle = new Bundle();
                            bundle.putString("songTitle",  response.getJSONObject("subsonic-response").getJSONObject("directory").getString("name").replace(titlePrefix, ""));
                            bundle.putString("songArist", mParentContext.getString(R.string.app_name));

                            bundle.putString("coverArt", response.getJSONObject("subsonic-response").getJSONObject("directory").getString("id"));

                            // put which album/playlist/local album is playing
                            bundle.putString("id", Integer.toString(
                                    response.getJSONObject("subsonic-response").getJSONObject("directory").getInt("id")
                            ));
                            bundle.putString("from", Playlist.From.SUBSONIC_ALBUM.toString());

                            ArrayList<Song> songs = new ArrayList<>();
                            JSONObject row;
                            JSONArray entry = response.getJSONObject("subsonic-response").getJSONObject("directory").getJSONArray("child");
                            for (int i=0; i < entry.length(); i++){
                                row = entry.getJSONObject(i);
                                songs.add(new Song(row.getInt("id"),
                                        row.getString("title"), row.has("artist")?row.getString("artist"):"",
                                        Integer.toString(row.getInt("duration")*1000),
                                        row.getString("coverArt"),
                                        false));
                            }
                            bundle.putString("numOfSongs", Integer.toString(songs.size()));

                            bundle.putParcelableArrayList("songs", songs);

                            // Begin the transaction
                            FragmentTransaction ft = mFragmentManager.beginTransaction();
                            ft.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
                            // Replace the contents of the container with the new fragment
                            PlaylistFragment fragment = new PlaylistFragment();
                            fragment.setArguments(bundle);
                            ft.replace(R.id.container, fragment, PlaylistFragment.FRAGMENT_ID);
                            ft.addToBackStack(null);
                            ft.commit();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        materialDialog.hide();
                        materialDialog.dismiss();
                    }
                    @Override
                    public void onError(JSONObject response) {
                        materialDialog.hide();
                        materialDialog.dismiss();
                    }
                }).start();
    }

    @Override
    public void onClick(View v) {
        if (v == mFirstLeftImageView){
            // Log.e(TAG, "mFirstLeftImageView clicked");
//            openPlaylistFragment(mFirstLeftId, Consts.FIRST_LEFT_PLAYLIST_PREFIX);
            mParentContext.openRadioStations();
        } else if( v == mSecondLeftImageView){
            openPlaylistFragment(mSecondLeftId, Consts.SECOND_LEFT_PLAYLIST_PREFIX);
        } else if( v == mFirstRightImageView){
            openPlaylistFragment(mFirstRightId, Consts.FIRST_RIGHT_PLAYLIST_PREFIX);
        } else if( v == mThirdLeftImageView){
            openPlaylistFragment(mThirdLeftId, Consts.THIRD_LEFT_PLAYLIST_PREFIX);
        } else if( v == mFourthLeftImageView){
            openPlaylistFragment(mFourthLeftId, Consts.FOURTH_LEFT_PLAYLIST_PREFIX);
        } else if( v == mSecondRightImageView){
            openPlaylistFragment(mSecondRightId, Consts.SECOND_RIGHT_PLAYLIST_PREFIX);
        }
    }

    private enum Position{
        FIRST_LEFT, SECOND_LEFT, FIRST_RIGHT, THIRD_LEFT, FOURTH_LEFT, SECOND_RIGHT
    }

    /**
     * load cache image of six cover album
     */
    public void loadImageFromCache() {
        if (!TextUtils.isEmpty(mSharedPref.getString(FIRST_LEFT, null))) {
            Picasso.with(mParentContext).load(Consts.getCoverArtUrl(mSharedPref.getString(FIRST_LEFT, null))).
                    into(mFirstLeftImageView);
            Picasso.with(mParentContext).load(Consts.getCoverArtUrl(mSharedPref.getString(SECOND_LEFT, null))).
                    into(mSecondLeftImageView);
            Picasso.with(mParentContext).load(Consts.getCoverArtUrl(mSharedPref.getString(THIRD_LEFT, null))).
                    into(mFirstRightImageView);
            Picasso.with(mParentContext).load(Consts.getCoverArtUrl(mSharedPref.getString(FOURTH_LEFT, null))).
                    into(mThirdLeftImageView);
            Picasso.with(mParentContext).load(Consts.getCoverArtUrl(mSharedPref.getString(FIRST_RIGHT, null))).
                    into(mFourthLeftImageView);
            Picasso.with(mParentContext).load(Consts.getCoverArtUrl(mSharedPref.getString(SECOND_RIGHT, null))).
                    into(mSecondRightImageView);

            mTvFirstLeft.setText(mSharedPref.getString(FIRST_LEFT_NAME, null));
            mTvSecondLeft.setText(mSharedPref.getString(SECOND_LEFT_NAME, null));
            mTvFirstRight.setText(mSharedPref.getString(FIRST_RIGHT_NAME, null));
            mTvThirdLeft.setText(mSharedPref.getString(THIRD_LEFT_NAME, null));
            mTvFourthLeft.setText(mSharedPref.getString(FOURTH_LEFT_NAME, null));
            mTvSecondRight.setText(mSharedPref.getString(SECOND_RIGHT_NAME, null));
        }
    }
}
