package com.example.dusan.musicapp.myplaylist;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.example.dusan.musicapp.Ajax;
import com.example.dusan.musicapp.Consts;
import com.example.dusan.musicapp.R;
import com.example.dusan.musicapp.SubsonicPlaylist;
import com.example.dusan.musicapp.player.MusicService;
import com.example.dusan.musicapp.playlist.Playlist;
import com.example.dusan.musicapp.playlist.PlaylistFragment;
import com.example.dusan.musicapp.playlist.Song;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MyPlaylistFragment extends Fragment
        implements View.OnClickListener, View.OnCreateContextMenuListener{
    private static final String TAG = "MyPlaylistFragment";

    public static final String FRAGMENT_ID = "MyPlaylsitFragment";

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private CoordinatorLayout rootLayout;

    private RelativeLayout mLoader;

    private View mView;

    private RequestQueue mRequestQueue;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Defines the xml file for the fragment
        View view = inflater.inflate(R.layout.my_playlist_fragment_layout, container, false);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.my_playlist));

        rootLayout = (CoordinatorLayout) view.findViewById(R.id.rootLayout);

        mLoader = (RelativeLayout) view.findViewById(R.id.loader);

        mLoader.setVisibility(View.VISIBLE);

        mRequestQueue = Volley.newRequestQueue(getActivity());
        new Ajax(mRequestQueue, Consts.getGetPlaylistsUrl(), new Ajax.AjaxCallbacks(){
            @Override
            public void onSuccess(JSONObject response) {
                try {
                    JSONArray playlists = response.getJSONObject("subsonic-response")
                            .getJSONObject("playlists").getJSONArray("playlist");
                    JSONObject row;

                    ArrayList<SubsonicPlaylist> subsonicPlaylists = new ArrayList<SubsonicPlaylist>();
                    for (int i = 0; i < playlists.length(); i++) {
                        row = playlists.getJSONObject(i);
                        // display only playlists created by user
                        if(!row.getBoolean("public")){
                            subsonicPlaylists.add(new SubsonicPlaylist(row));
                        }
                    }

                    setRecyclerView(subsonicPlaylists);
                } catch (JSONException e) {
                }
            }
            @Override
            public void onError(JSONObject response) {

            }
        }).start();

        mView = view;
        // Setup handles to view objects here
        // etFoo = (EditText) view.findViewById(R.id.etFoo);
        return view;
    }

    private void setRecyclerView(ArrayList<SubsonicPlaylist> playlists){
        // hide loader
        mLoader.setVisibility(View.GONE);

        if (playlists.isEmpty()){
            Snackbar.make(rootLayout, getString(R.string.no_playlist_err_msg), Snackbar.LENGTH_INDEFINITE)
                    .setAction(getString(R.string.close), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                        }
                    })
                    .show();
            return;
        }

        mRecyclerView = (RecyclerView) mView.findViewById(R.id.rec_view_songs);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        mAdapter = new MyPlaylistAdapter(playlists, getActivity(), this);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onClick(View v) {
        final SubsonicPlaylist clickedPlaylist = (SubsonicPlaylist)v.getTag();
        // ignore if there is no tag for current view
        if (clickedPlaylist == null)
            return;

        final MaterialDialog materialDialog = new MaterialDialog.Builder(getActivity())
                .title(R.string.app_name)
                .content(R.string.loading_dialog_msg)
                .progress(true, 0)
                .cancelable(false)
                .show();
        new Ajax(mRequestQueue, Consts.getGetPlaylistUrl(Integer.toString(clickedPlaylist.getId())), new Ajax.AjaxCallbacks(){
            @Override
            public void onSuccess(JSONObject response) {
                try {
                    JSONObject playlist = response.getJSONObject("subsonic-response").getJSONObject("playlist");

                    Bundle bundle = new Bundle();
                    bundle.putString("songTitle", playlist.getString("name"));
                    bundle.putString("songArist", playlist.getString("owner"));
                    bundle.putString("numOfSongs", playlist.getString("songCount"));
                    bundle.putString("coverArt", playlist.getString("coverArt"));

                    bundle.putString("id", Integer.toString(clickedPlaylist.getId()));
                    bundle.putString("from", Playlist.From.SUBSONIC_PLAYLIST.toString());

                    ArrayList<Song> songs = new ArrayList<>();
                    JSONObject row;
                    JSONArray entry = playlist.getJSONArray("entry");
                    for (int i=0; i < entry.length(); i++){
                        row = entry.getJSONObject(i);
                        songs.add(new Song(row.getInt("id"),
                                row.getString("title"), row.has("artist")?row.getString("artist"):"",
                                Integer.toString(row.getInt("duration")*1000),
                                clickedPlaylist.getCoverArt(),
                                false));
                    }

                    bundle.putParcelableArrayList("songs", songs);
                    // Begin the transaction
                    FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                    ft.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
                    // Replace the contents of the container with the new fragment
                    PlaylistFragment fragment = new PlaylistFragment();
                    fragment.setArguments(bundle);
                    ft.replace(R.id.container, fragment, PlaylistFragment.FRAGMENT_ID);
                    ft.addToBackStack(null);
                    ft.commit();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                materialDialog.hide();
                materialDialog.dismiss();

            }
            @Override
            public void onError(JSONObject response) {
                materialDialog.hide();
                materialDialog.dismiss();
            }
        }).start();
    }

    SubsonicPlaylist mLastLongTappedPlaylist = null;

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        mLastLongTappedPlaylist = (SubsonicPlaylist) v.getTag();

        MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.playlist_popup_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.delete_playlist:
                deleteLastLongTappedPlaylist();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void deleteLastLongTappedPlaylist(){
        if(mLastLongTappedPlaylist != null){
            // delete playlist
            new Ajax(mRequestQueue,
                    Consts.getDeletePlaylistUrl(Integer.toString(mLastLongTappedPlaylist.getId())),
                    new Ajax.AjaxCallbacks(){
                        @Override
                        public void onSuccess(JSONObject response) {

                            // reload current fragment
                            MyPlaylistFragment frg;
                            frg = (MyPlaylistFragment) getActivity().getSupportFragmentManager().findFragmentByTag(MyPlaylistFragment.FRAGMENT_ID);
                            final FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                            ft.detach(frg);
                            ft.attach(frg);
                            ft.commit();

                            Toast.makeText(getActivity(),
                                        getString(R.string.playlist_deleted),
                                        Toast.LENGTH_LONG)
                                    .show();
                        }

                        @Override
                        public void onError(JSONObject response) {
                        }
                    })
            .start();
        }
    }

}
