package com.example.dusan.musicapp.fragments;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.example.dusan.musicapp.R;

/**
 * Created by ${binhpd} on 6/30/2016.
 */
public class BaseFragment extends Fragment {

    private static final String TAG = BaseFragment.class.getName();

    public void replaceFragment(int pLayoutResId, Fragment pFragment) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.anim_right_in, R.anim.anim_left_out, R.anim.anim_left_in, R.anim.anim_right_out);
        final String backStateName = pFragment.getClass().getName();
        fragmentTransaction.addToBackStack(backStateName);
        fragmentTransaction.replace(pLayoutResId, pFragment, backStateName);
        fragmentTransaction.commit();
    }

    public void finishFragment() {
        int backStackCount = getFragmentManager().getBackStackEntryCount();
        if (backStackCount > 0) {
            Log.d(TAG, "popping back stack");
            getFragmentManager().popBackStack();
            if (backStackCount == 1) {
                getActivity().finish();
            }
        } else {
            Log.d(TAG, "nothing on back stack, calling super");
            getActivity().finish();
        }
    }

    public void addFragment(int contentId, Fragment fragment) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        String nameFragment = fragment.getClass().toString();
        fragmentTransaction.setCustomAnimations(
                R.anim.anim_right_in, R.anim.anim_left_out,
                R.anim.anim_left_in, R.anim.anim_right_out).addToBackStack(nameFragment);
        fragmentTransaction.replace(contentId, fragment, nameFragment);
        fragmentTransaction.commit();
    }

    /**
     * get top fragment
     *
     * @return fragment or null
     */
    public Fragment getTopFragment() {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        Fragment topFragment = null;
        int sizeStack = fragmentManager.getBackStackEntryCount();
        if (sizeStack > 0) {
            String fragmentTag = fragmentManager.getBackStackEntryAt(sizeStack - 1).getName();
            topFragment = fragmentManager.findFragmentByTag(fragmentTag);
            fragmentManager.getBackStackEntryAt(sizeStack - 1);
            return topFragment;
        }
        return topFragment;
    }

}
