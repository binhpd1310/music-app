package com.example.dusan.musicapp.homescreensctions;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.example.dusan.musicapp.Ajax;
import com.example.dusan.musicapp.Consts;
import com.example.dusan.musicapp.R;
import com.example.dusan.musicapp.SubsonicDirectory;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;

public class HomeScreenSectionsHelper implements View.OnClickListener{
    private static final String TAG = "HomeScreenSectionsHlpr";

    LinearLayout mParentContainerView;
    Context mParentContext;
    RequestQueue mRequestQueue;
    FragmentManager mFragmentManager;

    public HomeScreenSectionsHelper(LinearLayout parentContainerView, Context parentContext, FragmentManager fragmentManager){
        mParentContainerView = parentContainerView;
        mParentContext = parentContext;
        mFragmentManager = fragmentManager;

        // Instantiate the RequestQueue
        mRequestQueue = Volley.newRequestQueue(mParentContext);
    }

    public void load(){
        new Ajax(mRequestQueue, Consts.getGetIndexesUrl(), new Ajax.AjaxCallbacks() {
            @Override
            public void onSuccess(JSONObject response) {
                try {
                    JSONArray indexes = response.getJSONObject("subsonic-response")
                            .getJSONObject("indexes").getJSONArray("index");
                    JSONObject row;
                    JSONArray artists;

                    ArrayList<SubsonicDirectory> subsonicDirectories = new ArrayList<SubsonicDirectory>();
                    for (int i = 0; i < indexes.length(); i++) {
                        row = indexes.getJSONObject(i);

                        if(row.getString("name").equals("#"))
                            continue;

                        artists = row.getJSONArray("artist");

                        for(int j = 0; j < artists.length(); j++){
                            row = artists.getJSONObject(j);
                            subsonicDirectories.add(new SubsonicDirectory(row.getInt("id"), row.getString("name")));
                        }
                    }
                    displaySections(subsonicDirectories);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onError(JSONObject response) {
            }
        }).start();
    }

    private void displaySections(ArrayList<SubsonicDirectory> subDirectories) {
        for (final SubsonicDirectory dir:subDirectories){
            Log.e(TAG, "displaySections " + dir.getId() + " " + dir.getName());
            new Ajax(mRequestQueue, Consts.getGetMusicDirectoryUrl(Integer.toString(dir.getId())), new Ajax.AjaxCallbacks() {
                @Override
                public void onSuccess(JSONObject response) {
                    try {
                        JSONArray dirChildern = response.getJSONObject("subsonic-response")
                                .getJSONObject("directory").getJSONArray("child");
                        JSONObject row;

                        ArrayList<SubsonicDirectory> subsonicDirectories = new ArrayList<SubsonicDirectory>();
                        for (int i = 0; i < dirChildern.length(); i++) {
                            row = dirChildern.getJSONObject(i);

                            subsonicDirectories.add(
                                    new SubsonicDirectory(row.getInt("id"), row.getString("title"))
                            );

                            Log.e(TAG, row.getString("title"));
                        }
                        Log.e(TAG, "------");
                        displaySection(dir, subsonicDirectories);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                @Override
                public void onError(JSONObject response) {
                }
            }).start();
        }
    }


    private void displaySection(SubsonicDirectory parentDir, ArrayList<SubsonicDirectory> childrenDir){


        View recommendedForYouView = LayoutInflater.from(mParentContext)
                .inflate(R.layout.main_view_recommended_for_you_row, null);

        recommendedForYouView.setTag(childrenDir);

        ((TextView) recommendedForYouView.findViewById(R.id.title)).setText(parentDir.getName());
        ((TextView) recommendedForYouView.findViewById(R.id.title_small)).setText(parentDir.getName());

        // shuffle subDirectories to randomize them
        Collections.shuffle(childrenDir);

        // use first 4
        Picasso.with(mParentContext).load(Consts.getCoverArtUrl(Integer.toString(childrenDir.get(0).getId())))
                .into(((ImageView) recommendedForYouView.findViewById(R.id.first_left)));

        Picasso.with(mParentContext).load(Consts.getCoverArtUrl(Integer.toString(childrenDir.get(1).getId())))
                .into(((ImageView) recommendedForYouView.findViewById(R.id.first_right)));

        Picasso.with(mParentContext).load(Consts.getCoverArtUrl(Integer.toString(childrenDir.get(2).getId())))
                .into(((ImageView) recommendedForYouView.findViewById(R.id.second_left)));

        Picasso.with(mParentContext).load(Consts.getCoverArtUrl(Integer.toString(childrenDir.get(3).getId())))
                .into(((ImageView) recommendedForYouView.findViewById(R.id.second_right)));

        // use 5th as featured recommended
        Picasso.with(mParentContext).load(Consts.getCoverArtUrl(Integer.toString(childrenDir.get(4).getId())))
                .into(((ImageView) recommendedForYouView.findViewById(R.id.circleView)));
        ((TextView) recommendedForYouView.findViewById(R.id.album_title)).setText(childrenDir.get(4).getName());

        mParentContainerView.addView(recommendedForYouView);
        recommendedForYouView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("subDirectories", (ArrayList<SubsonicDirectory>) v.getTag());

        // Begin the transaction
        FragmentTransaction ft = mFragmentManager.beginTransaction();
        // ft.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        // Replace the contents of the container with the new fragment
        HomeScreenSectionsFragment fragment = new HomeScreenSectionsFragment();
        fragment.setArguments(bundle);
        ft.replace(R.id.container, fragment, HomeScreenSectionsFragment.FRAGMENT_ID);
        ft.addToBackStack(null);
        ft.commit();
    }
}
