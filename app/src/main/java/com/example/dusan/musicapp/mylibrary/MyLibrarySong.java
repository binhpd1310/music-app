package com.example.dusan.musicapp.mylibrary;

public class MyLibrarySong{
    public int mId;
    public String mTitle;
    public String mAuthor;

    MyLibrarySong(int id, String title, String author){
        mId = id;
        mTitle = title;
        mAuthor = author;
    }
}
