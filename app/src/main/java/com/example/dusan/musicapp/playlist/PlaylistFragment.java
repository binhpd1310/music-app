package com.example.dusan.musicapp.playlist;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.ecloud.pulltozoomview.PullToZoomScrollViewEx;
import com.example.dusan.musicapp.Consts;
import com.example.dusan.musicapp.MainActivity;
import com.example.dusan.musicapp.R;
import com.example.dusan.musicapp.adapters.PlayListAdapter;
import com.example.dusan.musicapp.adapters.RecyclePlayListAdapter;
import com.example.dusan.musicapp.player.MusicService;
import com.example.dusan.musicapp.view.ExpandedListView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class PlaylistFragment extends Fragment
        implements View.OnClickListener, RecyclePlayListAdapter.PlaylistItemCallback {
    private static final String TAG = "PlaylistFragment";

    public static final String FRAGMENT_ID = "PlaylistFragment";
    // this id is used when playlist fragment is used to display currently playing playlist
    public static final String CURR_PLAYING_FRAGMENT_ID = "CurrPlaying";

    private FloatingActionButton fabBtn;

    private ArrayList<Song> songs;
    private Playlist mPlaylist;
    /**
     * position of song in @songs will be play, default is the first of play list = 1
     */
    private int mIndexOfSong;

    private ExpandedListView mLvPlayList;
    private PlayListAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Bundle bundle = this.getArguments();

        // Defines the xml file for the fragment
        View view = inflater.inflate(R.layout.activity_playlist, container, false);

        loadViewForCode(view);

        DisplayMetrics localDisplayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(localDisplayMetrics);

        mLvPlayList = (ExpandedListView) view.findViewById(R.id.lvPlayList);

        mPlaylist = new Playlist();

        // get id and type of playlist/album/local album
        Log.e(TAG, "id="+ bundle.getString("id") + " from=" + bundle.getString("from"));
        mPlaylist.mId = bundle.getString("id");
        mPlaylist.setFrom(bundle.getString("from"));

        if(bundle.getByteArray("drawable") != null) {
            // if "drawable" is not null we are loading playlist from phone memory
            byte[] bytes = bundle.getByteArray("drawable");
            Bitmap bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
            ((ImageView) view.findViewById(R.id.ivCover)).setImageBitmap(bmp);

            mPlaylist.mBitmap = bmp;
        } else {
            // if "playlistId" is not null we are loading playlist from server,
            //   so load image from url into image view
            Picasso.with(getActivity()).load(Consts.getCoverArtUrl(bundle.getString("coverArt")))
                    .into((ImageView) view.findViewById(R.id.ivCover));

            mPlaylist.mCoverArt = bundle.getString("coverArt");
        }

        songs = bundle.getParcelableArrayList("songs");
        mIndexOfSong = bundle.getInt(Consts.EXTRAS_INDEX_SONG, 0);

        Log.e(TAG, songs.toString());

        // play song
        // if there are no songs in current playlist notify user
        if(songs != null && songs.size() == 0){
//            Snackbar.make(rootLayout, getString(R.string.no_songs_in_playlist_error_msg), Snackbar.LENGTH_INDEFINITE)
//                    .setAction("OK", new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                        }
//                    })
//                    .show();
        } else {
            // play from recently songs
            if (mIndexOfSong > 0) {
                ((MainActivity) getActivity()).getMusicService().processAddToQueueRequest(songs, mPlaylist, mIndexOfSong);
            } else {
                Song currSong = ((MainActivity) getActivity()).getMusicService().getCurrentlyPlayingSong();
                if (currSong != null) {
                    MusicService.State musicServiceState = ((MainActivity) getActivity()).getMusicService().getPlayerState();
                    for (Song s : songs) {
                        // clear all previous playing tags
                        s.mPlaying = false;
                        // == is overwritten
                        if (((MainActivity) getActivity()).getMusicService().isMediaPlayerPlaying() && s.equals(currSong)) {
                            s.mPlaying = true;
                        }
                    }
                }
            }
        }

        mPlaylist.mTitle = bundle.getString("songTitle");
        mPlaylist.mArtist = bundle.getString("songArist");
        mPlaylist.mNumOfSongs = bundle.getString("numOfSongs");

        // set adapter
        mAdapter = new PlayListAdapter(bundle.getString("songTitle"),
                bundle.getString("songArist"),
                bundle.getString("numOfSongs"),
                songs,
                this);
        mLvPlayList.setAdapter(mAdapter);
        mLvPlayList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                onPlaylistItemSelected(position);
            }
        });

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);

        initFAB(view);


        // Setup handles to view objects here
        // etFoo = (EditText) view.findViewById(R.id.etFoo);
        return view;
    }

    @Override
    public void onClick(View v) {
        // fab clicked
        if (fabBtn == v){
            ((MainActivity) getActivity()).getMusicService().processAddToQueueRequest(songs, mPlaylist);
        }
    }

    void initFAB(View view){
        fabBtn = (FloatingActionButton) view.findViewById(R.id.fabBtn);
        fabBtn.setOnClickListener(this);
    }

    @Override
    public void onPlaylistItemSelected(int position) {
        Playlist playlistPlaylist = ((MainActivity) getActivity()).getMusicService().getPlaylist();

        if (playlistPlaylist != null && mPlaylist.mId.equals(playlistPlaylist.mId) && mPlaylist.mFrom == playlistPlaylist.mFrom){
            // jump to a song
            ((MainActivity) getActivity()).getMusicService().setSongPointer(position);
        } else {
            Log.e(TAG, "clickd pos=" + position + " songs.size()=" + songs.size());

            // set all songs
            ((MainActivity) getActivity()).getMusicService().processAddToQueueRequest(songs, mPlaylist, position);
        }
    }

    public void updatePlayingAnimation(Song curSong){
        mAdapter.updatePlayingAnimation(curSong);
    }

    public void pausePlayingAnimation(){
        mAdapter.pausePlayingAnimation();
    }

    private void loadViewForCode(View view) {
        PullToZoomScrollViewEx scrollView = (PullToZoomScrollViewEx) view.findViewById(R.id.scrollView);
        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View headView = layoutInflater.inflate(R.layout.playlist_head_view, null, false);
        View zoomView = layoutInflater.inflate(R.layout.playlist_zoom_view, null, false);
        View contentView = layoutInflater.inflate(R.layout.playlist_content_view, null, false);
        scrollView.setHeaderView(headView);
        scrollView.setZoomView(zoomView);
        scrollView.setScrollContentView(contentView);
        DisplayMetrics localDisplayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(localDisplayMetrics);
        int mScreenHeight = localDisplayMetrics.heightPixels;
        int mScreenWidth = localDisplayMetrics.widthPixels;
        LinearLayout.LayoutParams localObject = new LinearLayout.LayoutParams(mScreenWidth, (int) (9.0F * (mScreenWidth / 16.0F)));
        scrollView.setHeaderLayoutParams(localObject);


    }

    @Override
    public void onDetach() {
        ((MainActivity)getActivity()).reloadRecentlySongs();
        super.onDetach();
    }
}