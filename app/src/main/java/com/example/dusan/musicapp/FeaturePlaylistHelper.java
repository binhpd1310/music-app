package com.example.dusan.musicapp;

public class FeaturePlaylistHelper {
    private String mPlaylistCoverArt;
    private int mPlaylistId;

    public FeaturePlaylistHelper(){
        mPlaylistCoverArt = "-1";
        mPlaylistId = -1;
    }


    void setFeatureCoverArt(String coverArt){
        mPlaylistCoverArt = coverArt;
    }

    String getFeatureCoverArt(){
        return mPlaylistCoverArt;
    }

    void setFeaturePlaylistId(int playlistId){
        mPlaylistId = playlistId;
    }

    int getFeaturePlaylistId(){
        return mPlaylistId;
    }

}
