package com.example.dusan.musicapp.recentactivity;


import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.dusan.musicapp.R;
import com.example.dusan.musicapp.mylibrary.Album;
import com.example.dusan.musicapp.playlist.Song;

import java.util.ArrayList;

public class GetLocalAlbumAndSongsAsyncTask extends AsyncTask<String, Void, Album> {
    private static final String TAG = "GetLocAlbAndSngAT";

    AppCompatActivity mParentAppCompatActivity;
    GetLocalAlbumAndSongsAsyncTaskResponse mCallback;

    Album mAlbum;

    public GetLocalAlbumAndSongsAsyncTask(AppCompatActivity parentAppCompatActivity,
                                          GetLocalAlbumAndSongsAsyncTaskResponse... callback){
        Log.i(TAG, "GetLocalAlbumAndSongsAsyncTask");
        mParentAppCompatActivity = parentAppCompatActivity;
        mCallback = callback[0];
    }


    @Override
    protected Album doInBackground(String... albumids) {
        Log.i(TAG, "protected Album doInBackground(String... albumids)");
        String[] projection = new String[] { MediaStore.Audio.Albums._ID,
                MediaStore.Audio.Albums.ALBUM,
                MediaStore.Audio.Albums.ARTIST,
                MediaStore.Audio.Albums.ALBUM_ART,
                MediaStore.Audio.Albums.NUMBER_OF_SONGS };
        String sortOrder = MediaStore.Audio.Media.ALBUM + " ASC";

        Cursor cursor = mParentAppCompatActivity
                .getContentResolver()
                .query(
                        MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI,
                        projection,
                        MediaStore.Audio.Albums._ID + " = " + albumids[0],
                        null,
                        sortOrder
                );

        if (!cursor.moveToFirst()) {
            // Nothing to query. There is no music on the device. How boring.
            Log.e(TAG, "Failed to move cursor to first row (no query results).");
            return null;
        }

        int albumIdIndex = cursor.getColumnIndex(MediaStore.Audio.Albums._ID);
        int albumNameIndex = cursor.getColumnIndex(MediaStore.Audio.Albums.ALBUM);
        int artistIndex = cursor.getColumnIndex(MediaStore.Audio.Albums.ARTIST);
        int albumArtIndex = cursor.getColumnIndex(MediaStore.Audio.Albums.ALBUM_ART);
        int numOfSongsIndex = cursor.getColumnIndex(MediaStore.Audio.Albums.NUMBER_OF_SONGS);

        Album tmpAlbum;
        do {
            tmpAlbum = new Album(cursor.getInt(albumIdIndex),
                    cursor.getString(albumNameIndex) == null ? cursor.getString(albumNameIndex) : "",
                    cursor.getString(artistIndex).equals("<unknown>") ?
                            mParentAppCompatActivity.getString(R.string.unknown_artist) : cursor.getString(artistIndex),
                    cursor.getString(albumArtIndex) == null ? null : Drawable.createFromPath(cursor.getString(albumArtIndex)),
                        /*Drawable.createFromPath(cursor.getString(albumArtIndex)),*/
                    cursor.getInt(numOfSongsIndex));
        } while (cursor.moveToNext());

        cursor.close();

        return tmpAlbum;
    }

    @Override
    protected void onPostExecute(Album result) {
        Log.i(TAG, "onPostExecute ");
        mAlbum = result;

        new GetLocalMusic().execute(Integer.toString(result.mId));
    }

    private class GetLocalMusic extends AsyncTask<String, Void, ArrayList<Song>>{
        @Override
        protected ArrayList<Song> doInBackground(String... albumids) {
            ArrayList<Song> songs = new ArrayList<>();

            Uri uri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;

            // Perform a query on the content resolver. The URI we're passing specifies that we
            // want to query for all audio media on external storage (e.g. SD card)
            Cursor cur = mParentAppCompatActivity.getContentResolver().query(
                    uri,
                    null,
                    MediaStore.Audio.Media.IS_MUSIC + " = 1 AND " + MediaStore.Audio.Media.ALBUM_ID + " = " + albumids[0],
                    null,
                    MediaStore.Audio.Media.TITLE + " ASC");

            if (cur == null) {
                // Query failed...
                Log.e(TAG, "Failed to retrieve music: cursor is null");
                return null;
            }
            if (!cur.moveToFirst()) {
                // Nothing to query. There is no music on the device. How boring.
                Log.e(TAG, "Failed to move cursor to first row (no query results).");
                return null;
            }

            // retrieve the indices of the columns where the ID, title, etc. of the song are
            int artistColumn = cur.getColumnIndex(MediaStore.Audio.Media.ARTIST);
            int titleColumn = cur.getColumnIndex(MediaStore.Audio.Media.TITLE);
            int albumIdColumn = cur.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID);
            int idColumn = cur.getColumnIndex(MediaStore.Audio.Media._ID);
            int durationColumn = cur.getColumnIndex(MediaStore.Audio.Media.DURATION);

            do {
                songs.add(new Song(
                        cur.getInt(idColumn),
                        cur.getString(titleColumn),
                        cur.getString(artistColumn).equals("<unknown>") ? mParentAppCompatActivity.getString(R.string.unknown_artist) : cur.getString(artistColumn),
                        cur.getString(durationColumn),
                        cur.getString(albumIdColumn)
                ));
            } while (cur.moveToNext());

            cur.close();

            return songs;
        }

        @Override
        protected void onPostExecute(ArrayList<Song> result) {
            mCallback.onPostExecute(mAlbum, result);
        }
    }

    public interface GetLocalAlbumAndSongsAsyncTaskResponse {
        void onPostExecute(Album album, ArrayList<Song> songs);
    }
}