package com.example.dusan.musicapp;

public interface NavigationDrawerCallbacks {
    void onNavigationDrawerItemSelected(int position);
}
