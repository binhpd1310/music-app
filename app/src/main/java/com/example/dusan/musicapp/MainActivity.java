package com.example.dusan.musicapp;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.provider.MediaStore;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.dusan.musicapp.activities.BaseActivity;
import com.example.dusan.musicapp.adapters.RecentlySongGridAdapter;
import com.example.dusan.musicapp.common.models.RecentlySong;
import com.example.dusan.musicapp.common.utilities.AppUtils;
import com.example.dusan.musicapp.common.utilities.NetworkUtils;
import com.example.dusan.musicapp.common.utilities.RecentlySongHelper;
import com.example.dusan.musicapp.featurealbum.FeatureAlbumHelper;
import com.example.dusan.musicapp.fragments.StationRadioListFragment;
import com.example.dusan.musicapp.homescreensctions.HomeScreenSectionsHelper;
import com.example.dusan.musicapp.mylibrary.MyLibraryFragment;
import com.example.dusan.musicapp.myplaylist.MyPlaylistFragment;
import com.example.dusan.musicapp.player.MusicService;
import com.example.dusan.musicapp.playlist.Playlist;
import com.example.dusan.musicapp.playlist.PlaylistFragment;
import com.example.dusan.musicapp.playlist.Song;
import com.example.dusan.musicapp.view.ExpandableHeightGridView;
import com.google.android.gms.appinvite.AppInviteInvitation;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

public class MainActivity extends BaseActivity
        implements NavigationDrawerCallbacks, FragmentManager.OnBackStackChangedListener,
        View.OnClickListener, ServiceConnection, MusicService.Callback, GoogleApiClient.OnConnectionFailedListener,
        View.OnCreateContextMenuListener {
    private static final String TAG = "MainActivity";

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;
    private Toolbar mToolbar;

    /**
     * music player service
     */
    private MusicService mMusicService;

    /**
     * media player UI elements
     */
    ImageButton mMusiPlayerNextBtn;
    ImageButton mMusicPlayerPrevBtn;
    ImageButton mMusicPlayerPlayPauseBtn;
    TextView mSongTitle;
    TextView mArtist;
    ImageView mMusicPlayerImg;
    ProgressBar mMusicPlayerImgLoader;
    SeekBar mMusicPlayerSeekBar;
    RelativeLayout mMusicPlayerMainLoader;
    /**
     * recently grid view
     */
    ExpandableHeightGridView mGvRecentlySong;

    Handler mSeekBarHandler;
    Runnable mSeekBarRunnable;

    SharedPreferences mSharedPref;

    // image fade in animation
    Animation mFadeInAnimation;

    // request queue
    RequestQueue mRequestQueue;

    // use to manage feature playlist
    FeaturePlaylistHelper mFeaturePlaylistHelper;

    // helper for managing six feature playlists (beneath main feature playlist)
    SixFeaturedPlaylistsHelper mSixFeaturePlaylistHelper;

    // handle and process click on popup menu image button click in list view screen
    SongPopupMenuHelper mSongPopupMenuHepler;

    HomeScreenSectionsHelper mRecommendedForYouHelper;

    FeatureAlbumHelper mFeatureAlbumHelper;

    RecentlySongGridAdapter mRecentlySongGridAdapter;
    private ArrayList<RecentlySong> mListRecentlySongs = new ArrayList<>();

    private ProgressDialog mProgressDialog;

    /**
     * instance google api client
     */
    private GoogleApiClient mGoogleApiClient;

    /* Request code used to invoke sign in user interactions. */
    private static final int RC_SIGN_IN = 0;

    /* A flag indicating that a PendingIntent is in progress and prevents
   * us from starting further intents.
   */
    private boolean mIntentInProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.e(TAG, "onCreate");
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        initLayout();
        /**/

        /** set music player buttons click listener */
        mMusiPlayerNextBtn = (ImageButton) findViewById(R.id.music_player_next);
        mMusicPlayerPrevBtn = (ImageButton) findViewById(R.id.music_player_prev);
        mMusicPlayerPlayPauseBtn = (ImageButton) findViewById(R.id.music_player_play_pause);
        mSongTitle = (TextView) findViewById(R.id.music_player_song_name);
        mArtist = (TextView) findViewById(R.id.music_player_artist_name);
        mMusicPlayerMainLoader = (RelativeLayout) findViewById(R.id.music_player_main_loader);

//        mFeatureAlbumFrameLayout = (FrameLayout) findViewById(R.id.feature_album);
//        mFeatureAlbumFrameLayout.setOnClickListener(this);
        // instance is created
        // feature album is retrieved/displayed when google api client is connected
        mFeatureAlbumHelper = new FeatureAlbumHelper((FrameLayout) findViewById(R.id.feature_album), this, mRequestQueue);

        mMusicPlayerImg = (ImageView) findViewById(R.id.music_player_img);
        mFadeInAnimation = AnimationUtils.loadAnimation(this, R.anim.fadein);

        mMusicPlayerImgLoader = (ProgressBar) findViewById(R.id.music_player_img_loader);
        mMusicPlayerSeekBar = (SeekBar) findViewById(R.id.music_player_seek_bar);

        mMusiPlayerNextBtn.setOnClickListener(this);
        mMusicPlayerPrevBtn.setOnClickListener(this);
        mMusicPlayerPlayPauseBtn.setOnClickListener(this);

        mMusicPlayerSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                // rewind player only if change is from user
                if (fromUser)
                    mMusicService.onSeekBarProgressChanged(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        // fill data for grid view recently song
        initGridView();

        // set back stack change listener
        getSupportFragmentManager().addOnBackStackChangedListener(this);

        if(NetworkUtils.isNetworkAvailable(MainActivity.this)){
            createGoogleApiClient();
        } else {
            // there is no internet connection
            // notify user
            new MaterialDialog.Builder(this)
                    .callback(new MaterialDialog.ButtonCallback() {
                        @Override
                        public void onPositive(MaterialDialog dialog) {
//                            Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
//                            startActivity(intent);
                        }
                    })
                    .title(R.string.app_name)
                    .content(R.string.no_internet_connection_txt)
                    .positiveText(R.string.no_internet_connection_positive_btn_txt)
                    .show();

        }

        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);

        // load first six covert album from cache
        mSixFeaturePlaylistHelper.loadImageFromCache();
    }

    private void initGridView() {
        mRecentlySongGridAdapter = new RecentlySongGridAdapter(this, mListRecentlySongs);
        mGvRecentlySong.setExpanded(true);
        mGvRecentlySong.setAdapter(mRecentlySongGridAdapter);
        mGvRecentlySong.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Drawable drawable = ((ImageView)view.findViewById(R.id.ivPoster)).getDrawable();

                RecentlySong recentlySong = mListRecentlySongs.get(position);
                if (recentlySong.getPlaylist().mFrom.equals(Playlist.From.LOCAL_ALBUM)) {
                    new GetLocalMusic(recentlySong.getIndex(), recentlySong.getSongs().mTitle, recentlySong.getSongs().mArtist, recentlySong.getPlaylist().mNumOfSongs, drawable).execute(recentlySong.getSongs().mAlbumId);
                } else {
//                ArrayList<Song> songs = new ArrayList<Song>();
//                songs.add(recentlySong.getSongs());
//                mMusicService.processAddToQueueRequest(songs, recentlySong.getPlaylist(), MusicService.MODE_RECENTLY_SONG);
                    openPlaylistFragment(recentlySong.getIndex(), Integer.valueOf(recentlySong.getPlaylist().mId), recentlySong.getPlaylist().mTitle);
                }
            }
        });
    }

    private void initLayout() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(mToolbar);

        // Instantiate the RequestQueue.
        mRequestQueue = Volley.newRequestQueue(this);

        mSharedPref = getPreferences(Context.MODE_PRIVATE);

        mSixFeaturePlaylistHelper = new SixFeaturedPlaylistsHelper(mRequestQueue, this, getSupportFragmentManager(), mSharedPref);
        mSixFeaturePlaylistHelper.setFirstLeftImageView((ImageView) findViewById(R.id.left_card_view_image0),
                (TextView) findViewById(R.id.tv_left_card_view_0));
        mSixFeaturePlaylistHelper.setSecondLeftImageView((ImageView) findViewById(R.id.left_card_view_image1),
                (TextView) findViewById(R.id.tv_left_card_view_1));
        mSixFeaturePlaylistHelper.setThirdLeftImageView((ImageView) findViewById(R.id.left_card_view_image2),
                (TextView) findViewById(R.id.tv_left_card_view_2));
        mSixFeaturePlaylistHelper.setFourthLeftImageView((ImageView) findViewById(R.id.left_card_view_image3),
                (TextView) findViewById(R.id.tv_left_card_view_3));

        mSixFeaturePlaylistHelper.setFirstRightImageView((ImageView) findViewById(R.id.right_img_view_0),
                (TextView) findViewById(R.id.tv_right_card_view_0));
        mSixFeaturePlaylistHelper.setSecondRightImageView((ImageView) findViewById(R.id.right_img_view_1),
                (TextView) findViewById(R.id.tv_right_card_view_1));

        mSongPopupMenuHepler = new SongPopupMenuHelper(this, mRequestQueue);

        // Instantiate feature playlist helper
        mFeaturePlaylistHelper = new FeaturePlaylistHelper();

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.fragment_drawer);

        // Set up the drawer.
        mNavigationDrawerFragment.setup(R.id.fragment_drawer, (DrawerLayout) findViewById(R.id.drawer), mToolbar);

        LinearLayout main_scroll_view_child = (LinearLayout) findViewById(R.id.main_scroll_view_child);

        mGvRecentlySong = (ExpandableHeightGridView) findViewById(R.id.gvRecentlySong);

        // init recently helper
        RecentlySongHelper.getInstance(this);

        /*
        * add recommended for you
        * */
        // instance is created
        // section is added after google details are retrieved
        mRecommendedForYouHelper = new HomeScreenSectionsHelper(main_scroll_view_child, this, getSupportFragmentManager());
    }

    /**
     * public method for setting seekbar max value
     */
    public void setSeekBarMaxValue(int max) {
        mMusicPlayerSeekBar.setMax(max);
    }

    /**
     * activity callbacks
     */
    @Override
    protected void onResume() {
        Log.e(TAG, "onResume");
//        reloadRecentlySongs();
        super.onResume();
    }

    public void reloadRecentlySongs() {
        mListRecentlySongs.clear();
        mListRecentlySongs.addAll(RecentlySongHelper.getRecentlySongs());
        mRecentlySongGridAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onDestroy() {
        Log.e(TAG, "onDestroy");
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }

        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (NetworkUtils.isNetworkAvailable(MainActivity.this)) {
            OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
            if (opr.isDone()) {
                // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
                // and the GoogleSignInResult will be available instantly.
                Log.d(TAG, "Got cached sign-in");
                GoogleSignInResult result = opr.get();
                handleSignInResult(result);
            } else {
                // If the user has not previously signed in on this device or the sign-in has expired,
                // this asynchronous branch will attempt to sign in the user silently.  Cross-device
                // single sign-on will occur in this branch.
                showProgressDialog();
                opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                    @Override
                    public void onResult(GoogleSignInResult googleSignInResult) {
                        hideProgressDialog();
                        handleSignInResult(googleSignInResult);
                    }
                });
            }
        }

        bindService();
    }

    /** bind to a service */
    private void bindService() {
        Intent bindIntent = new Intent(this, MusicService.class);
        startService(bindIntent);
        bindService(bindIntent, this, 0);
    }

    @Override
    protected void onStop() {
        Log.e(TAG, "onStop");
        super.onStop();

        // if song popup is opened when app is closing clear them
        mSongPopupMenuHepler.clearDialogs();

        /** unbind form service */
        if (mMusicService != null) {
            mMusicService.setCallback(null); // IMPORTANT! to avoid memory leaks
            unbindService(this);
        }
    }

    /** */

    /**
     * event when MusicService is created
     */
    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        Log.e(TAG, "onServiceConnected");
        // get binded service
        mMusicService = ((MusicService.LocalBinder) service).getService();
        mMusicService.setCallback(this);
    }

    /**
     * event when service is disconnected
     */
    @Override
    public void onServiceDisconnected(ComponentName name) {
        mMusicService = null;
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        // Toast.makeText(this, "Menu item selected -> " + position, Toast.LENGTH_SHORT).show();

        if (position == 1) {
            // my library clicked

            // Begin the transaction
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            // Replace the contents of the container with the new fragment
            ft.replace(R.id.container, new MyLibraryFragment());
            ft.addToBackStack(null);
            ft.commit();
        } else if (position == 0) {
            // home clicked

            // remove all fragments from stack
            getSupportFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        } else if (position == 2) {
            // my playlist clicked
            openMyPlayListScreen();


        }
    }

    public void openMyPlayListScreen() {
        // Begin the transaction
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        // Replace the contents of the container with the new fragment
        ft.replace(R.id.container, new MyPlaylistFragment(), MyPlaylistFragment.FRAGMENT_ID);
        ft.addToBackStack(null);
        ft.commit();
    }

    // flag to detect double back press
    private Boolean exit = false;

    @Override
    public void onBackPressed() {
        if (mNavigationDrawerFragment.isDrawerOpen())
            mNavigationDrawerFragment.closeDrawer();
        else {
            if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
                // user is on main page
                if (exit) {
                    // exit app
                    finish();
                } else {
                    Toast.makeText(this, "Press Back again to Exit.",
                            Toast.LENGTH_SHORT).show();
                    exit = true;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            exit = false;
                        }
                    }, 3 * 1000);

                }
            } else {
                // some fragment is opened
                super.onBackPressed();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mNavigationDrawerFragment.getActionBarDrawerToggle().syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mNavigationDrawerFragment.getActionBarDrawerToggle().onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        switch (item.getItemId()) {
            case R.id.action_invite:
                InviteHelper inviteHelper = new InviteHelper(MainActivity.this, null);
                inviteHelper.showInviteDialog();

                break;
        }
        // Handle your other action bar items...

        Log.e("MUSICAPP", "onOptionsItemSelected");

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackStackChanged() {
        int backStackEntryCount = getSupportFragmentManager().getBackStackEntryCount();
        if (backStackEntryCount == 0) {
            // user navigated to main screen by pressing back button
            getSupportActionBar().setTitle("MusicApp");
            // udpate drawer selector
            if (mNavigationDrawerFragment != null)
                mNavigationDrawerFragment.selectHomeItem();

        } else {
            // some fragment is opened
        }
    }

    /**
     * get service object in fragments
     */
    public MusicService getMusicService() {
        return mMusicService;
    }

    @Override
    public void onClick(View v) {
        if (v == mMusiPlayerNextBtn) {
            // send play next to service
            disableMediaPlayerNavButton();
            mMusicService.processPlayNextRequest();
        } else if (v == mMusicPlayerPrevBtn) {
            // send play prev to service
            disableMediaPlayerNavButton();
            mMusicService.processPlayPreviousRequest();
        } else if (v == mMusicPlayerPlayPauseBtn) {
            // send pause to service
            mMusicService.processTogglePlayRequest();
        }
    }

    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }

    public void openRadioStations() {
        StationRadioListFragment stationRadioListFragment = new StationRadioListFragment();
//          // Begin the transaction
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        // Replace the contents of the container with the new fragment
        ft.replace(R.id.container, stationRadioListFragment, StationRadioListFragment.class.getName());
        ft.addToBackStack(null);
        ft.commit();
    }

    /**
     * disable all 3 media player buttons (prev, next and pause)
     * they will be enabled when onPreparedListener() is called from service
     */
    private void disableMediaPlayerNavButton() {
        mMusiPlayerNextBtn.setEnabled(false);
        mMusicPlayerPrevBtn.setEnabled(false);
        mMusicPlayerPlayPauseBtn.setEnabled(false);
    }

    /**
     * custom service listeners
     */

    // if we start a song
    @Override
    public void onSongPlaying(int index, Song song, Playlist playlist) {
        // notify currently playing fragment
        // PlaylistFragment can be opened as CURR_PLAYING_FRAGMENT_ID and FRAGMENT_ID
        // over both cases
        PlaylistFragment playlistFragment = (PlaylistFragment) getSupportFragmentManager()
                .findFragmentByTag(PlaylistFragment.CURR_PLAYING_FRAGMENT_ID);
        if (playlistFragment == null)
            playlistFragment = (PlaylistFragment) getSupportFragmentManager()
                    .findFragmentByTag(PlaylistFragment.FRAGMENT_ID);

        if (playlistFragment != null) {
            // if playlist fragment is opened in any case
            playlistFragment.updatePlayingAnimation(song);
        }

        // notify recent activity helper to store the data
        // add song to recently songs
        ArrayList<Song> recentlySongs = new ArrayList<>();
        recentlySongs.add(getMusicService().getCurrentlyPlayingSong());
        Playlist curPlaylist = getMusicService().getPlaylist();
        // TODO: save recently
        if (curPlaylist.mFrom != Playlist.From.RADIO) {
            RecentlySongHelper.addSong(new RecentlySong(index, song, playlist));
            RecentlySongHelper.write();
        }
//        if (playlist.mId != null && playlist.mFrom != null)
//            mRecentActivityHelper.add(playlist);

        // set seek bar length
        mMusicPlayerSeekBar.setMax(Integer.parseInt(song.mDuration));
        mMusicPlayerSeekBar.setProgress(0);

        mSongTitle.setText(song.mTitle);
        mArtist.setText(song.mArtist);

        // load cover
        if (curPlaylist.mFrom == Playlist.From.RADIO) {
            Song curSong = getMusicService().getCurrentlyPlayingSong();
            AppUtils.loadLogoFromAssets(this, curSong.mLogo, mMusicPlayerImg);
        } else if (song.mLocal) {
            new GetSongPoster().execute(Integer.parseInt(song.mAlbumId));
        } else {
            mMusicPlayerImg.startAnimation(mFadeInAnimation);
            Picasso.with(getApplicationContext()).load(Consts.getCoverArtUrl(song.mAlbumId))
                    .into(mMusicPlayerImg);
        }
    }

    //
    // This method is called when user start the app, but some song is already playing from
    //   background service. In that case just update UI with song which is playing.
    //
    //
    public void setMusicPlayerViewWithPlayingSong() {
        Song song = mMusicService.getCurrentlyPlayingSong();


        if (mMusicService.isMediaPlayerPlaying()) {
            // display pause button
            mMusicPlayerPlayPauseBtn.setImageResource(R.drawable.ic_action_pause);
        } else {
            // display play button
            mMusicPlayerPlayPauseBtn.setImageResource(R.drawable.ic_action_play);
        }


        // set seek bar length
        mMusicPlayerSeekBar.setMax(Integer.parseInt(song.mDuration));
        mMusicPlayerSeekBar.setProgress(0);

        mSongTitle.setText(song.mTitle);
        mArtist.setText(song.mArtist);

        if (song.mLocal)
            new GetSongPoster().execute(Integer.parseInt(song.mAlbumId));
        else {
            mMusicPlayerImg.startAnimation(mFadeInAnimation);
            Picasso.with(getApplicationContext()).load(Consts.getCoverArtUrl(song.mAlbumId))
                    .into(mMusicPlayerImg);
        }
    }

    // method for bitmap to base64
    public static String encodeTobase64(Bitmap image) {
        Bitmap immage = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immage.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);

        Log.d("Image Log:", imageEncoded);
        return imageEncoded;
    }

    // method for base64 to bitmap
    public static Bitmap decodeBase64(String input) {
        byte[] decodedByte = Base64.decode(input, 0);
        return BitmapFactory
                .decodeByteArray(decodedByte, 0, decodedByte.length);
    }

    // song is playing
    @Override
    public void onPlayListener() {
        // update UI
        // if it's playing display pause btn
        mMusicPlayerPlayPauseBtn.setImageResource(R.drawable.ic_action_pause);

        // start play animation
        PlaylistFragment playlistFragment = (PlaylistFragment) getSupportFragmentManager()
                .findFragmentByTag(PlaylistFragment.CURR_PLAYING_FRAGMENT_ID);
        if (playlistFragment == null)
            playlistFragment = (PlaylistFragment) getSupportFragmentManager()
                    .findFragmentByTag(PlaylistFragment.FRAGMENT_ID);

        if (playlistFragment != null) {
            // if playlist fragment is opened in any case
            playlistFragment.updatePlayingAnimation(mMusicService.getCurrentlyPlayingSong());
        }
    }

    @Override
    public void onPreparing() {
        // wait for player to load music from server
        // display loader in music player
        mMusicPlayerMainLoader.setVisibility(View.VISIBLE);
    }

    @Override
    public void onPrepareErrorListener() {
        // disable backword/forward and pause button
        mMusiPlayerNextBtn.setEnabled(true);
        mMusicPlayerPrevBtn.setEnabled(true);
        mMusicPlayerPlayPauseBtn.setEnabled(true);

        // remove loader if there was one
        mMusicPlayerMainLoader.setVisibility(View.GONE);
    }

    // song finished preparing
    @Override
    public void onPreparedListener() {
        // remove loader if there was one
        mMusicPlayerMainLoader.setVisibility(View.GONE);

        // enable backword/forward and pause button
        mMusiPlayerNextBtn.setEnabled(true);
        mMusicPlayerPrevBtn.setEnabled(true);
        mMusicPlayerPlayPauseBtn.setEnabled(true);
    }

    @Override
    public void onProgressChanged(int progress) {
        mMusicPlayerSeekBar.setProgress(progress);
    }

    // song is paused
    @Override
    public void onPauseListener() {
        // update UI
        // if it's pause display play btn
        mMusicPlayerPlayPauseBtn.setImageResource(R.drawable.ic_action_play);

        // pause playing animation
        PlaylistFragment playlistFragment = (PlaylistFragment) getSupportFragmentManager()
                .findFragmentByTag(PlaylistFragment.CURR_PLAYING_FRAGMENT_ID);
        if (playlistFragment == null)
            playlistFragment = (PlaylistFragment) getSupportFragmentManager()
                    .findFragmentByTag(PlaylistFragment.FRAGMENT_ID);

        if (playlistFragment != null) {
            // if playlist fragment is opened in any case
            playlistFragment.pausePlayingAnimation();
        }

    }

    /** end custom service listeners */

    /**
     * async task for fetching song poster
     */
    private class GetSongPoster extends AsyncTask<Integer, Void, Drawable> {
        @Override
        protected void onPreExecute() {
            // display loader instead of img
            mMusicPlayerImgLoader.setVisibility(View.VISIBLE);
            mMusicPlayerImg.setVisibility(View.INVISIBLE);
        }

        @Override
        protected Drawable doInBackground(Integer... params) {
            Drawable drawable;

            Uri uri = MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI;

            String[] projection = new String[]{MediaStore.Audio.Albums.ALBUM_ART};

            // Perform a query on the content resolver. The URI we're passing specifies that we
            // want to query for all audio media on external storage (e.g. SD card)
            Cursor cur = getContentResolver().query(
                    uri,
                    projection,
                    MediaStore.Audio.Albums._ID + " = " + params[0],
                    null,
                    null);

            if (cur == null) {
                // Query failed...
                return null;
            }
            if (!cur.moveToFirst()) {
                // Nothing to query. There is no music on the device. How boring.
                return null;
            }

            int albumArtIndex = cur.getColumnIndex(MediaStore.Audio.Albums.ALBUM_ART);
            do {
                drawable = cur.getString(albumArtIndex) == null ?
                        null : Drawable.createFromPath(cur.getString(albumArtIndex));
            } while (cur.moveToNext());

            cur.close();

            return drawable;
        }

        @Override
        protected void onPostExecute(Drawable drawable) {
            super.onPostExecute(drawable);
            // update UI with received image
            mMusicPlayerImg.startAnimation(mFadeInAnimation);
            if (drawable != null)
                // if we received some valid img display it as poster
                mMusicPlayerImg.setImageDrawable(drawable);
            else
                // if not use default poster img
                mMusicPlayerImg.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.ic_default_song_poster));
            // show image and hide loader
            mMusicPlayerImgLoader.setVisibility(View.GONE);
            mMusicPlayerImg.setVisibility(View.VISIBLE);
        }
    }

    private void createGoogleApiClient() {

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestProfile()
                .build();
        // [END configure_signin]

        // [START build_client]
        // Build a GoogleApiClient with access to the Google Sign-In API and the
        // options specified by gso.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

    }

    private void registerUser() {
        String url = Consts.SERVER_URL + Consts.SERVER_ACTION_CREATE_USER +
                "u=" + Consts.ADMIN_USERNAME + "&p=" + Consts.ADMIN_PASSWORD + "&" + Consts.SERVER_REQUIRED_PARAMETERS +
                "&username=" + Consts.USER_NAME + "&password=" + Consts.USER_PASSWORD + "&email=" + Consts.USER_EMAIL + "&playlistRole=true";

        final MaterialDialog materialDialog = new MaterialDialog.Builder(this)
                .title(R.string.app_name)
                .content(R.string.loading_dialog_msg)
                .progress(true, 0)
                .cancelable(false)
                .show();

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject r = new JSONObject(response);
                            if (r.getJSONObject("subsonic-response").getString("status").equals("ok")) {
                                // user is registered
                                Log.e(TAG, "User registerd");
                            } else {
                                // failed registration
                                // we assume that user is already registered
                                // Log.e(TAG, response);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        materialDialog.hide();
                        materialDialog.dismiss();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "on error register user" + error.getClass());
                materialDialog.hide();
                materialDialog.dismiss();
            }
        });
        // Add the request to the RequestQueue.
        mRequestQueue.add(stringRequest);

    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.e(TAG, "onConnectionFailed");
        if (!mIntentInProgress && result.hasResolution()) {
            try {
                mIntentInProgress = true;
                startIntentSenderForResult(result.getResolution().getIntentSender(),
                        RC_SIGN_IN, null, 0, 0, 0);
            } catch (IntentSender.SendIntentException e) {
                // The intent was canceled before it was sent.  Return to the default
                // state and attempt to connect to get an updated ConnectionResult.
                mIntentInProgress = false;
                mGoogleApiClient.connect();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int responseCode, Intent intent) {
        super.onActivityResult(requestCode, responseCode, intent);
        if (requestCode == RC_SIGN_IN) {
            mIntentInProgress = false;
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(intent);
            handleSignInResult(result);
        } else if (requestCode == InviteHelper.REQUEST_INVITE) {
            if (responseCode == RESULT_OK) {
                // Check how many invitations were sent and log a message
                // The ids array contains the unique invitation ids for each invitation sent
                // (one for each contact select by the user). You can use these for analytics
                // as the ID will be consistent on the sending and receiving devices.
//                String[] ids = AppInviteInvitation.getInvitationIds(resultCode, data);
                showMessage(getString(R.string.send_success));
//                Log.d(TAG, getString(R.string.sent_invitations_fmt, ids.length));
            } else {
                // Sending failed or it was canceled, show failure message to the user
                showMessage(getString(R.string.send_failed));
            }
        }
    }

    private void showMessage(String msg) {
        Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            Log.d("account", acct.getDisplayName() + " email" + acct.getEmail());

            mNavigationDrawerFragment.setUserData(acct.getDisplayName(),
                    acct.getEmail(),
                    acct.getPhotoUrl(),
                    null);
            try {
                Consts.USER_NAME =  URLEncoder.encode(acct.getDisplayName(), "utf-8");
                Consts.USER_EMAIL = URLEncoder.encode(acct.getEmail(), "utf-8");
                Consts.USER_PASSWORD = acct.getId();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
           // user password will be google ID

            registerUser();
            // get and set featured playlist
            // getFeaturedPlaylist();
            mFeatureAlbumHelper.load();

            // load again first six playlist (beneath feature playlist)
            mSixFeaturePlaylistHelper.load();

            // load all other sections
            mRecommendedForYouHelper.load();
        }
    }

    /*
    * function called when three dots in playlist view are clicked (open popup menu(
    * */
    public void openSongPopupMenu(View v) {
        mSongPopupMenuHepler.openPopupMenu(v);
    }

    public void openCurrentlyPlayingPlaylist(View v) {
        // if current stream is steaming radio then open stations ratio screen

        Song song = getMusicService().getCurrentlyPlayingSong();
        Playlist playlist = getMusicService().getPlaylist();
        if (song != null && playlist != null&& playlist.mFrom == Playlist.From.RADIO) {
            addFragment(R.id.container, new StationRadioListFragment());
            return;
        }

        // if user wants to open currently playling playlist but that fragment is already
        //  opened
        // avoid having this fragment multiple times
        if (getSupportFragmentManager()
                .findFragmentByTag(PlaylistFragment.CURR_PLAYING_FRAGMENT_ID) != null)
            return;

        // ignore click if playlist is not loaded
        if (mMusicService == null || mMusicService.getPlaylist() == null)
            return;

        Bundle bundle = new Bundle();
        bundle.putString("songTitle", mMusicService.getPlaylist().mTitle);
        bundle.putString("songArist", mMusicService.getPlaylist().mArtist);
        bundle.putString("numOfSongs", mMusicService.getPlaylist().mNumOfSongs);

        bundle.putString("id", mMusicService.getPlaylist().mId);
        bundle.putString("from", mMusicService.getPlaylist().mFrom.toString());

        if (mMusicService.getPlaylist().mCoverArt != null)
            bundle.putString("coverArt", mMusicService.getPlaylist().mCoverArt);
        else if (mMusicService.getPlaylist().mBitmap != null) {
            // add drawable to bundle
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            mMusicService.getPlaylist().mBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] bytes = stream.toByteArray();
            bundle.putByteArray("drawable", bytes);
        }


        bundle.putParcelableArrayList("songs", mMusicService.getCurrentlyPlayingSongs());
        // Begin the transaction
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        // Replace the contents of the container with the new fragment
        PlaylistFragment fragment = new PlaylistFragment();
        fragment.setArguments(bundle);
        ft.replace(R.id.container, fragment, PlaylistFragment.CURR_PLAYING_FRAGMENT_ID);
        ft.addToBackStack(null);
        ft.commit();

    }


    private void openPlaylistFragment(final int indexSong , int albumId, final String titlePrefix){
        final MaterialDialog materialDialog = new MaterialDialog.Builder(MainActivity.this)
                .title(R.string.app_name)
                .content(R.string.loading_dialog_msg)
                .progress(true, 0)
                .cancelable(false)
                .show();

        new Ajax(mRequestQueue,
                Consts.getGetMusicDirectoryUrl(Integer.toString(albumId)),
                new Ajax.AjaxCallbacks() {
                    @Override
                    public void onSuccess(JSONObject response) {
                        try {
                            Bundle bundle = new Bundle();
                            bundle.putString("songTitle",  response.getJSONObject("subsonic-response").getJSONObject("directory").getString("name").replace(titlePrefix, ""));
                            bundle.putString("songArist", MainActivity.this.getString(R.string.app_name));

                            bundle.putString("coverArt", response.getJSONObject("subsonic-response").getJSONObject("directory").getString("id"));

                            // put which album/playlist/local album is playing
                            bundle.putString("id", Integer.toString(
                                    response.getJSONObject("subsonic-response").getJSONObject("directory").getInt("id")
                            ));
                            bundle.putString("from", Playlist.From.SUBSONIC_ALBUM.toString());

                            ArrayList<Song> songs = new ArrayList<>();
                            JSONObject row;
                            JSONArray entry = response.getJSONObject("subsonic-response").getJSONObject("directory").getJSONArray("child");
                            for (int i=0; i < entry.length(); i++){
                                row = entry.getJSONObject(i);
                                songs.add(new Song(row.getInt("id"),
                                        row.getString("title"), row.has("artist")?row.getString("artist"):"",
                                        Integer.toString(row.getInt("duration")*1000),
                                        row.getString("coverArt"),
                                        false));
                            }
                            bundle.putString("numOfSongs", Integer.toString(songs.size()));
                            bundle.putInt(Consts.EXTRAS_INDEX_SONG, indexSong);
                            bundle.putParcelableArrayList("songs", songs);

                            // Begin the transaction
                            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                            ft.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
                            // Replace the contents of the container with the new fragment
                            PlaylistFragment fragment = new PlaylistFragment();
                            fragment.setArguments(bundle);
                            ft.replace(R.id.container, fragment, PlaylistFragment.FRAGMENT_ID);
                            ft.addToBackStack(null);
                            ft.commit();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        materialDialog.hide();
                        materialDialog.dismiss();
                    }
                    @Override
                    public void onError(JSONObject response) {
                        materialDialog.hide();
                        materialDialog.dismiss();
                    }
                }).start();
    }

    private void openPlaylistFragment(ArrayList<Song> songs, int index, String songTitle, String songArist,
                                      String numOfSongs, Drawable drawablePoster, String albumId){
        Bundle bundle = new Bundle();
        bundle.putString("songTitle", songTitle);
        bundle.putString("songArist", songArist);
        bundle.putString("numOfSongs", numOfSongs);

        // add songs from current album to bundle
        bundle.putParcelableArrayList("songs", songs);

        // put which album/playlist/local album is playing
        bundle.putInt(Consts.EXTRAS_INDEX_SONG, index);
        bundle.putString("id", albumId);
        bundle.putString("from", Playlist.From.LOCAL_ALBUM.toString());

        // add drawable to bundle
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        ((BitmapDrawable)drawablePoster).getBitmap().compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] bytes = stream.toByteArray();
        bundle.putByteArray("drawable", bytes);

        // Begin the transaction
        FragmentTransaction ft = MainActivity.this.getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        // Replace the contents of the container with the new fragment
        PlaylistFragment fragment = new PlaylistFragment();
        fragment.setArguments(bundle);
        ft.replace(R.id.container, fragment, PlaylistFragment.FRAGMENT_ID);
        ft.addToBackStack(null);
        ft.commit();
    }

    /*
  * get user songs on diff thread
   */
    private class GetLocalMusic extends AsyncTask<String, Void, ArrayList<Song>> {
        Drawable mDrawablePoster;
        int mIndex;
        String mSongTitle;
        String mSongArtist;
        String mNumOfSongs;
        String mAlbumId;

        MaterialDialog materialDialog;

        GetLocalMusic(int index, String songTitle, String songArtist, String numOfSongs, Drawable drawablePoster) {
            mIndex = index;
            mSongTitle = songTitle;
            mSongArtist = songArtist;
            mNumOfSongs = numOfSongs;
            mDrawablePoster = drawablePoster;
        }

        @Override
        protected void onPreExecute() {
            // show dialog
            materialDialog = new MaterialDialog.Builder(MainActivity.this)
                    .title(R.string.app_name)
                    .content(R.string.loading_dialog_msg)
                    .progress(true, 0)
                    .cancelable(false)
                    .show();
        }

        @Override
        protected ArrayList<Song> doInBackground(String... albumids) {
            mAlbumId = albumids[0];
            ArrayList<Song> songs = new ArrayList<>();

            Uri uri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;

            // Perform a query on the content resolver. The URI we're passing specifies that we
            // want to query for all audio media on external storage (e.g. SD card)
            Cursor cur = MainActivity.this.getContentResolver().query(
                    uri,
                    null,
                    MediaStore.Audio.Media.IS_MUSIC + " = 1 AND " + MediaStore.Audio.Media.ALBUM_ID + " = " + albumids[0],
                    null,
                    MediaStore.Audio.Media.TITLE + " ASC");

            if (cur == null) {
                // Query failed...
                Log.e(TAG, "Failed to retrieve music: cursor is null");
                return null;
            }
            if (!cur.moveToFirst()) {
                // Nothing to query. There is no music on the device. How boring.
                Log.e(TAG, "Failed to move cursor to first row (no query results).");
                return null;
            }

            // retrieve the indices of the columns where the ID, title, etc. of the song are
            int artistColumn = cur.getColumnIndex(MediaStore.Audio.Media.ARTIST);
            int titleColumn = cur.getColumnIndex(MediaStore.Audio.Media.TITLE);
            int albumIdColumn = cur.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID);
            int idColumn = cur.getColumnIndex(MediaStore.Audio.Media._ID);
            int durationColumn = cur.getColumnIndex(MediaStore.Audio.Media.DURATION);

            do {
                songs.add(new Song(
                        cur.getInt(idColumn),
                        cur.getString(titleColumn),
                        cur.getString(artistColumn).equals("<unknown>") ? getString(R.string.unknown_artist) : cur.getString(artistColumn),
                        cur.getString(durationColumn),
                        cur.getString(albumIdColumn)
                ));
            } while (cur.moveToNext());

            cur.close();

            return songs;
        }

        @Override
        protected void onPostExecute(ArrayList<Song> result) {
            materialDialog.hide();
            materialDialog.dismiss();
            materialDialog = null;
            openPlaylistFragment(result, mIndex, mSongTitle, mSongArtist, mNumOfSongs, mDrawablePoster, mAlbumId);
        }
    }
}
