package com.example.dusan.musicapp;

import org.json.JSONException;
import org.json.JSONObject;

public class SubsonicPlaylist {
    private int mId;
    private String mName;
    private String mOwner;
    private int mSongCount;
    private String mCoverArt;

    public SubsonicPlaylist(JSONObject album){
        try {
            mId = album.has("id") ? album.getInt("id") : -1;
            mName = album.has("name") ? album.getString("name") : "-1";
            mOwner = album.has("owner") ? album.getString("owner") : "-1";
            mSongCount = album.has("songCount") ? album.getInt("songCount") : -1;
            mCoverArt = album.has("coverArt") ? album.getString("coverArt") : "-1";
        } catch (JSONException e) {
        }
    }

    public int getId(){
        return mId;
    }
    public String getName(){
        return mName;
    }
    public String getOwner(){
        return mOwner;
    }
    public int getSongCount(){
        return mSongCount;
    }
    public String getCoverArt(){
        return mCoverArt;
    }
}
