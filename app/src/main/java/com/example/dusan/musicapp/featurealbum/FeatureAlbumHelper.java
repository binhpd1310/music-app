package com.example.dusan.musicapp.featurealbum;


import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.RequestQueue;
import com.example.dusan.musicapp.Ajax;
import com.example.dusan.musicapp.Consts;
import com.example.dusan.musicapp.R;
import com.example.dusan.musicapp.SubsonicDirectory;
import com.example.dusan.musicapp.player.MusicService;
import com.example.dusan.musicapp.playlist.Playlist;
import com.example.dusan.musicapp.playlist.PlaylistFragment;
import com.example.dusan.musicapp.playlist.Song;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FeatureAlbumHelper implements View.OnClickListener{
    private static final String TAG = "FeatureAlbumHelper";

    FrameLayout mFrameLayoutWrapper;
    AppCompatActivity mParentAppCompatActivity;
    RequestQueue mRequestQueue;

    public FeatureAlbumHelper(FrameLayout frameLayout, AppCompatActivity parentActivity, RequestQueue requestQueue){
        mFrameLayoutWrapper = frameLayout;
        mParentAppCompatActivity = parentActivity;
        mRequestQueue = requestQueue;

        mFrameLayoutWrapper.setOnClickListener(this);
    }

    public void load(){
        new Ajax(mRequestQueue, Consts.getGetIndexesUrl(), new Ajax.AjaxCallbacks() {
            @Override
            public void onSuccess(JSONObject response) {
                try {
                    JSONArray indexes = response.getJSONObject("subsonic-response")
                            .getJSONObject("indexes").getJSONArray("index");
                    JSONObject row;
                    JSONArray artists;

                    ArrayList<SubsonicDirectory> subsonicDirectories = new ArrayList<SubsonicDirectory>();
                    for (int i = 0; i < indexes.length(); i++) {
                        row = indexes.getJSONObject(i);

                        artists = row.getJSONArray("artist");

                        for(int j = 0; j < artists.length(); j++){
                            row = artists.getJSONObject(j);
                            if(row.getString("name").startsWith(Consts.FEATURE_ALBUM_PREFIX)){
                                setFeatureSubsonicDir(
                                        new SubsonicDirectory(row.getInt("id"),
                                                row.getString("name").replace(Consts.FEATURE_ALBUM_PREFIX, ""))
                                );
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onError(JSONObject response) {
            }
        }).start();
    }

    private void setFeatureSubsonicDir(SubsonicDirectory dir){
        // display image
        Picasso.with(mParentAppCompatActivity)
                .load(Consts.getCoverArtUrl(Integer.toString(dir.getId())))
                .into((ImageView) mFrameLayoutWrapper.findViewById(R.id.feature_album_img_view));

        // set tag for onclick event
        mFrameLayoutWrapper.setTag(dir);
    }

    @Override
    public void onClick(View v) {
        if( v == mFrameLayoutWrapper){
            // fetch songs from server
            // open it in playlist fragment
            final SubsonicDirectory directory = (SubsonicDirectory) v.getTag();

            final MaterialDialog materialDialog = new MaterialDialog.Builder(mParentAppCompatActivity)
                    .title(R.string.app_name)
                    .content(R.string.loading_dialog_msg)
                    .progress(true, 0)
                    .cancelable(false)
                    .show();

            new Ajax(mRequestQueue,
                    Consts.getGetMusicDirectoryUrl(Integer.toString(directory.getId())),
                    new Ajax.AjaxCallbacks() {
                        @Override
                        public void onSuccess(JSONObject response) {
                            Log.e(TAG, response.toString() );
                            try {
                                Bundle bundle = new Bundle();
                                bundle.putString("songTitle", directory.getName());
                                bundle.putString("songArist", mParentAppCompatActivity.getString(R.string.app_name));

                                bundle.putString("coverArt", response.getJSONObject("subsonic-response").getJSONObject("directory").getString("id"));


                                bundle.putString("id", Integer.toString(
                                        response.getJSONObject("subsonic-response").getJSONObject("directory").getInt("id")
                                ));
                                bundle.putString("from", Playlist.From.SUBSONIC_ALBUM.toString());

                                ArrayList<Song> songs = new ArrayList<>();
                                JSONObject row;
                                JSONArray entry = response.getJSONObject("subsonic-response").getJSONObject("directory").getJSONArray("child");
                                for (int i=0; i < entry.length(); i++){
                                    row = entry.getJSONObject(i);
                                    songs.add(new Song(row.getInt("id"),
                                            row.getString("title"), row.has("artist")?row.getString("artist"):"",
                                            Integer.toString(row.getInt("duration")*1000),
                                            row.getString("coverArt"),
                                            false));
                                }
                                bundle.putString("numOfSongs", Integer.toString(songs.size()));

                                bundle.putParcelableArrayList("songs", songs);

                                // Begin the transaction
                                FragmentTransaction ft = mParentAppCompatActivity.getSupportFragmentManager().beginTransaction();
                                ft.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
                                // Replace the contents of the container with the new fragment
                                PlaylistFragment fragment = new PlaylistFragment();
                                fragment.setArguments(bundle);
                                ft.replace(R.id.container, fragment, PlaylistFragment.FRAGMENT_ID);
                                ft.addToBackStack(null);
                                ft.commit();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            materialDialog.hide();
                            materialDialog.dismiss();
                        }
                        @Override
                        public void onError(JSONObject response) {
                            materialDialog.hide();
                            materialDialog.dismiss();
                        }
                    }).start();
        }
    }
}
