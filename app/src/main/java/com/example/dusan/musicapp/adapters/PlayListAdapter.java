package com.example.dusan.musicapp.adapters;

import android.graphics.drawable.AnimationDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dusan.musicapp.R;
import com.example.dusan.musicapp.playlist.Song;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * Created by ${binhpd} on 5/22/2016.
 */
public class PlayListAdapter extends BaseAdapter {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    private String mPlaylistName;
    private String mPlaylistArtistName;
    private String mNumOfSongs;

    private ArrayList<Song> mSongs;
    private int mPlayingAnimationPosition = -1;

    AnimationDrawable playingAnimation;

    private RecyclePlayListAdapter.PlaylistItemCallback mPlaylistItemCallback;

    public PlayListAdapter(String playlistName, String playlistArtistName, String numOfSongs, ArrayList<Song> songs,
                           RecyclePlayListAdapter.PlaylistItemCallback playlistItemCallback) {
        mPlaylistName = playlistName;
        mPlaylistArtistName = playlistArtistName;
        mNumOfSongs = numOfSongs;
        mSongs = songs;
        mPlaylistItemCallback = playlistItemCallback;
    }

    @Override
    public int getCount() {
        return mSongs.size()+1;
    }

    @Override
    public Object getItem(int position) {
        return mSongs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(
                    position == 0 ? R.layout.playlist_header : R.layout.playlist_item, parent, false);
            holder = new ViewHolder(convertView, position == 0 ? TYPE_HEADER : TYPE_ITEM);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPlaylistItemCallback.onPlaylistItemSelected(position-1);
            }
        });

        if (holder.holderType == TYPE_HEADER) {
            holder.album_title.setText(mPlaylistName);
            holder.artist_name.setText(mPlaylistArtistName);
            holder.album_year_num_songs.setText(mNumOfSongs + " songs");
        } else {
            holder.song_position.setVisibility(View.VISIBLE);
            holder.playing_animation.setVisibility(View.INVISIBLE);

            holder.song_position.setText(Integer.toString(position));

            holder.song_name.setText(mSongs.get(position-1).mTitle);
            holder.song_artist.setText(mSongs.get(position-1).mArtist);

            String ms = String.format("%02d:%02d",
                    TimeUnit.MILLISECONDS.toMinutes(Long.parseLong(mSongs.get(position-1).mDuration)) % TimeUnit.HOURS.toMinutes(1),
                    TimeUnit.MILLISECONDS.toSeconds(Long.parseLong(mSongs.get(position-1).mDuration)) % TimeUnit.MINUTES.toSeconds(1));
            holder.song_duration.setText(ms);

            holder.song_settings.setTag(mSongs.get(position-1));

            if (mSongs.get(position-1).mPlaying) {
                mPlayingAnimationPosition = position-1;

                holder.song_position.setVisibility(View.INVISIBLE);
                holder.playing_animation.setVisibility(View.VISIBLE);

                holder.playing_animation.setBackgroundResource(R.drawable.play_anim);
                playingAnimation = (AnimationDrawable) holder.playing_animation.getBackground();
                playingAnimation.start();
            }
        }
        return convertView;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        int holderType;

        // header views
        TextView album_title;
        TextView artist_name;
        TextView album_year_num_songs;

        // item views
        TextView song_position;
        TextView song_name;
        TextView song_artist;
        TextView song_duration;
        ImageButton song_settings;
        ImageView playing_animation;

        public ViewHolder(View itemView, int viewType) {
            super(itemView);

            // Here we set the appropriate view in accordance with the the view type as passed
            // when the holder object is created
            if (viewType == TYPE_ITEM) {
                song_position = (TextView) itemView.findViewById(R.id.song_position);
                song_name = (TextView) itemView.findViewById(R.id.song_name);
                song_artist = (TextView) itemView.findViewById(R.id.song_artist);
                song_duration = (TextView) itemView.findViewById(R.id.song_duration);
                song_settings = (ImageButton) itemView.findViewById(R.id.song_settings);
                playing_animation = (ImageView) itemView.findViewById(R.id.playing_animation);
                holderType = viewType;
            } else {
                album_title = (TextView) itemView.findViewById(R.id.album_title);
                artist_name = (TextView) itemView.findViewById(R.id.artist_name);
                album_year_num_songs = (TextView) itemView.findViewById(R.id.album_year_num_songs);
                holderType = viewType;
            }
        }
    }

    public void updatePlayingAnimation(Song song){
        for(Song s : mSongs){
            // clear all previous playing tags
            s.mPlaying = false;
            // == is overwritten
            if(s.equals(song)){
                s.mPlaying = true;
            }
        }
        // refresh view
        notifyDataSetChanged();
    }

    public void pausePlayingAnimation(){
        for(Song s : mSongs){
            // clear all previous playing tags
            s.mPlaying = false;
        }
        // refresh view
        notifyDataSetChanged();
    }
}
