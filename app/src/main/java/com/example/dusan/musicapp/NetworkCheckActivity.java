package com.example.dusan.musicapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;

import com.afollestad.materialdialogs.MaterialDialog;

/**
 * Use this activity only to check weather user is connected or not
 * if so move to main activity
 * if not, open settings so he/she can turn wifi/data on
 */
public class NetworkCheckActivity extends Activity {
    private static final String TAG = "NetworkCheckActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();

//        if(isNetworkAvailable()){
            // there is internet connection proceed
            // start main activity
        startActivity(new Intent(this, MainActivity.class));
//        } else {
//            // there is no internet connection
//            // notify user
//            new MaterialDialog.Builder(this)
//                    .callback(new MaterialDialog.ButtonCallback() {
//                        @Override
//                        public void onPositive(MaterialDialog dialog) {
//                            Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
//                            startActivity(intent);
//                        }
//                    })
//                    .title(R.string.app_name)
//                    .content(R.string.no_internet_connection_txt)
//                    .positiveText(R.string.no_internet_connection_positive_btn_txt)
//                    .show();
//
//        }
    }

    /**
     * detect if network is avaliable (is user connected)
     * */
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
