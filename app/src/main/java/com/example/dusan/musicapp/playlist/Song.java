package com.example.dusan.musicapp.playlist;

import android.content.ContentUris;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import com.example.dusan.musicapp.Consts;

public class Song implements Parcelable {
    public int mId;
    public String mTitle;
    public String mArtist;
    public String mDuration;
    public boolean mLocal;
    public String mAlbumId;
    public boolean mPlaying; // flag if song is playing
    public String mStreamUrl;
    public String mLogo;

    /**
     * A constructor that initializes the Song object
     **/
    public Song(int id, String title, String artist, String duration, String albumId){
        mId = id;
        mTitle = title;
        mArtist = artist;
        mDuration = duration;
        mAlbumId = albumId;
        mLocal = true;
        mPlaying = false;
    }

    public Song(int id, String title, String artist, String duration, String albumId, boolean local){
        mId = id;
        mTitle = title;
        mArtist = artist;
        mDuration = duration;
        mAlbumId = albumId;
        mLocal = local;
        mPlaying = false;
    }

    public Song() {

    }

    public Uri getURI() {
        if(mLocal) {
            return ContentUris.withAppendedId(
                    android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, mId);
        } else {
            return Uri.parse(Consts.getStream(mId));
        }
    }

    public String getStreamUrl() {
        return mStreamUrl;
    }

    @Override
    public boolean equals(Object other){
        if (other == null) return false;
        if (other == this) return true;
        if (!(other instanceof Song))return false;



        Song otherSong = (Song)other;
        if (mId == otherSong.mId && mTitle.equals(otherSong.mTitle) && mArtist.equals(otherSong.mArtist)
                && mDuration.equals(otherSong.mDuration) && mAlbumId.equals(otherSong.mAlbumId)
                && mLocal == otherSong.mLocal)
            return true;
        else
            return false;
    }



    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * Storing the Song data to Parcel object
     **/
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mId);
        dest.writeString(mTitle);
        dest.writeString(mArtist);
        dest.writeString(mDuration);
        dest.writeString(mAlbumId);
        dest.writeByte((byte) (mLocal ? 1 : 0));
        dest.writeByte((byte) (mPlaying ? 1 : 0));
    }

    /**
     * Retrieving Song data from Parcel object
     * This constructor is invoked by the method createFromParcel(Parcel source) of
     * the object CREATOR
     **/
    private Song(Parcel in){
        this.mId = in.readInt();
        this.mTitle = in.readString();
        this.mArtist = in.readString();
        this.mDuration = in.readString();
        this.mAlbumId = in.readString();
        this.mLocal = in.readByte() != 0;
        this.mPlaying = in.readByte() != 0;
    }

    public static final Parcelable.Creator<Song> CREATOR = new Parcelable.Creator<Song>() {

        @Override
        public Song createFromParcel(Parcel source) {
            return new Song(source);
        }

        @Override
        public Song[] newArray(int size) {
            return new Song[size];
        }
    };
}