package com.example.dusan.musicapp.adapters;

import android.content.Context;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dusan.musicapp.Consts;
import com.example.dusan.musicapp.R;
import com.example.dusan.musicapp.common.models.RecentlySong;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by lion on 6/21/16.
 */
public class RecentlySongGridAdapter extends BaseAdapter {

    Context mContext;
    ArrayList<RecentlySong> mRecentlySongs;
    LayoutInflater layoutInflater;

    public RecentlySongGridAdapter(Context context, ArrayList<RecentlySong> recentlySongs) {
        mContext = context;
        this.mRecentlySongs = recentlySongs;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mRecentlySongs.size();
    }

    @Override
    public Object getItem(int position) {
        return mRecentlySongs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = layoutInflater.inflate(R.layout.item_song_grid, null);
        RecentlySong recentlySong = mRecentlySongs.get(position);

        // load poster
        ImageView ivPoster = (ImageView) convertView.findViewById(R.id.ivPoster);
        if (recentlySong.getSongs().mLocal) {
            new GetSongPoster(ivPoster).execute(Integer.parseInt(recentlySong.getSongs().mAlbumId));
        } else {
            ivPoster.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.fadein));
            Picasso.with(mContext).load(Consts.getCoverArtUrl(recentlySong.getSongs().mAlbumId))
                    .into(ivPoster);
        }
        ((TextView)convertView.findViewById(R.id.tvSongTitle)).setText(recentlySong.getSongs().mTitle);
        ((TextView)convertView.findViewById(R.id.tvSongArtist)).setText(recentlySong.getSongs().mArtist);
        return convertView;
    }

    private class GetSongPoster extends AsyncTask<Integer, Void, Drawable> {

        ImageView mIv;
        public GetSongPoster(ImageView iv) {
            mIv = iv;
        }
        @Override
        protected void onPreExecute() {
            // display loader instead of img
        }

        @Override
        protected Drawable doInBackground(Integer... params) {
            Drawable drawable;

            Uri uri = MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI;

            String[] projection = new String[]{MediaStore.Audio.Albums.ALBUM_ART};

            // Perform a query on the content resolver. The URI we're passing specifies that we
            // want to query for all audio media on external storage (e.g. SD card)
            Cursor cur = mContext.getContentResolver().query(
                    uri,
                    projection,
                    MediaStore.Audio.Albums._ID + " = " + params[0],
                    null,
                    null);

            if (cur == null) {
                // Query failed...
                return null;
            }
            if (!cur.moveToFirst()) {
                // Nothing to query. There is no music on the device. How boring.
                return null;
            }

            int albumArtIndex = cur.getColumnIndex(MediaStore.Audio.Albums.ALBUM_ART);
            do {
                drawable = cur.getString(albumArtIndex) == null ?
                        null : Drawable.createFromPath(cur.getString(albumArtIndex));
            } while (cur.moveToNext());

            cur.close();

            return drawable;
        }

        @Override
        protected void onPostExecute(Drawable drawable) {
            super.onPostExecute(drawable);
            // update UI with received image
            mIv.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.fadein));
            if (drawable != null)
                // if we received some valid img display it as poster
                mIv.setImageDrawable(drawable);
            else
                // if not use default poster img
                mIv.setImageBitmap(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.ic_default_song_poster));
        }
    }
}
