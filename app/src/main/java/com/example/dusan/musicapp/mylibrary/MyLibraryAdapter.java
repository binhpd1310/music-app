package com.example.dusan.musicapp.mylibrary;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.dusan.musicapp.R;

import java.util.ArrayList;

public class MyLibraryAdapter extends RecyclerView.Adapter<MyLibraryAdapter.ViewHolder>{
    private static final String TAG = "MyLibraryAdapter";

    private ArrayList<Album> mAlbums;

    private static final int NUM_OF_ITEMS_IN_A_ROW = 3;

    View.OnClickListener mListener;

    /*
    * Creating a ViewHolder which extends the RecyclerView View Holder
    * ViewHolder are used to to store the inflated views in order to recycle them
    */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView song_count_left;
        TextView song_count_center;
        TextView song_count_right;

        TextView album_id_left;
        TextView album_id_center;
        TextView album_id_right;

        ImageView poster_img_btn_left;
        ImageView poster_img_btn_center;
        ImageView poster_img_btn_right;

        TextView song_title_left;
        TextView song_title_center;
        TextView song_title_right;

        TextView song_artist_left;
        TextView song_artist_center;
        TextView song_artist_right;


        public ViewHolder(View itemView) {
            super(itemView);

            song_count_left = (TextView) itemView.findViewById(R.id.song_count_left);
            song_count_center = (TextView) itemView.findViewById(R.id.song_count_center);
            song_count_right = (TextView) itemView.findViewById(R.id.song_count_right);

            album_id_left = (TextView) itemView.findViewById(R.id.album_id_left);
            album_id_center = (TextView) itemView.findViewById(R.id.album_id_center);
            album_id_right = (TextView) itemView.findViewById(R.id.album_id_right);

            poster_img_btn_left = (ImageView) itemView.findViewById(R.id.poster_img_btn_left);
            poster_img_btn_center = (ImageView) itemView.findViewById(R.id.poster_img_btn_center);
            poster_img_btn_right = (ImageView) itemView.findViewById(R.id.poster_img_btn_right);

            song_title_left = (TextView) itemView.findViewById(R.id.song_title_left);
            song_title_center = (TextView) itemView.findViewById(R.id.song_title_center);
            song_title_right = (TextView) itemView.findViewById(R.id.song_title_right);

            song_artist_left = (TextView) itemView.findViewById(R.id.song_artist_left);
            song_artist_center = (TextView) itemView.findViewById(R.id.song_artist_center);
            song_artist_right = (TextView) itemView.findViewById(R.id.song_artist_right);
        }


    }

    MyLibraryAdapter(ArrayList<Album> albums, View.OnClickListener listener) {
        mAlbums = albums;
        mListener = listener;
    }


    @Override
    public MyLibraryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.my_library_recycler_view_row, parent, false);
        ViewHolder vhItem = new ViewHolder(v);

        v.findViewById(R.id.left).setOnClickListener(mListener);
        v.findViewById(R.id.center).setOnClickListener(mListener);
        v.findViewById(R.id.right).setOnClickListener(mListener);
        return vhItem;
    }


    /*
    * Next we override a method which is called when the item in a row is needed to be displayed,
    * here the int position tells us item at which position is being constructed to be displayed
    * and the holder id of the holder object tell us which view type is being created 1 for item row
    */
    @Override
    public void onBindViewHolder(MyLibraryAdapter.ViewHolder holder, int position) {

        if (position*NUM_OF_ITEMS_IN_A_ROW+NUM_OF_ITEMS_IN_A_ROW < mAlbums.size()){
            // if it fits in current row
            int cPosition = position*NUM_OF_ITEMS_IN_A_ROW;
            if( mAlbums.get(cPosition).mAlbumArt != null ) {
                holder.poster_img_btn_left.setImageDrawable(mAlbums.get(cPosition).mAlbumArt);
                holder.poster_img_btn_left.setScaleType(ImageView.ScaleType.CENTER_CROP);
            }
            if( mAlbums.get(cPosition + 1).mAlbumArt != null ) {
                holder.poster_img_btn_center.setImageDrawable(mAlbums.get(cPosition + 1).mAlbumArt);
                holder.poster_img_btn_center.setScaleType(ImageView.ScaleType.CENTER_CROP);
            }
            if( mAlbums.get(cPosition + 2).mAlbumArt != null ) {
                holder.poster_img_btn_right.setImageDrawable(mAlbums.get(cPosition + 2).mAlbumArt);
                holder.poster_img_btn_right.setScaleType(ImageView.ScaleType.CENTER_CROP);
            }

            holder.song_artist_left.setText(mAlbums.get(cPosition).mArtist);
            holder.song_artist_center.setText(mAlbums.get(cPosition + 1).mArtist);
            holder.song_artist_right.setText(mAlbums.get(cPosition + 2).mArtist);

            holder.song_title_left.setText(mAlbums.get(cPosition).mAlbumName);
            holder.song_title_center.setText(mAlbums.get(cPosition+1).mAlbumName);
            holder.song_title_right.setText(mAlbums.get(cPosition + 2).mAlbumName);

            // set ids
            holder.album_id_left.setText(Integer.toString(mAlbums.get(cPosition).mId));
            holder.album_id_center.setText(Integer.toString(mAlbums.get(cPosition + 1).mId));
            holder.album_id_right.setText(Integer.toString(mAlbums.get(cPosition + 2).mId));

            // set song counts
            holder.song_count_left.setText(Integer.toString(mAlbums.get(cPosition).mNumberSongs));
            holder.song_count_center.setText(Integer.toString(mAlbums.get(cPosition + 1).mNumberSongs));
            holder.song_count_right.setText(Integer.toString(mAlbums.get(cPosition + 2).mNumberSongs));

        } else if(position*NUM_OF_ITEMS_IN_A_ROW+NUM_OF_ITEMS_IN_A_ROW - mAlbums.size() == 1){
            // there is one too much
            // remove right
            int cPosition = position*NUM_OF_ITEMS_IN_A_ROW;
            if( mAlbums.get(cPosition).mAlbumArt != null ) {
                holder.poster_img_btn_left.setImageDrawable(mAlbums.get(cPosition).mAlbumArt);
                holder.poster_img_btn_left.setScaleType(ImageView.ScaleType.CENTER_CROP);
            }
            if( mAlbums.get(cPosition + 1).mAlbumArt != null ) {
                holder.poster_img_btn_center.setImageDrawable(mAlbums.get(cPosition + 1).mAlbumArt);
                holder.poster_img_btn_center.setScaleType(ImageView.ScaleType.CENTER_CROP);
            }

            holder.song_artist_left.setText(mAlbums.get(cPosition).mArtist);
            holder.song_artist_center.setText(mAlbums.get(cPosition + 1).mArtist);

            holder.song_title_left.setText(mAlbums.get(cPosition).mAlbumName);
            holder.song_title_center.setText(mAlbums.get(cPosition + 1).mAlbumName);

            ((RelativeLayout)holder.poster_img_btn_right.getParent()).setVisibility(View.INVISIBLE);

            // set ids
            holder.album_id_left.setText(Integer.toString(mAlbums.get(cPosition).mId));
            holder.album_id_center.setText(Integer.toString(mAlbums.get(cPosition + 1).mId));

            // set song counts
            holder.song_count_left.setText(Integer.toString(mAlbums.get(cPosition).mNumberSongs));
            holder.song_count_center.setText(Integer.toString(mAlbums.get(cPosition + 1).mNumberSongs));
        } else if(position*NUM_OF_ITEMS_IN_A_ROW+NUM_OF_ITEMS_IN_A_ROW - mAlbums.size() == 2){
            // there is two too much
            // remove right and center
            int cPosition = position*NUM_OF_ITEMS_IN_A_ROW;
            if( mAlbums.get(cPosition).mAlbumArt != null ) {
                holder.poster_img_btn_left.setImageDrawable(mAlbums.get(cPosition).mAlbumArt);
                // set scale type for custom imgs
                holder.poster_img_btn_left.setScaleType(ImageView.ScaleType.CENTER_CROP);
            }
            holder.song_artist_left.setText(mAlbums.get(cPosition).mArtist);
            holder.song_title_left.setText(mAlbums.get(cPosition).mAlbumName);

            ((RelativeLayout)holder.poster_img_btn_center.getParent()).setVisibility(View.INVISIBLE);
            ((RelativeLayout)holder.poster_img_btn_right.getParent()).setVisibility(View.INVISIBLE);

            // set ids
            holder.album_id_left.setText(Integer.toString(mAlbums.get(cPosition).mId));

            // set song counts
            holder.song_count_left.setText(Integer.toString(mAlbums.get(cPosition).mNumberSongs));
        }
    }

    /*
    * This method returns the number of items present in the list
    */
    @Override
    public int getItemCount() {
        return mAlbums.size()/NUM_OF_ITEMS_IN_A_ROW+1; // the number of items in the list will be +1 the titles including the header view.
    }


}