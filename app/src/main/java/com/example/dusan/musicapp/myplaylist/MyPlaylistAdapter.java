package com.example.dusan.musicapp.myplaylist;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.dusan.musicapp.Consts;
import com.example.dusan.musicapp.R;
import com.example.dusan.musicapp.SubsonicPlaylist;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MyPlaylistAdapter extends RecyclerView.Adapter<MyPlaylistAdapter.ViewHolder> {
    private ArrayList<SubsonicPlaylist> mPlaylists;
    private Context mParentContext;

    private MyPlaylistFragment mParent;

    private static final int NUM_ITEMS_PER_ROW = 2;

    // Provide a reference to the views for each data item
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public RelativeLayout leftWrapper;
        public SquareImageView leftImgView;
        public TextView leftTitle;

        public RelativeLayout rightWrapper;
        public SquareImageView rightImgView;
        public TextView rightTitle;

        public ViewHolder(View v) {
            super(v);

            leftWrapper = (RelativeLayout) v.findViewById(R.id.left);
            leftImgView = (SquareImageView) v.findViewById(R.id.left_image);
            leftTitle = (TextView) v.findViewById(R.id.left_title);

            rightWrapper = (RelativeLayout) v.findViewById(R.id.right);
            rightImgView = (SquareImageView) v.findViewById(R.id.right_image);
            rightTitle = (TextView) v.findViewById(R.id.right_title);
        }
    }

    public MyPlaylistAdapter(ArrayList<SubsonicPlaylist> playlists, Context parentContext, MyPlaylistFragment parent){
        mPlaylists = playlists;
        mParentContext = parentContext;
        mParent = parent;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public MyPlaylistAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_playlist_recycler_view_row, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        // there are 2 items per row
        // calculate relative position
        int currPos = position*NUM_ITEMS_PER_ROW;

        if (position*NUM_ITEMS_PER_ROW+NUM_ITEMS_PER_ROW <= mPlaylists.size()) {
            // if there are enough items for current row

            mParent.registerForContextMenu(holder.leftWrapper);
            mParent.registerForContextMenu(holder.rightWrapper);

            holder.leftWrapper.setTag(mPlaylists.get(currPos));
            holder.leftWrapper.setOnClickListener(mParent);
            Picasso.with(mParentContext).load(Consts.getCoverArtUrl(mPlaylists.get(currPos).getCoverArt()))
                    .into(holder.leftImgView);
            holder.leftTitle.setText(mPlaylists.get(currPos).getName());

            holder.rightWrapper.setTag(mPlaylists.get(currPos));
            holder.rightWrapper.setOnClickListener(mParent);
            holder.rightWrapper.setTag(mPlaylists.get(currPos + 1));
            Picasso.with(mParentContext).load(Consts.getCoverArtUrl(mPlaylists.get(currPos + 1).getCoverArt()))
                    .into(holder.rightImgView);
            holder.rightTitle.setText(mPlaylists.get(currPos + 1).getName());

        } else if(position*NUM_ITEMS_PER_ROW+NUM_ITEMS_PER_ROW - mPlaylists.size() == 1){
            // there is one item too much
            // remove right item in this row

            mParent.registerForContextMenu(holder.leftWrapper);

            holder.leftWrapper.setTag(mPlaylists.get(currPos));
            holder.leftWrapper.setOnClickListener(mParent);
            Picasso.with(mParentContext).load(Consts.getCoverArtUrl(mPlaylists.get(currPos).getCoverArt()))
                    .into(holder.leftImgView);
            holder.leftTitle.setText(mPlaylists.get(currPos).getName());

            holder.rightWrapper.setVisibility(View.GONE);

        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        // round to top largest int number
        return (int) Math.ceil((double)mPlaylists.size()/NUM_ITEMS_PER_ROW);
    }

}
