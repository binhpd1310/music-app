package com.example.dusan.musicapp.mylibrary;

import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.dusan.musicapp.Consts;
import com.example.dusan.musicapp.R;
import com.example.dusan.musicapp.player.MusicService;
import com.example.dusan.musicapp.playlist.Playlist;
import com.example.dusan.musicapp.playlist.PlaylistFragment;
import com.example.dusan.musicapp.playlist.Song;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;


public class MyLibraryFragment extends Fragment
        implements View.OnClickListener {
    private static final String TAG = "MyLibraryFragment";

    CoordinatorLayout rootLayout;
    View mView;

    private enum Side {LEFT, CENTER, RIGHT};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Defines the xml file for the fragment
        View view = inflater.inflate(R.layout.my_library_fragment_layout, container, false);

        rootLayout = (CoordinatorLayout) view.findViewById(R.id.rootLayout);
        mView = view;


        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.my_library));


        new GetLocalAlbums().execute();


        // Setup handles to view objects here
        // etFoo = (EditText) view.findViewById(R.id.etFoo);
        return view;
    }

    void setRecyclerView(ArrayList<Album> albums){

        RecyclerView recList = (RecyclerView) mView.findViewById(R.id.rec_view_songs);
        recList.setHasFixedSize(true);

        if(albums != null) {
            // set adapter
            MyLibraryAdapter mAdapter = new MyLibraryAdapter(albums, this);
            recList.setAdapter(mAdapter);
        } else {
            Snackbar.make(rootLayout, getString(R.string.no_albums_err_msg), Snackbar.LENGTH_INDEFINITE)
                    .setAction("Ok", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                        }
                    })
                    .show();
        }

        // set layout manager
        LinearLayoutManager llm = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        // llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);
    }


    /*
    * get user albums on diff thread
     */
    private class GetLocalAlbums extends AsyncTask<Void, Void, ArrayList<Album>>{
        MaterialDialog materialDialog;
        @Override
        protected void onPreExecute() {
            // show dialog
            materialDialog = new MaterialDialog.Builder(getActivity())
                .title(R.string.app_name)
                .content(R.string.loading_dialog_msg)
                .progress(true, 0)
                .cancelable(false)
                .show();
        }

        @Override
        protected ArrayList<Album> doInBackground(Void... params) {
            ArrayList<Album> albums = new ArrayList<>();


            String[] projection = new String[] { MediaStore.Audio.Albums._ID,
                    MediaStore.Audio.Albums.ALBUM,
                    MediaStore.Audio.Albums.ARTIST,
                    MediaStore.Audio.Albums.ALBUM_ART,
                    MediaStore.Audio.Albums.NUMBER_OF_SONGS };
            String sortOrder = MediaStore.Audio.Media.ALBUM + " ASC";

            Cursor cursor = getActivity()
                    .getContentResolver()
                    .query(MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI, projection, null, null, sortOrder);

            if (!cursor.moveToFirst()) {
                // Nothing to query. There is no music on the device. How boring.
                Log.e(TAG, "Failed to move cursor to first row (no query results).");
                return null;
            }

            int albumIdIndex = cursor.getColumnIndex(MediaStore.Audio.Albums._ID);
            int albumNameIndex = cursor.getColumnIndex(MediaStore.Audio.Albums.ALBUM);
            int artistIndex = cursor.getColumnIndex(MediaStore.Audio.Albums.ARTIST);
            int albumArtIndex = cursor.getColumnIndex(MediaStore.Audio.Albums.ALBUM_ART);
            int numOfSongsIndex = cursor.getColumnIndex(MediaStore.Audio.Albums.NUMBER_OF_SONGS);

            do {
                albums.add(new Album(cursor.getInt(albumIdIndex),
                        cursor.getString(albumNameIndex),
                        cursor.getString(artistIndex).equals("<unknown>") ? getString(R.string.unknown_artist) : cursor.getString(artistIndex),
                        cursor.getString(albumArtIndex) == null ? null : Drawable.createFromPath(cursor.getString(albumArtIndex)),
                        /*Drawable.createFromPath(cursor.getString(albumArtIndex)),*/
                        cursor.getInt(numOfSongsIndex)));
            } while (cursor.moveToNext());

            return albums;
        }

        @Override
        protected void onPostExecute(ArrayList<Album> result) {
            materialDialog.hide();
            materialDialog.dismiss();
            materialDialog = null;
            setRecyclerView(result);
        }
    }
    /**/


    @Override
    public void onClick(View v) {

        TextView t;
        Drawable drawablePoster = null;
        Side side = Side.LEFT;

        String songTitle = null,
                songArtist = null,
                numOfSongs = null;

        t = (TextView) v.findViewById(R.id.album_id_left);
        if(t == null) {
            t = (TextView) v.findViewById(R.id.album_id_center);
            side = Side.CENTER;
        }
        if(t == null) {
            t = (TextView) v.findViewById(R.id.album_id_right);
            side = Side.RIGHT;
        }

        switch (side){
            case LEFT:
                drawablePoster = ((ImageView) v.findViewById(R.id.poster_img_btn_left)).getDrawable();
                songTitle = ((TextView) v.findViewById(R.id.song_title_left)).getText().toString();
                songArtist = ((TextView) v.findViewById(R.id.song_artist_left)).getText().toString();
                numOfSongs = ((TextView) v.findViewById(R.id.song_count_left)).getText().toString();
                break;
            case CENTER:
                drawablePoster = ((ImageView) v.findViewById(R.id.poster_img_btn_center)).getDrawable();
                songTitle = ((TextView) v.findViewById(R.id.song_title_center)).getText().toString();
                songArtist = ((TextView) v.findViewById(R.id.song_artist_center)).getText().toString();
                numOfSongs = ((TextView) v.findViewById(R.id.song_count_center)).getText().toString();
                break;
            case  RIGHT:
                drawablePoster = ((ImageView) v.findViewById(R.id.poster_img_btn_right)).getDrawable();
                songTitle = ((TextView) v.findViewById(R.id.song_title_right)).getText().toString();
                songArtist = ((TextView) v.findViewById(R.id.song_artist_right)).getText().toString();
                numOfSongs = ((TextView) v.findViewById(R.id.song_count_right)).getText().toString();
                break;
        }

        new GetLocalMusic(songTitle, songArtist, numOfSongs, drawablePoster).execute(t.getText().toString());
    }

    /*
   * get user songs on diff thread
    */
    private class GetLocalMusic extends AsyncTask<String, Void, ArrayList<Song>>{
        Drawable mDrawablePoster;
        String mSongTitle;
        String mSongArtist;
        String mNumOfSongs;
        String mAlbumId;

        MaterialDialog materialDialog;

        GetLocalMusic(String songTitle, String songArtist, String numOfSongs, Drawable drawablePoster){
            mSongTitle = songTitle;
            mSongArtist = songArtist;
            mNumOfSongs = numOfSongs;
            mDrawablePoster = drawablePoster;
        }

        @Override
        protected void onPreExecute() {
            // show dialog
            materialDialog = new MaterialDialog.Builder(getActivity())
                    .title(R.string.app_name)
                    .content(R.string.loading_dialog_msg)
                    .progress(true, 0)
                    .cancelable(false)
                    .show();
        }

        @Override
        protected ArrayList<Song> doInBackground(String... albumids) {
            mAlbumId = albumids[0];
            ArrayList<Song> songs = new ArrayList<>();

            Uri uri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;

            // Perform a query on the content resolver. The URI we're passing specifies that we
            // want to query for all audio media on external storage (e.g. SD card)
            Cursor cur = getActivity().getContentResolver().query(
                    uri,
                    null,
                    MediaStore.Audio.Media.IS_MUSIC + " = 1 AND " + MediaStore.Audio.Media.ALBUM_ID + " = " + albumids[0],
                    null,
                    MediaStore.Audio.Media.TITLE + " ASC");

            if (cur == null) {
                // Query failed...
                Log.e(TAG, "Failed to retrieve music: cursor is null");
                return null;
            }
            if (!cur.moveToFirst()) {
                // Nothing to query. There is no music on the device. How boring.
                Log.e(TAG, "Failed to move cursor to first row (no query results).");
                return null;
            }

            // retrieve the indices of the columns where the ID, title, etc. of the song are
            int artistColumn = cur.getColumnIndex(MediaStore.Audio.Media.ARTIST);
            int titleColumn = cur.getColumnIndex(MediaStore.Audio.Media.TITLE);
            int albumIdColumn = cur.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID);
            int idColumn = cur.getColumnIndex(MediaStore.Audio.Media._ID);
            int durationColumn = cur.getColumnIndex(MediaStore.Audio.Media.DURATION);

            do {
                songs.add(new Song(
                        cur.getInt(idColumn),
                        cur.getString(titleColumn),
                        cur.getString(artistColumn).equals("<unknown>") ? getString(R.string.unknown_artist) : cur.getString(artistColumn),
                        cur.getString(durationColumn),
                        cur.getString(albumIdColumn)
                ));
            } while (cur.moveToNext());

            cur.close();

            return songs;
        }

        @Override
        protected void onPostExecute(ArrayList<Song> result) {
            materialDialog.hide();
            materialDialog.dismiss();
            materialDialog = null;
            openPlaylistFragment(result, mSongTitle, mSongArtist, mNumOfSongs, mDrawablePoster, mAlbumId);
        }
    }
    /**/

    private void openPlaylistFragment(ArrayList<Song> songs, String songTitle, String songArist,
                                      String numOfSongs, Drawable drawablePoster, String albumId){
        Bundle bundle = new Bundle();
        bundle.putString("songTitle", songTitle);
        bundle.putString("songArist", songArist);
        bundle.putString("numOfSongs", numOfSongs);

        // add songs from current album to bundle
        bundle.putParcelableArrayList("songs", songs);

        // put which album/playlist/local album is playing
        bundle.putString("id", albumId);
        bundle.putString("from", Playlist.From.LOCAL_ALBUM.toString());

        // add drawable to bundle
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        ((BitmapDrawable)drawablePoster).getBitmap().compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] bytes = stream.toByteArray();
        bundle.putByteArray("drawable", bytes);

        // Begin the transaction
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        // Replace the contents of the container with the new fragment
        PlaylistFragment fragment = new PlaylistFragment();
        fragment.setArguments(bundle);
        ft.replace(R.id.container, fragment, PlaylistFragment.FRAGMENT_ID);
        ft.addToBackStack(null);
        ft.commit();
    }
}