package com.example.dusan.musicapp.common.utilities;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by ${binhpd} on 7/2/2016.
 */
public class AppUtils {
    public static void loadLogoFromAssets(Context context, String fileName, ImageView imageView) {
        InputStream ims = null;
        try {
            ims = context.getAssets().open(fileName);
            // load image as Drawable
            Drawable d = Drawable.createFromStream(ims, null);
            imageView.setImageDrawable(d);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
