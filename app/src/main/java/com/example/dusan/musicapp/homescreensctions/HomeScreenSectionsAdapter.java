package com.example.dusan.musicapp.homescreensctions;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.dusan.musicapp.Consts;
import com.example.dusan.musicapp.R;
import com.example.dusan.musicapp.SubsonicDirectory;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class HomeScreenSectionsAdapter extends RecyclerView.Adapter<HomeScreenSectionsAdapter.ViewHolder>{
    ArrayList<SubsonicDirectory> mSubDirectories;
    Context mParentContext;
    HomeScreenSectionsAdapter.Callback mCallback;

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        // each data item is just a string in this case
        public ImageView albumCover;
        public TextView albumName;
        public RelativeLayout wrapper;

        public ViewHolder(View v) {
            super(v);
            v.setOnClickListener(this);
            albumCover = (ImageView) v.findViewById(R.id.circleView);
            albumName = (TextView) v.findViewById(R.id.album_title);
            wrapper = (RelativeLayout) v.findViewById(R.id.wrapper);
        }

        @Override
        public void onClick(View v) {
            mCallback.onDirectoryClicked((SubsonicDirectory) v.getTag());
        }
    }

    public interface Callback{
        void onDirectoryClicked(SubsonicDirectory directory);
    }

    public HomeScreenSectionsAdapter(ArrayList<SubsonicDirectory> subDirs, Context parentContext, Callback... callback){
        mSubDirectories = subDirs;
        mParentContext = parentContext;
        mCallback = callback[0];
    }

    // Create new views (invoked by the layout manager)
    @Override
    public HomeScreenSectionsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recommended_for_you_rec_view_row, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Picasso.with(mParentContext).load(Consts.getCoverArtUrl(Integer.toString(mSubDirectories.get(position).getId())))
                .into(holder.albumCover);
        holder.albumName.setText(mSubDirectories.get(position).getName());

        holder.wrapper.setTag(mSubDirectories.get(position));
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mSubDirectories.size();
    }
}
