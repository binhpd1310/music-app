package com.example.dusan.musicapp.fragments;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dusan.musicapp.MainActivity;
import com.example.dusan.musicapp.R;
import com.example.dusan.musicapp.playlist.Playlist;
import com.example.dusan.musicapp.playlist.Song;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by ${binhpd} on 7/1/2016.
 */
public class RadioStreamingFragment extends BaseFragment implements View.OnClickListener {

    private TextView mTvTitle;
    private ImageView mIvBack;
    private CircleImageView mIbPlay;
    private CircleImageView mIbPause;
    private ImageView mIvLogo;

    private String mUrlStream;
    private String mLogoUrl;
    private String mRadioName;
    private Song mSong;
    private Playlist mPlaylist;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_radio_streaming, null);

        mTvTitle = (TextView) view.findViewById(R.id.tvTitle);
        mIvLogo = (ImageView) view.findViewById(R.id.ivLogo);
        mIvBack = (ImageView) view.findViewById(R.id.ivLeft);
        mIbPlay = (CircleImageView) view.findViewById(R.id.civPlay);
        mIbPause = (CircleImageView) view.findViewById(R.id.civPause);

        mIvBack.setOnClickListener(this);
        mIbPlay.setOnClickListener(this);
        mIbPause.setOnClickListener(this);

        Bundle bundle = getArguments();

        mRadioName = bundle.getString(StationRadioListFragment.KEY_EXTRA_NAME);
        mUrlStream = bundle.getString(StationRadioListFragment.KEY_EXTRA_URL);
        mLogoUrl = bundle.getString(StationRadioListFragment.KEY_EXTRA_LOGO);

        loadLogoFromAssets();

        ((AppCompatActivity)getActivity()).getSupportActionBar().hide();


        mSong = new Song(-1,
                mRadioName,
                "",
                "0",
                "-1",
                false);
        mSong.mStreamUrl = mUrlStream;
        mSong.mLogo = mLogoUrl;
        ArrayList<Song> songs = new ArrayList<>();
        songs.add(mSong);
        mPlaylist = new Playlist(getBitmapFromAsset(getActivity(), mLogoUrl),
                "", "", "", "1");
        mPlaylist.setFrom(Playlist.From.RADIO.toString());

        return view;

    }

    @Override
    public void onResume() {
        super.onResume();
        Song song = ((MainActivity)getActivity()).getMusicService().getCurrentlyPlayingSong();
        Playlist playlist = ((MainActivity)getActivity()).getMusicService().getPlaylist();
        if (song != null && playlist != null&& playlist.mFrom == mPlaylist.mFrom && song.mStreamUrl.equals(mUrlStream)) {
            mIbPause.setVisibility(View.VISIBLE);
            mIbPlay.setVisibility(View.GONE);
        }
    }



    private void loadLogoFromAssets() {
        InputStream ims = null;
        try {
            ims = getActivity().getAssets().open(mLogoUrl);
            // load image as Drawable
            Drawable d = Drawable.createFromStream(ims, null);
            mIvLogo.setImageDrawable(d);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivLeft:
                finishFragment();
                // stop
                break;

            case R.id.civPlay:
                mIbPause.setVisibility(View.VISIBLE);
                mIbPlay.setVisibility(View.GONE);

                ArrayList<Song> songs = new ArrayList<>();
                songs.add(mSong);
                ((MainActivity) getActivity()).getMusicService().processAddToQueueRequest(songs, mPlaylist);

                break;

            case R.id.civPause:
                mIbPlay.setVisibility(View.VISIBLE);
                mIbPause.setVisibility(View.GONE);
                ((MainActivity) getActivity()).getMusicService().processTogglePlayRequest();
                break;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public static Bitmap getBitmapFromAsset(Context context, String filePath) {
        AssetManager assetManager = context.getAssets();

        InputStream istr;
        Bitmap bitmap = null;
        try {
            istr = assetManager.open(filePath);
            bitmap = BitmapFactory.decodeStream(istr);
        } catch (IOException e) {
            // handle exception
        }

        return bitmap;
    }
}

