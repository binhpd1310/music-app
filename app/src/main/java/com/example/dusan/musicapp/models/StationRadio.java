package com.example.dusan.musicapp.models;

/**
 * Created by ${binhpd} on 6/30/2016.
 */
public class StationRadio {
    private String name;
    private String urlLogo;
    private String urlStrem;

    public StationRadio(String name, String urlLogo, String urlStrem) {
        this.name = name;
        this.urlLogo = urlLogo;
        this.urlStrem = urlStrem;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrlLogo() {
        return urlLogo;
    }

    public void setUrlLogo(String urlLogo) {
        this.urlLogo = urlLogo;
    }

    public String getUrlStream() {
        return urlStrem;
    }

    public void setUrlStrem(String urlStrem) {
        this.urlStrem = urlStrem;
    }
}
