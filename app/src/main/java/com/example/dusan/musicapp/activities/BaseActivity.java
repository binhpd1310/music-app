package com.example.dusan.musicapp.activities;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.dusan.musicapp.R;

/**
 * Created by ${binhpd} on 6/30/2016.
 */
public class BaseActivity extends AppCompatActivity {
    private static final String TAG = BaseActivity.class.getSimpleName();

    // Intent keys
    public static final String EXTRA_CHANNEL_ID = "EXTRA_CHANNEL_ID";
    public static final String EXTRA_FRIEND_ID = "EXTRA_FRIEND_ID";
    public static final String EXTRA_STATUS = "EXTRA_STATUS";


    public void addFragment(int contentId, Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        String nameFragment = fragment.getClass().toString();
        fragmentTransaction.add(contentId, fragment, nameFragment);
        fragmentTransaction.addToBackStack(nameFragment);
        fragmentTransaction.commit();
    }

    public void replaceFragment(int pLayoutResId, Fragment pFragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.anim_right_in, R.anim.anim_left_out, R.anim.anim_left_in, R.anim.anim_right_out);
        final String backStateName = pFragment.getClass().getName();
        fragmentTransaction.addToBackStack(backStateName);
        fragmentTransaction.replace(pLayoutResId, pFragment, backStateName);
        fragmentTransaction.commit();
    }

    public void finishFragment() {
        int backStackCount = getFragmentManager().getBackStackEntryCount();
        if (backStackCount > 0) {
            Log.d(TAG, "popping back stack");
            getFragmentManager().popBackStack();
            if (backStackCount == 1) {
                finish();
            }
        } else {
            Log.d(TAG, "nothing on back stack, calling super");
            finish();
        }
    }

    /**
     * get top fragment
     *
     * @return fragment or null
     */
    public Fragment getTopFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment topFragment = null;
        int sizeStack = fragmentManager.getBackStackEntryCount();
        if (sizeStack > 0) {
            String fragmentTag = fragmentManager.getBackStackEntryAt(sizeStack - 1).getName();
            topFragment = fragmentManager.findFragmentByTag(fragmentTag);
            fragmentManager.getBackStackEntryAt(sizeStack - 1);
            return topFragment;
        }
        return topFragment;
    }
}
