package com.example.dusan.musicapp.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.example.dusan.musicapp.R;
import com.example.dusan.musicapp.fragments.StationRadioListFragment;

/**
 * Created by ${binhpd} on 6/30/2016.
 */
public class RadioActivity extends BaseActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_radio);
        StationRadioListFragment stationRadioListFragment = new StationRadioListFragment();
        addFragment(R.id.llContent, stationRadioListFragment);
    }

    @Override
    public void onBackPressed() {
        if (getTopFragment() instanceof StationRadioListFragment) {
            finish();
        } else {
            super.onBackPressed();
        }
    }
}
