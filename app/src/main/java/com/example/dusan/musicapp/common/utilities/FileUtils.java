package com.example.dusan.musicapp.common.utilities;

import android.content.Context;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

/**
 * Created by ${binhpd} on 6/8/2016.
 */
public class FileUtils {
    public static boolean exist(Context context, String filename) {
        File file = new File(context.getFilesDir(), filename);
        return file.exists();
    }

    public static void delete(Context context, String filename) {
        File file = new File(context.getFilesDir(), filename);
        file.delete();
    }

    public static FileInputStream getFileInput(Context context, String filename) {
        try {
            return context.openFileInput(filename);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static FileOutputStream getFileOutput(Context context, String filename) {
        try {
            return context.openFileOutput(filename, Context.MODE_PRIVATE);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

}
