package com.example.dusan.musicapp.recentactivity;

import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.RequestQueue;
import com.example.dusan.musicapp.Ajax;
import com.example.dusan.musicapp.Consts;
import com.example.dusan.musicapp.R;
import com.example.dusan.musicapp.common.models.RecentlySong;
import com.example.dusan.musicapp.common.utilities.RecentlySongHelper;
import com.example.dusan.musicapp.mylibrary.Album;
import com.example.dusan.musicapp.playlist.Playlist;
import com.example.dusan.musicapp.playlist.PlaylistFragment;
import com.example.dusan.musicapp.playlist.Song;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class RecentActivityHelper implements View.OnClickListener,
        GetLocalAlbumAndSongsAsyncTask.GetLocalAlbumAndSongsAsyncTaskResponse{
    private static final String TAG = "RecentActivityHelper";

    LinearLayout mViewWrapper;
    AppCompatActivity mParentAppCompatActivity;
    RequestQueue mRequestQueue;
    SharedPreferences mSharedPref;

    JSONObject mRecActJSON;

    ArrayList<RecentlySong> mRecentlySongs;

    View header;
    ArrayList<View> rows;


    public RecentActivityHelper(LinearLayout viewWrapper, AppCompatActivity parentActivity, RequestQueue requestQueue, SharedPreferences sharedPref){
        mViewWrapper = viewWrapper;
        mParentAppCompatActivity = parentActivity;
        mRequestQueue = requestQueue;
        mSharedPref = sharedPref;

        mRecActJSON = new JSONObject();

        mRecentlySongs = new ArrayList<>();

        header = LayoutInflater.from(mParentAppCompatActivity).inflate(
                R.layout.main_view_recent_activity_section, null);

        rows = new ArrayList<>();
        rows.add(LayoutInflater.from(mParentAppCompatActivity).inflate(
                R.layout.main_view_recent_activity_section_row, null));
        rows.add(LayoutInflater.from(mParentAppCompatActivity).inflate(
                R.layout.main_view_recent_activity_section_row, null));
        rows.add(LayoutInflater.from(mParentAppCompatActivity).inflate(
                R.layout.main_view_recent_activity_section_row, null));

        // set onclick listeners
        // only once!!
        rows.get(0).findViewById(R.id.left).setOnClickListener(this);
        rows.get(0).findViewById(R.id.center).setOnClickListener(this);
        rows.get(0).findViewById(R.id.right).setOnClickListener(this);

        rows.get(1).findViewById(R.id.left).setOnClickListener(this);
        rows.get(1).findViewById(R.id.center).setOnClickListener(this);
        rows.get(1).findViewById(R.id.right).setOnClickListener(this);

        rows.get(2).findViewById(R.id.left).setOnClickListener(this);
        rows.get(2).findViewById(R.id.center).setOnClickListener(this);
        rows.get(2).findViewById(R.id.right).setOnClickListener(this);
    }

    //
    // load json object to mRecActJSON from shared preferences
    //
    //
    public void load(){
        Log.e(TAG, "load()");
        updateArrayList();

        fillWithRandomAlbums();
    }

    //
    // Display albums to user
    //
    //
    private void display(ArrayList<RecentlySong> toDisplayPlaylists){
        Log.e(TAG, "display()");

        populateRows(toDisplayPlaylists);


        // if header and rows are not already attached to the view
        if (header.getParent() == null && rows.get(0).getParent() == null
                && rows.get(1).getParent() == null
                && rows.get(2).getParent() == null) { // this will be false upon view refresh (to add new album to view)
            mViewWrapper.addView(header);
            mViewWrapper.addView(rows.get(0));
            mViewWrapper.addView(rows.get(1));
            mViewWrapper.addView(rows.get(2));

            updateLastPlayedPlaylist(toDisplayPlaylists.get(0).playlist);
        }
    }

    //
    // Use first element from recent activities to display last played playlist
    //
    //
    private void updateLastPlayedPlaylist(Playlist lastPlayedPlaylist){
        Log.e(TAG, "updateLastPlayedPlaylist(Playlist lastPlayedPlaylist)");

        if (lastPlayedPlaylist.mFrom == Playlist.From.LOCAL_ALBUM){
            // get local album detais & songs
            // after this task is finished onPostExecute of implemented interface will be called
            //   with corresponding arguments
            new GetLocalAlbumAndSongsAsyncTask(mParentAppCompatActivity, this)
                    .execute(lastPlayedPlaylist.mId);
        } else if (lastPlayedPlaylist.mFrom == Playlist.From.SUBSONIC_PLAYLIST){

            updateLastPlayedSubsonicPlaylist(lastPlayedPlaylist);

        } else if (lastPlayedPlaylist.mFrom == Playlist.From.SUBSONIC_ALBUM){

            updateLastPlayedSubsonicAlbum(lastPlayedPlaylist);
        }
    }

    private void updateLastPlayedSubsonicAlbum(Playlist lastPlayedPlaylist){
        new Ajax(mRequestQueue,
            Consts.getGetMusicDirectoryUrl(lastPlayedPlaylist.mId),
            new Ajax.AjaxCallbacks() {
                @Override
                public void onSuccess(JSONObject response) {
                    try {

                        ArrayList<Song> songs = new ArrayList<>();
                        JSONObject row;
                        JSONArray entry = response.getJSONObject("subsonic-response").getJSONObject("directory").getJSONArray("child");
                        for (int i=0; i < entry.length(); i++){
                            row = entry.getJSONObject(i);
                            songs.add(new Song(row.getInt("id"),
                                    row.getString("title"), row.has("artist")?row.getString("artist"):"",
                                    Integer.toString(row.getInt("duration")*1000),
                                    row.getString("coverArt"),
                                    false));
                        }

                        // get playlist
                        Playlist pl = new Playlist();
                        pl.mId = response.getJSONObject("subsonic-response").getJSONObject("directory").getString("id");
                        pl.mFrom = Playlist.From.SUBSONIC_ALBUM;
                        pl.mTitle = formatName(response.getJSONObject("subsonic-response").getJSONObject("directory").getString("name"));
                        pl.mArtist = mParentAppCompatActivity.getString(R.string.app_name);
                        pl.mCoverArt = response.getJSONObject("subsonic-response").getJSONObject("directory").getString("id");
                        pl.mNumOfSongs = Integer.toString(songs.size());

                        // if user opens the app and music is not playing display last played song
//                        if(!((MainActivity) mParentAppCompatActivity).getMusicService().isMedaPlayerNull())
//                            ((MainActivity)mParentAppCompatActivity).getMusicService().processAddToQueueRequest(songs, false, pl, MusicService.MODE_RECENTLY_SONG);
//                        else {
//                            // if music is playing from service in background
//                            // just update music player UI
//
//                            ((MainActivity) mParentAppCompatActivity).setMusicPlayerViewWithPlayingSong();
//                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                @Override
                public void onError(JSONObject response) {
                }
            }).start();
    }

    private void updateLastPlayedSubsonicPlaylist(Playlist lastPlayedPlaylist){
        new Ajax(mRequestQueue, Consts.getGetPlaylistUrl(lastPlayedPlaylist.mId), new Ajax.AjaxCallbacks(){
            @Override
            public void onSuccess(JSONObject response) {
                try {
                    JSONObject playlist = response.getJSONObject("subsonic-response").getJSONObject("playlist");

                    ArrayList<Song> songs = new ArrayList<>();
                    JSONObject row;
                    JSONArray entry = playlist.getJSONArray("entry");
                    for (int i=0; i < entry.length(); i++){
                        row = entry.getJSONObject(i);
                        songs.add(new Song(row.getInt("id"),
                                row.getString("title"), row.has("artist")?row.getString("artist"):"",
                                Integer.toString(row.getInt("duration")*1000),
                                row.getString("coverArt"),
                                false));
                    }

                    // get playlist
                    Playlist pl = new Playlist();
                    pl.mId = playlist.getString("id");
                    pl.mFrom = Playlist.From.SUBSONIC_PLAYLIST;
                    pl.mTitle = playlist.getString("name");
                    pl.mArtist = playlist.getString("owner");
                    pl.mCoverArt = playlist.getString("coverArt");
                    pl.mNumOfSongs = playlist.getString("songCount");

                    // if user opens the app and music is not playing display last played song
//                    if(!((MainActivity) mParentAppCompatActivity).getMusicService().isMedaPlayerNull())
//                        ((MainActivity)mParentAppCompatActivity).getMusicService().processAddToQueueRequest(songs, false, pl, MusicService.MODE_RECENTLY_SONG);
//                    else {
//                        // if music is playing from service in background
//                        // just update music player UI
//
//                        ((MainActivity) mParentAppCompatActivity).setMusicPlayerViewWithPlayingSong();
//                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onError(JSONObject response) {
            }
        }).start();
    }

    @Override
    public void onPostExecute(Album album, ArrayList<Song> songs) {
        Log.e(TAG, "onPostExecute(Album result)");
        if( album == null || songs == null )
            return;

        Log.e(TAG, album.mId + " " + album.mAlbumName + " " + album.mArtist);

        // get playlist
        Playlist playlist = new Playlist();
        playlist.mId = Integer.toString(album.mId);
        playlist.mFrom = Playlist.From.LOCAL_ALBUM;
        playlist.mTitle = album.mAlbumName != null ? album.mAlbumName : "";
        playlist.mArtist = album.mArtist != null ? album.mArtist : "";
        playlist.mBitmap = album.mAlbumArt != null ? ((BitmapDrawable) album.mAlbumArt).getBitmap() :
                BitmapFactory.decodeResource(mParentAppCompatActivity.getResources(), R.drawable.ic_default_song_poster);
        playlist.mNumOfSongs = Integer.toString(album.mNumberSongs);

        // if user opens the app and music is not playing display last played song
//        if(!((MainActivity) mParentAppCompatActivity).getMusicService().isMedaPlayerNull())
//            ((MainActivity)mParentAppCompatActivity).getMusicService().processAddToQueueRequest(songs, false, playlist, MusicService.MODE_RECENTLY_SONG);
//        else {
//            // if music is playing from service in background
//            // just update music player UI
//
//            ((MainActivity) mParentAppCompatActivity).setMusicPlayerViewWithPlayingSong();
//        }
    }

    private void populateRows(ArrayList<RecentlySong> recentlySongs){
        Log.i(TAG, "populateRows");

        for (int i = 0; i < 9; i++){ // there are 9 items
            if (i > recentlySongs.size() -1) {
                if (recentlySongs.get(i).getPlaylist().mFrom == Playlist.From.LOCAL_ALBUM) {

                    // Log.i(TAG, Playlist.From.LOCAL_ALBUM.toString());
                    new GetLocalAlbum(i % 3, rows.get(i / 3)).execute(recentlySongs.get(i).getPlaylist().mId);

                } else if (recentlySongs.get(i).getPlaylist().mFrom == Playlist.From.SUBSONIC_PLAYLIST) {

                    Log.i(TAG, Playlist.From.SUBSONIC_PLAYLIST.toString());
                    getSubsonicPlaylist(i % 3, rows.get(i / 3),recentlySongs.get(i).getPlaylist().mId);

                } else if (recentlySongs.get(i).getPlaylist().mFrom == Playlist.From.SUBSONIC_ALBUM) {

                    // Log.i(TAG, Playlist.From.SUBSONIC_ALBUM.toString());
                    getSubsonicAlbum(i % 3, rows.get(i / 3), recentlySongs.get(i).getPlaylist().mId);
                }
            }
        }
    }

    //
    // Get deatils for subsonic playlist
    //
    //
    private void getSubsonicPlaylist(final int side, final View row, final String playlistId){
        new Ajax(mRequestQueue, Consts.getGetPlaylistUrl(playlistId), new Ajax.AjaxCallbacks(){
            @Override
            public void onSuccess(JSONObject response) {
                try {
                    JSONObject playlist = response.getJSONObject("subsonic-response").getJSONObject("playlist");

                    setRow(side, row, playlist.getString("coverArt"),
                            playlist.getString("name"), playlist.getString("owner"),
                            new Playlist(playlistId, Playlist.From.SUBSONIC_PLAYLIST));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onError(JSONObject response) {
            }
        }).start();

    }

    //
    // Get deatils for subsonic album
    //
    //
    private void getSubsonicAlbum(final int side, final View row, String id){
        new Ajax(mRequestQueue, Consts.getGetMusicDirectoryUrl(id), new Ajax.AjaxCallbacks() {
            @Override
            public void onSuccess(JSONObject response) {
                try {
                    JSONObject dir = response.getJSONObject("subsonic-response")
                            .getJSONObject("directory");

                    setRow(side, row, dir.getString("id"), formatName(dir.getString("name")),
                            mParentAppCompatActivity.getString(R.string.app_name),
                            new Playlist(dir.getString("id"), Playlist.From.SUBSONIC_ALBUM));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onError(JSONObject response) {
            }
        }).start();
    }

    //
    // If name of subsonic album starts with any of prefixes remove them
    //
    //
    private String formatName(String name){
        if (name.startsWith(Consts.FEATURE_ALBUM_PREFIX)) {
            name = name.replace(Consts.FEATURE_ALBUM_PREFIX, "");
            return name;
        } else if (name.startsWith(Consts.FIRST_LEFT_PLAYLIST_PREFIX)) {
            name = name.replace(Consts.FIRST_LEFT_PLAYLIST_PREFIX, "");
            return name;
        } else if (name.startsWith(Consts.SECOND_LEFT_PLAYLIST_PREFIX)) {
            name = name.replace(Consts.SECOND_LEFT_PLAYLIST_PREFIX, "");
            return name;
        } else if (name.startsWith(Consts.THIRD_LEFT_PLAYLIST_PREFIX)) {
            name = name.replace(Consts.THIRD_LEFT_PLAYLIST_PREFIX, "");
            return name;
        } else if (name.startsWith(Consts.FOURTH_LEFT_PLAYLIST_PREFIX)) {
            name = name.replace(Consts.FOURTH_LEFT_PLAYLIST_PREFIX, "");
            return name;
        } else if (name.startsWith(Consts.FIRST_RIGHT_PLAYLIST_PREFIX)) {
            name = name.replace(Consts.FIRST_RIGHT_PLAYLIST_PREFIX, "");
            return name;
        } else if (name.startsWith(Consts.SECOND_RIGHT_PLAYLIST_PREFIX)) {
            name = name.replace(Consts.SECOND_RIGHT_PLAYLIST_PREFIX, "");
            return name;
        }
        else return name;
    }

    private void setRow(int side, View row, String albumId, String albumTitle, String albumArtist, Playlist playlist){
        ImageView image = null;
        TextView title = null,
                artistTxt = null;
        FrameLayout wrapper = null;
        // in constructor side is setted
        // there are three items per row, use correct one
        switch (side){
            case 0: // left
                // Log.e(TAG, "switch (mSide) = left");
                image = (ImageView) row.findViewById(R.id.poster_img_btn_left);
                title = (TextView) row.findViewById(R.id.song_title_left);
                artistTxt = (TextView) row.findViewById(R.id.song_artist_left);
                wrapper = (FrameLayout) row.findViewById(R.id.left);
                break;
            case 1: // center
                // Log.e(TAG, "switch (mSide) = center");
                image = (ImageView) row.findViewById(R.id.poster_img_btn_center);
                title = (TextView) row.findViewById(R.id.song_title_center);
                artistTxt = (TextView) row.findViewById(R.id.song_artist_center);
                wrapper = (FrameLayout) row.findViewById(R.id.center);
                break;
            case 2: // right
                // Log.e(TAG, "switch (mSide) = right");
                image = (ImageView) row.findViewById(R.id.poster_img_btn_right);
                title = (TextView) row.findViewById(R.id.song_title_right);
                artistTxt = (TextView) row.findViewById(R.id.song_artist_right);
                wrapper = (FrameLayout) row.findViewById(R.id.right);
                break;
        }

        if(image != null && title != null && artistTxt != null && wrapper != null) {
            Picasso.with(mParentAppCompatActivity).load(Consts.getCoverArtUrl(albumId))
                    .into(image);
            title.setText(albumTitle);
            artistTxt.setText(albumArtist);

            wrapper.setTag(playlist);
        }

    }

    //
    // Get local album details (title, author, image) based on album id
    //
    //
    private class GetLocalAlbum extends AsyncTask<String, Void, Album> {
        // on which side to place data (left/center/right)
        int mSide;
        View mRow;

        public GetLocalAlbum(int side, View row){
            mSide = side;
            mRow = row;
        }
        @Override
        protected Album doInBackground(String... albumids) {
            String[] projection = new String[] { MediaStore.Audio.Albums._ID,
                    MediaStore.Audio.Albums.ALBUM,
                    MediaStore.Audio.Albums.ARTIST,
                    MediaStore.Audio.Albums.ALBUM_ART,
                    MediaStore.Audio.Albums.NUMBER_OF_SONGS };
            String sortOrder = MediaStore.Audio.Media.ALBUM + " ASC";

            Cursor cursor = mParentAppCompatActivity
                    .getContentResolver()
                    .query(
                            MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI,
                            projection,
                            MediaStore.Audio.Albums._ID + " = " + albumids[0],
                            null,
                            sortOrder
                    );

            if (!cursor.moveToFirst()) {
                // Nothing to query. There is no music on the device. How boring.
                Log.e(TAG, "Failed to move cursor to first row (no query results).");
                return null;
            }

            int albumIdIndex = cursor.getColumnIndex(MediaStore.Audio.Albums._ID);
            int albumNameIndex = cursor.getColumnIndex(MediaStore.Audio.Albums.ALBUM);
            int artistIndex = cursor.getColumnIndex(MediaStore.Audio.Albums.ARTIST);
            int albumArtIndex = cursor.getColumnIndex(MediaStore.Audio.Albums.ALBUM_ART);
            int numOfSongsIndex = cursor.getColumnIndex(MediaStore.Audio.Albums.NUMBER_OF_SONGS);

            Album tmpAlbum;
            do {
                tmpAlbum = new Album(cursor.getInt(albumIdIndex),
                        cursor.getString(albumNameIndex),
                        cursor.getString(artistIndex).equals("<unknown>") ?
                                mParentAppCompatActivity.getString(R.string.unknown_artist) : cursor.getString(artistIndex),
                        cursor.getString(albumArtIndex) == null ? null : Drawable.createFromPath(cursor.getString(albumArtIndex)),
                        /*Drawable.createFromPath(cursor.getString(albumArtIndex)),*/
                        cursor.getInt(numOfSongsIndex));
            } while (cursor.moveToNext());
            return tmpAlbum;
        }

        @Override
        protected void onPostExecute(Album result) {
            // Log.e(TAG,"found for album id="+result.mId + " " + result.mAlbumName);

            setRow(result.mAlbumName, result.mArtist, result.mAlbumArt, mSide, mRow,
                    new Playlist(Integer.toString(result.mId), Playlist.From.LOCAL_ALBUM));
        }
    }

    private void setRow(String name, String artist, Drawable img, int side, View row, Playlist playlist){
        ImageView image = null;
        TextView title = null,
                artistTxt = null;
        FrameLayout wrapper = null;
        // in constructor side is setted
        // there are three items per row, use correct one
        switch (side){
            case 0: // left
                // Log.e(TAG, "switch (mSide) = left");
                image = (ImageView) row.findViewById(R.id.poster_img_btn_left);
                title = (TextView) row.findViewById(R.id.song_title_left);
                artistTxt = (TextView) row.findViewById(R.id.song_artist_left);
                wrapper = (FrameLayout) row.findViewById(R.id.left);
                break;
            case 1: // center
                // Log.e(TAG, "switch (mSide) = center");
                image = (ImageView) row.findViewById(R.id.poster_img_btn_center);
                title = (TextView) row.findViewById(R.id.song_title_center);
                artistTxt = (TextView) row.findViewById(R.id.song_artist_center);
                wrapper = (FrameLayout) row.findViewById(R.id.center);
                break;
            case 2: // right
                // Log.e(TAG, "switch (mSide) = right");
                image = (ImageView) row.findViewById(R.id.poster_img_btn_right);
                title = (TextView) row.findViewById(R.id.song_title_right);
                artistTxt = (TextView) row.findViewById(R.id.song_artist_right);
                wrapper = (FrameLayout) row.findViewById(R.id.right);
                break;
        }

        if(image != null && title != null && artist != null && wrapper != null) {
            image.setImageDrawable(img);
            title.setText(name);
            artistTxt.setText(artist);

            wrapper.setTag(playlist);
        }
    }


    //
    // use albums in mRecActJSON and add how many more albums are missing to make full recent
    // activity list (there needs to be 9 albums)
    //
    //
    private void fillWithRandomAlbums(){
//        Log.e(TAG, "fillWithRandomAlbums()");
//        final ArrayList<Playlist> tmpPlaylist = new ArrayList<>(mRecentlySongs);
//
//        // if there are enough playlists in array don't make server call
//        if (mRecentlySongs.size() == 9){
//            display(mRecentlySongs);
//            return;
//        }
//
//        new Ajax(mRequestQueue,
//                // 7 is number of albums which start with "-" (albums which should not be
//                //   displayed to user as so (for example -featured-...))
//                // 9 is max number of display recent activities
//                // calculate how much songs are missing (in worst case scenario)
//                Consts.getGetAlbumList("random", 7 + 9 - mRecentlySongs.size()),
//                new Ajax.AjaxCallbacks() {
//                    @Override
//                    public void onSuccess(JSONObject response) {
//                        try {
//                            // 9 is max number of displayed albums
//                            // 9-n where n is number of albums in mRecActJSON
//                            int numOfMissingAlbums = 9 - mRecentlySongs.size();
//                            Log.e(TAG, "mRecentlySongs.size()="+ mRecentlySongs.size() + " numOfMissingAlbums=" + numOfMissingAlbums);
//
//
//
//                            JSONArray albumList = response.getJSONObject("subsonic-response")
//                                    .getJSONObject("albumList")
//                                    .getJSONArray("album");
//
//                            JSONObject row;
//                            for (int i = 0; i < albumList.length(); i++) {
//                                row = albumList.getJSONObject(i);
//
//                                if(numOfMissingAlbums==0)
//                                    break;
//
//                                if (!row.getString("title").startsWith("-")){
//                                    Log.e(TAG, row.getString("title"));
//                                    tmpPlaylist.add(new Playlist(row.getString("id"), Playlist.From.SUBSONIC_ALBUM));
//
//                                    numOfMissingAlbums--;
//                                }
//                            }
//
//                            Log.e(TAG, "array list is filled with random albums. curr length=" + tmpPlaylist.size());
//                            display(tmpPlaylist);
//
//                        } catch (JSONException e){
//                            e.printStackTrace();
//                        }
//                    }
//                    @Override
//                    public void onError(JSONObject response) {
//                    }
//                }).start();


    }

    //
    // Adds an playlist to JSON object for storedge
    //
    // @playlist Playlist to add
//    public void add(Playlist playlist){
//        Log.e(TAG, "add(Playlist playlist)");
//
//        Log.e(TAG, "BEFORE mRecentlySongs.size()="+ mRecentlySongs.size());
//        // 9 is max number of stored playlists
//        while (mRecentlySongs.size() >= 9){
//            // remove last element (element from end)
//            mRecentlySongs.remove(mRecentlySongs.size() - 1);
//        }

//        Log.e(TAG, "id " + playlist.mId + " " + mRecentlySongs.get(0).mId);
//        Log.e(TAG, "from " +  playlist.mFrom + " " + mRecentlySongs.get(0).mFrom);


//        if (mRecentlySongs.isEmpty())
//            mRecentlySongs.add(0, playlist);
//
//        // add song only if it's different from first song
//        if (!(playlist.mId.equals(mRecentlySongs.get(0).mId)
//                && playlist.mFrom == mRecentlySongs.get(0).mFrom) ) {
//            // add object at start of saved playlists
//            mRecentlySongs.add(0, playlist);
//        }
//
//        Log.e(TAG, "AFTER mRecentlySongs.size()=" + mRecentlySongs.size());
//
//        // "reload" the view
//        fillWithRandomAlbums();
//    }

    //
    // Update a mRecentlySongs using json object, which is read from shared pref
    //
    //
    private void updateArrayList(){
        RecentlySongHelper.getInstance(mParentAppCompatActivity);
        mRecentlySongs = RecentlySongHelper.getRecentlySongs();
    }

    //
    // Update mRecActJSON JSONObject with corresponding array list
    //
    //
//    private void updateJSON(){
//        Log.e(TAG, "updateJSON()");
//        mRecActJSON = new JSONObject();
//
//        JSONArray jsonArray = new JSONArray();
//        for(Playlist p : mRecentlySongs){
//            JSONObject tmpJsonObj = new JSONObject();
//            try {
//                tmpJsonObj.put("id", p.mId);
//                tmpJsonObj.put("from", p.mFrom);
//                jsonArray.put(tmpJsonObj);
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        }
//        try {
//            mRecActJSON.put("songs", jsonArray);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        Log.e(TAG, "mRecActJSON updated=" + mRecActJSON.toString());
//    }


    //
    // save mRecActJSON to shared preferences
    //
    //
//    public void saveToSharedPref(){
//        Log.e(TAG, "saveToSharedPref");
//
//        updateJSON();
//
//        SharedPreferences.Editor editor = mSharedPref.edit();
//        editor.putString(mParentAppCompatActivity.getString(R.string.recent_act_json), mRecActJSON.toString());
//        editor.apply();
//    }

    @Override
    public void onClick(View v) {
        Playlist clickedPlaylist = (Playlist) v.getTag();

        if (clickedPlaylist.mFrom == Playlist.From.SUBSONIC_ALBUM){

            // Log.i(TAG, "clicked " + Playlist.From.SUBSONIC_ALBUM);
            openSubsonicAlbum(clickedPlaylist.mId);

        } else if (clickedPlaylist.mFrom == Playlist.From.SUBSONIC_PLAYLIST){

            // Log.i(TAG, "clicked " + Playlist.From.SUBSONIC_PLAYLIST);
            openSubsonicPlaylist(clickedPlaylist.mId);

        } else if (clickedPlaylist.mFrom == Playlist.From.LOCAL_ALBUM){
            TextView title, artist;
            ImageView poster;

            title = (TextView) v.findViewById(R.id.song_title_left);
            artist = (TextView) v.findViewById(R.id.song_artist_left);
            poster = (ImageView) v.findViewById(R.id.poster_img_btn_left);

            if (title == null || artist == null || poster == null){
                title = (TextView) v.findViewById(R.id.song_title_center);
                artist = (TextView) v.findViewById(R.id.song_artist_center);
                poster = (ImageView) v.findViewById(R.id.poster_img_btn_center);
            }

            if (title == null || artist == null || poster == null){
                title = (TextView) v.findViewById(R.id.song_title_right);
                artist = (TextView) v.findViewById(R.id.song_artist_right);
                poster = (ImageView) v.findViewById(R.id.poster_img_btn_right);
            }


            // Log.i(TAG, "clicked " + Playlist.From.LOCAL_ALBUM);
            new GetLocalMusic(title.getText().toString(),
                    artist.getText().toString(), poster.getDrawable()).execute(clickedPlaylist.mId);
        }
    }

    private class GetLocalMusic extends AsyncTask<String, Void, ArrayList<Song>>{
        Drawable mDrawablePoster;
        String mSongTitle;
        String mSongArtist;
        String mAlbumId;

        MaterialDialog materialDialog;

        GetLocalMusic(String songTitle, String songArtist, Drawable drawablePoster){
            mSongTitle = songTitle;
            mSongArtist = songArtist;
            mDrawablePoster = drawablePoster;
        }

        @Override
        protected void onPreExecute() {
            // show dialog
            materialDialog = new MaterialDialog.Builder(mParentAppCompatActivity)
                    .title(R.string.app_name)
                    .content(R.string.loading_dialog_msg)
                    .progress(true, 0)
                    .cancelable(false)
                    .show();
        }

        @Override
        protected ArrayList<Song> doInBackground(String... albumids) {
            mAlbumId = albumids[0];
            ArrayList<Song> songs = new ArrayList<>();

            Uri uri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;

            // Perform a query on the content resolver. The URI we're passing specifies that we
            // want to query for all audio media on external storage (e.g. SD card)
            Cursor cur = mParentAppCompatActivity.getContentResolver().query(
                    uri,
                    null,
                    MediaStore.Audio.Media.IS_MUSIC + " = 1 AND " + MediaStore.Audio.Media.ALBUM_ID + " = " + albumids[0],
                    null,
                    MediaStore.Audio.Media.TITLE + " ASC");

            if (cur == null) {
                // Query failed...
                Log.e(TAG, "Failed to retrieve music: cursor is null");
                return null;
            }
            if (!cur.moveToFirst()) {
                // Nothing to query. There is no music on the device. How boring.
                Log.e(TAG, "Failed to move cursor to first row (no query results).");
                return null;
            }

            // retrieve the indices of the columns where the ID, title, etc. of the song are
            int artistColumn = cur.getColumnIndex(MediaStore.Audio.Media.ARTIST);
            int titleColumn = cur.getColumnIndex(MediaStore.Audio.Media.TITLE);
            int albumIdColumn = cur.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID);
            int idColumn = cur.getColumnIndex(MediaStore.Audio.Media._ID);
            int durationColumn = cur.getColumnIndex(MediaStore.Audio.Media.DURATION);

            do {
                songs.add(new Song(
                        cur.getInt(idColumn),
                        cur.getString(titleColumn),
                        cur.getString(artistColumn).equals("<unknown>") ? mParentAppCompatActivity.getString(R.string.unknown_artist) : cur.getString(artistColumn),
                        cur.getString(durationColumn),
                        cur.getString(albumIdColumn)
                ));
            } while (cur.moveToNext());

            cur.close();

            return songs;
        }

        @Override
        protected void onPostExecute(ArrayList<Song> result) {
            materialDialog.hide();
            materialDialog.dismiss();
            materialDialog = null;


            Bundle bundle = new Bundle();
            bundle.putString("songTitle", mSongTitle);
            bundle.putString("songArist", mSongArtist);
            bundle.putString("numOfSongs", Integer.toString(result.size()));

            // add songs from current album to bundle
            bundle.putParcelableArrayList("songs", result);

            // put which album/playlist/local album is playing
            bundle.putString("id", mAlbumId);
            bundle.putString("from", Playlist.From.LOCAL_ALBUM.toString());

            // add drawable to bundle
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            ((BitmapDrawable)mDrawablePoster).getBitmap().compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] bytes = stream.toByteArray();
            bundle.putByteArray("drawable", bytes);

            // Begin the transaction
            FragmentTransaction ft = mParentAppCompatActivity.getSupportFragmentManager().beginTransaction();
            ft.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
            // Replace the contents of the container with the new fragment
            PlaylistFragment fragment = new PlaylistFragment();
            fragment.setArguments(bundle);
            ft.replace(R.id.container, fragment, PlaylistFragment.FRAGMENT_ID);
            ft.addToBackStack(null);
            ft.commit();
        }
    }

    private void openSubsonicPlaylist(final String playlistId){
        final MaterialDialog materialDialog = new MaterialDialog.Builder(mParentAppCompatActivity)
                .title(R.string.app_name)
                .content(R.string.loading_dialog_msg)
                .progress(true, 0)
                .cancelable(false)
                .show();

        new Ajax(mRequestQueue, Consts.getGetPlaylistUrl(playlistId), new Ajax.AjaxCallbacks(){
            @Override
            public void onSuccess(JSONObject response) {
                try {
                    JSONObject playlist = response.getJSONObject("subsonic-response").getJSONObject("playlist");

                    Bundle bundle = new Bundle();
                    bundle.putString("songTitle", playlist.getString("name"));
                    bundle.putString("songArist", playlist.getString("owner"));
                    bundle.putString("numOfSongs", playlist.getString("songCount"));
                    bundle.putString("coverArt", playlist.getString("coverArt"));

                    bundle.putString("id", playlistId);
                    bundle.putString("from", Playlist.From.SUBSONIC_PLAYLIST.toString());

                    ArrayList<Song> songs = new ArrayList<>();
                    JSONObject row;
                    JSONArray entry = playlist.getJSONArray("entry");
                    for (int i=0; i < entry.length(); i++){
                        row = entry.getJSONObject(i);
                        songs.add(new Song(row.getInt("id"),
                                row.getString("title"), row.has("artist")?row.getString("artist"):"",
                                Integer.toString(row.getInt("duration")*1000),
                                row.getString("coverArt"),
                                false));
                    }

                    bundle.putParcelableArrayList("songs", songs);

                    // Begin the transaction
                    FragmentTransaction ft = mParentAppCompatActivity.getSupportFragmentManager().beginTransaction();
                    ft.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
                    // Replace the contents of the container with the new fragment
                    PlaylistFragment fragment = new PlaylistFragment();
                    fragment.setArguments(bundle);
                    ft.replace(R.id.container, fragment, PlaylistFragment.FRAGMENT_ID);
                    ft.addToBackStack(null);
                    ft.commit();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                materialDialog.hide();
                materialDialog.dismiss();

            }
            @Override
            public void onError(JSONObject response) {
                materialDialog.hide();
                materialDialog.dismiss();
            }
        }).start();
    }

    private void openSubsonicAlbum(String albumId){
        final MaterialDialog materialDialog = new MaterialDialog.Builder(mParentAppCompatActivity)
                .title(R.string.app_name)
                .content(R.string.loading_dialog_msg)
                .progress(true, 0)
                .cancelable(false)
                .show();

        new Ajax(mRequestQueue,
                Consts.getGetMusicDirectoryUrl(albumId),
                new Ajax.AjaxCallbacks() {
                    @Override
                    public void onSuccess(JSONObject response) {
                        try {
                            Bundle bundle = new Bundle();
                            bundle.putString("songTitle", response.getJSONObject("subsonic-response").getJSONObject("directory").getString("name"));
                            bundle.putString("songArist", mParentAppCompatActivity.getString(R.string.app_name));

                            bundle.putString("coverArt", response.getJSONObject("subsonic-response").getJSONObject("directory").getString("id"));


                            bundle.putString("id", Integer.toString(response.getJSONObject("subsonic-response").getJSONObject("directory").getInt("id")));
                            bundle.putString("from", Playlist.From.SUBSONIC_ALBUM.toString());

                            ArrayList<Song> songs = new ArrayList<>();
                            JSONObject row;
                            JSONArray entry = response.getJSONObject("subsonic-response").getJSONObject("directory").getJSONArray("child");
                            for (int i=0; i < entry.length(); i++){
                                row = entry.getJSONObject(i);
                                songs.add(new Song(row.getInt("id"),
                                        row.getString("title"), row.has("artist")?row.getString("artist"):"",
                                        Integer.toString(row.getInt("duration")*1000),
                                        row.getString("coverArt"),
                                        false));
                            }
                            bundle.putString("numOfSongs", Integer.toString(songs.size()));

                            bundle.putParcelableArrayList("songs", songs);

                            // Begin the transaction
                            FragmentTransaction ft = mParentAppCompatActivity.getSupportFragmentManager().beginTransaction();
                            ft.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
                            // Replace the contents of the container with the new fragment
                            PlaylistFragment fragment = new PlaylistFragment();
                            fragment.setArguments(bundle);
                            ft.replace(R.id.container, fragment, PlaylistFragment.FRAGMENT_ID);
                            ft.addToBackStack(null);
                            ft.commit();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        materialDialog.hide();
                        materialDialog.dismiss();
                    }
                    @Override
                    public void onError(JSONObject response) {
                        materialDialog.hide();
                        materialDialog.dismiss();
                    }
                }).start();
    }
}
