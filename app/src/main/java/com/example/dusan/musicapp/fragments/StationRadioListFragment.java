package com.example.dusan.musicapp.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.example.dusan.musicapp.MainActivity;
import com.example.dusan.musicapp.R;
import com.example.dusan.musicapp.adapters.RadioAdapter;
import com.example.dusan.musicapp.models.StationRadio;
import com.example.dusan.musicapp.playlist.Playlist;
import com.example.dusan.musicapp.playlist.Song;

import java.util.ArrayList;

/**
 * Created by ${binhpd} on 6/30/2016.
 */
public class StationRadioListFragment extends BaseFragment implements View.OnClickListener {

    public static final String KEY_EXTRA_NAME = "KEY_EXTRA_NAME";
    public static final String KEY_EXTRA_LOGO = "KEY_EXTRA_LOGO";
    public static final String KEY_EXTRA_URL = "KEY_EXTRA_URL";

    private Activity mActivity;

    private ListView mLvStatiosList;
    private ArrayList<StationRadio> mListStations;
    private RadioAdapter mRadioAdapter;

    private ImageView mIvBack;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_stations_radio_list, null);
        mActivity = getActivity();

        mLvStatiosList = (ListView) view.findViewById(R.id.lvStationRadioList);
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar.setTitle("Stations");

        //for crate home button
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolbar);
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });
//        mIvBack.setOnClickListener(this);


        mListStations = new ArrayList<>();
        mListStations.add(new StationRadio("Station radio 1", "logo_station.jpg", "http://whooshserver.net:8402/live"));
        mListStations.add(new StationRadio("WSJE-DB - Smooth Jazz Expressions", "logo_station.jpg", "http://64.78.234.171:8216/"));
        mListStations.add(new StationRadio("National Soul Radio", "logo_station.jpg", "http://91.121.91.172:8516/stream"));
        mListStations.add(new StationRadio("RJMRnB", "logo_station.jpg", "http://streaming.radionomy.com/RJM-RnB"));
        mListStations.add(new StationRadio("181.fm - True R&B", "logo_station.jpg", "http://relay.181.fm:8022/"));
        mRadioAdapter = new RadioAdapter(mActivity, mListStations);
        mLvStatiosList.setAdapter(mRadioAdapter);

        mLvStatiosList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                StationRadio stationRadio = mListStations.get(position);
                RadioStreamingFragment radioStreamingFragment = new RadioStreamingFragment();
                Bundle bundle = new Bundle();
                bundle.putString(KEY_EXTRA_NAME, stationRadio.getName());
                bundle.putString(KEY_EXTRA_LOGO, stationRadio.getUrlLogo());
                bundle.putString(KEY_EXTRA_URL, stationRadio.getUrlStream());
                radioStreamingFragment.setArguments(bundle);
                addFragment(R.id.container, radioStreamingFragment);
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        Song song = ((MainActivity)getActivity()).getMusicService().getCurrentlyPlayingSong();
        Playlist playlist = ((MainActivity)getActivity()).getMusicService().getPlaylist();
        if (song != null && playlist != null&& playlist.mFrom == Playlist.From.RADIO) {
            mRadioAdapter.setRadioPlaying(song.getStreamUrl());
            mRadioAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onClick(View v) {
        if (v == mIvBack) {
            getFragmentManager().popBackStack();
        }
    }
}
