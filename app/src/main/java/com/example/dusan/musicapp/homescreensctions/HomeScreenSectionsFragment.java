package com.example.dusan.musicapp.homescreensctions;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.example.dusan.musicapp.Ajax;
import com.example.dusan.musicapp.Consts;
import com.example.dusan.musicapp.R;
import com.example.dusan.musicapp.SubsonicDirectory;
import com.example.dusan.musicapp.playlist.Playlist;
import com.example.dusan.musicapp.playlist.PlaylistFragment;
import com.example.dusan.musicapp.playlist.Song;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class HomeScreenSectionsFragment extends Fragment implements HomeScreenSectionsAdapter.Callback{
    public static String  FRAGMENT_ID = "HomeScreenSectionsFragment";

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    RequestQueue mRequestQueue;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Defines the xml file for the fragment
        View view = inflater.inflate(R.layout.recommended_for_you_fragment_layout, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.rec_view);
        mRecyclerView.addItemDecoration(new HorizontalDividerItemDecoration.Builder(getActivity()).build());

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        Bundle bundle = this.getArguments();
        ArrayList<SubsonicDirectory> subDirectories = bundle.getParcelableArrayList("subDirectories");

        // specify an adapter (see also next example)
        mAdapter = new HomeScreenSectionsAdapter(subDirectories, getActivity(), this);
        mRecyclerView.setAdapter(mAdapter);

        mRequestQueue = Volley.newRequestQueue(getActivity());

        return view;
    }

    @Override
    public void onDirectoryClicked(SubsonicDirectory directory) {
        final MaterialDialog materialDialog = new MaterialDialog.Builder(getActivity())
                .title(R.string.app_name)
                .content(R.string.loading_dialog_msg)
                .progress(true, 0)
                .cancelable(false)
                .show();

        new Ajax(mRequestQueue,
                Consts.getGetMusicDirectoryUrl(Integer.toString(directory.getId())),
                new Ajax.AjaxCallbacks() {
                    @Override
                    public void onSuccess(JSONObject response) {
                        try {
                            Bundle bundle = new Bundle();
                            bundle.putString("songTitle", response.getJSONObject("subsonic-response").getJSONObject("directory").getString("name"));
                            bundle.putString("songArist", getString(R.string.app_name));

                            bundle.putString("coverArt", response.getJSONObject("subsonic-response").getJSONObject("directory").getString("id"));


                            bundle.putString("id", Integer.toString(response.getJSONObject("subsonic-response").getJSONObject("directory").getInt("id")));
                            bundle.putString("from", Playlist.From.SUBSONIC_ALBUM.toString());

                            ArrayList<Song> songs = new ArrayList<>();
                                JSONObject row;
                            JSONArray entry = response.getJSONObject("subsonic-response").getJSONObject("directory").getJSONArray("child");
                            for (int i=0; i < entry.length(); i++){
                                row = entry.getJSONObject(i);
                                songs.add(new Song(row.getInt("id"),
                                        row.getString("title"), row.has("artist")?row.getString("artist"):"",
                                        Integer.toString(row.getInt("duration")*1000),
                                        row.getString("coverArt"),
                                        false));
                            }
                            bundle.putString("numOfSongs", Integer.toString(songs.size()));

                            bundle.putParcelableArrayList("songs", songs);

                            // Begin the transaction
                            FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                            ft.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
                            // Replace the contents of the container with the new fragment
                            PlaylistFragment fragment = new PlaylistFragment();
                            fragment.setArguments(bundle);
                            ft.replace(R.id.container, fragment, PlaylistFragment.FRAGMENT_ID);
                            ft.addToBackStack(null);
                            ft.commit();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        materialDialog.hide();
                        materialDialog.dismiss();
                    }
                    @Override
                    public void onError(JSONObject response) {
                        materialDialog.hide();
                        materialDialog.dismiss();
                    }
                }).start();
    }
}
