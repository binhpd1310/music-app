package com.example.dusan.musicapp;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

public class Ajax {
    private String mUrl;

    private AjaxCallbacks mCallbacks;

    RequestQueue mRequestQueue;

    public Ajax(RequestQueue requestQueue, String url, AjaxCallbacks... callbacks){
        mRequestQueue = requestQueue;
        mUrl = url;
        mCallbacks = callbacks[0];
    }

    public void start(){
        StringRequest stringRequest = new StringRequest(Request.Method.GET, mUrl,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject r = new JSONObject(response);
                            if( r.getJSONObject("subsonic-response").getString("status").equals("ok") ){
                                mCallbacks.onSuccess(r);
                            } else {
                                mCallbacks.onError(r);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        mRequestQueue.add(stringRequest);
    }

    public interface AjaxCallbacks{
        void onSuccess(JSONObject response);
        void onError(JSONObject response);
    }

}
