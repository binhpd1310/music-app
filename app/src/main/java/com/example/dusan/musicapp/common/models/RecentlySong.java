package com.example.dusan.musicapp.common.models;

import com.example.dusan.musicapp.playlist.Playlist;
import com.example.dusan.musicapp.playlist.Song;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by ${binhpd} on 6/8/2016.
 */
public class RecentlySong implements Serializable {
    public Song song;
    public Playlist playlist;
    private int index;

    public RecentlySong(int index, Song songs, Playlist playlist) {
        this.index = index;
        this.song = songs;
        this.playlist = playlist;
    }

    public Song getSongs() {
        return song;
    }

    public void setSongs(Song song) {
        this.song = song;
    }

    public Playlist getPlaylist() {
        return playlist;
    }

    public void setPlaylist(Playlist playlist) {
        this.playlist = playlist;
    }

    public int getIndex() {
        return index;
    }
}