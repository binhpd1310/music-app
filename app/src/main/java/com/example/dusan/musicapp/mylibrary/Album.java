package com.example.dusan.musicapp.mylibrary;

import android.graphics.drawable.Drawable;

public class Album {
    public int mId;
    public String mAlbumName;
    public String mArtist;
    public Drawable mAlbumArt;
    public int mNumberSongs;

    public Album(int id, String albumName, String artist, Drawable albumArt, int numberSongs){
        mId = id;
        mAlbumName = albumName;
        mArtist = artist;
        mAlbumArt = albumArt;
        mNumberSongs = numberSongs;
    }

}
